// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: blue; icon-glyph: circle;
// This script was downloaded using ScriptDude.
// Do not remove these lines, if you want to benefit from automatic updates.
// source: https://raw.githubusercontent.com/wickenico/random-meme.js/main/random-meme.js; docs: https://github.com/wickenico/random-meme.js; hash: 69584071;

/* --------------------------------------------------------------
Script: random-meme.js
Author: Nico Wickersheim
Version: 1.0.0

Description:
Displays a random meme every time the script is executed.
Widget is large for optimal visualization.

Changelog:

1.0.0: Initialization
-------------------------------------------------------------- */
const url = `https://meme-api.herokuapp.com/gimme`
const req = new Request(url)
const res = await req.loadJSON()
const memeUrl = res.url;
const i = new Request(memeUrl);
const img = await i.loadImage()


let widget = createWidget(img)
if (config.runsInWidget) {
  // create and show widget
  Script.setWidget(widget)
  Script.complete()
}
else {
  widget.presentLarge()
}

// Assemble widget layout 
function createWidget(img) {
  let w = new ListWidget()
  w.backgroundColor = new Color("#1A1A1A")

  w.addSpacer(1)

  let image = w.addImage(img)
  image.centerAlignImage()

  w.addSpacer(1)
  
  w.setPadding(0, 0, 0, 0)
  return w
}
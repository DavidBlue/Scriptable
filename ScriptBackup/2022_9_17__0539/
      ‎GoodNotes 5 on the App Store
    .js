// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: light-brown; icon-glyph: magic;
<!DOCTYPE html><html prefix="og: http://ogp.me/ns#"  dir="ltr" lang="en-US"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta name="applicable-device" content="pc,mobile">

    <script id="perfkit">window.initialPageRequestTime = +new Date();</script>
    <link rel="preconnect" href="https://amp-api.apps.apple.com" crossorigin="">
<link rel="preconnect" href="https://api-edge.apps.apple.com" crossorigin="">
<link rel="preconnect" href="https://is1-ssl.mzstatic.com" crossorigin="">
<link rel="preconnect" href="https://is2-ssl.mzstatic.com" crossorigin="">
<link rel="preconnect" href="https://is3-ssl.mzstatic.com" crossorigin="">
<link rel="preconnect" href="https://is4-ssl.mzstatic.com" crossorigin="">
<link rel="preconnect" href="https://is5-ssl.mzstatic.com" crossorigin="">
<link rel="preconnect" href="https://xp.apple.com" crossorigin="">
<link rel="preconnect" href="https://js-cdn.music.apple.com" crossorigin="">
<link rel="preconnect" href="https://www.apple.com" crossorigin="">
    
<meta name="web-experience-app/config/environment" content="%7B%22appVersion%22%3A1%2C%22modulePrefix%22%3A%22web-experience-app%22%2C%22environment%22%3A%22production%22%2C%22rootURL%22%3A%22%2F%22%2C%22locationType%22%3A%22history-hash-router-scroll%22%2C%22historySupportMiddleware%22%3Atrue%2C%22EmberENV%22%3A%7B%22FEATURES%22%3A%7B%7D%2C%22EXTEND_PROTOTYPES%22%3A%7B%22Date%22%3Afalse%7D%2C%22_APPLICATION_TEMPLATE_WRAPPER%22%3Afalse%2C%22_DEFAULT_ASYNC_OBSERVERS%22%3Atrue%2C%22_JQUERY_INTEGRATION%22%3Afalse%2C%22_TEMPLATE_ONLY_GLIMMER_COMPONENTS%22%3Atrue%7D%2C%22APP%22%3A%7B%22PROGRESS_BAR_DELAY%22%3A3000%2C%22CLOCK_INTERVAL%22%3A1000%2C%22LOADING_SPINNER_SPY%22%3Atrue%2C%22BREAKPOINTS%22%3A%7B%22large%22%3A%7B%22min%22%3A1069%2C%22max%22%3A1440%2C%22content%22%3A980%7D%2C%22medium%22%3A%7B%22min%22%3A735%2C%22max%22%3A1068%2C%22content%22%3A692%7D%2C%22small%22%3A%7B%22min%22%3A320%2C%22max%22%3A734%2C%22content%22%3A280%7D%7D%2C%22buildVariant%22%3A%22apps%22%2C%22name%22%3A%22web-experience-app%22%2C%22version%22%3A%222230.0.0%2Bb558459a%22%7D%2C%22MEDIA_API%22%3A%7B%22token%22%3A%22eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlU4UlRZVjVaRFMifQ.eyJpc3MiOiI3TktaMlZQNDhaIiwiaWF0IjoxNjU3NTQxMTQ0LCJleHAiOjE2NjQ3OTg3NDQsInJvb3RfaHR0cHNfb3JpZ2luIjpbImFwcGxlLmNvbSJdfQ.Oqznjt8qXrtl0OKiynAwDXzfsRK7HkzwDMDkMdz3oRr2T7dmJubCc-xm0SiFziKxX8tVYCHaHo16hIXlxYqEKQ%22%7D%2C%22i18n%22%3A%7B%22defaultLocale%22%3A%22en-gb%22%2C%22useDevLoc%22%3Afalse%2C%22pathToLocales%22%3A%22dist%2Flocales%22%7D%2C%22MEDIA_ARTWORK%22%3A%7B%22BREAKPOINTS%22%3A%7B%22large%22%3A%7B%22min%22%3A1069%2C%22max%22%3A1440%2C%22content%22%3A980%7D%2C%22medium%22%3A%7B%22min%22%3A735%2C%22max%22%3A1068%2C%22content%22%3A692%7D%2C%22small%22%3A%7B%22min%22%3A320%2C%22max%22%3A734%2C%22content%22%3A280%7D%7D%7D%2C%22API%22%3A%7B%22AppHost%22%3A%22https%3A%2F%2Famp-api.apps.apple.com%22%2C%22AppEventHost%22%3A%22https%3A%2F%2Fapi-edge.apps.apple.com%22%2C%22globalElementsPath%22%3A%22%2Fglobal-elements%22%7D%2C%22fastboot%22%3A%7B%22hostWhitelist%22%3A%5B%7B%7D%5D%7D%2C%22ember-short-number%22%3A%7B%22locales%22%3A%5B%22ar-dz%22%2C%22ar-bh%22%2C%22ar-eg%22%2C%22ar-iq%22%2C%22ar-jo%22%2C%22ar-kw%22%2C%22ar-lb%22%2C%22ar-ly%22%2C%22ar-ma%22%2C%22ar-om%22%2C%22ar-qa%22%2C%22ar-sa%22%2C%22ar-sd%22%2C%22ar-sy%22%2C%22ar-tn%22%2C%22ar-ae%22%2C%22ar-ye%22%2C%22he-il%22%2C%22iw-il%22%2C%22ca-es%22%2C%22cs-cz%22%2C%22da-dk%22%2C%22de-ch%22%2C%22de-de%22%2C%22el-gr%22%2C%22en-au%22%2C%22en-ca%22%2C%22en-gb%22%2C%22en-us%22%2C%22es-419%22%2C%22es-es%22%2C%22es-mx%22%2C%22es-xl%22%2C%22et-ee%22%2C%22fi-fi%22%2C%22fr-ca%22%2C%22fr-fr%22%2C%22hi-in%22%2C%22hr-hr%22%2C%22hu-hu%22%2C%22id-id%22%2C%22is-is%22%2C%22it-it%22%2C%22iw-il%22%2C%22ja-jp%22%2C%22ko-kr%22%2C%22lt-lt%22%2C%22lv-lv%22%2C%22ms-my%22%2C%22nl-nl%22%2C%22no-no%22%2C%22no-nb%22%2C%22nb-no%22%2C%22pl-pl%22%2C%22pt-br%22%2C%22pt-pt%22%2C%22ro-ro%22%2C%22ru-ru%22%2C%22sk-sk%22%2C%22sv-se%22%2C%22th-th%22%2C%22tr-tr%22%2C%22uk-ua%22%2C%22vi-vi%22%2C%22vi-vn%22%2C%22zh-cn%22%2C%22zh-hans%22%2C%22zh-hant%22%2C%22zh-hk%22%2C%22zh-tw%22%2C%22ar%22%2C%22ca%22%2C%22cs%22%2C%22da%22%2C%22de%22%2C%22el%22%2C%22en%22%2C%22es%22%2C%22fi%22%2C%22fr%22%2C%22he%22%2C%22hi%22%2C%22hr%22%2C%22hu%22%2C%22id%22%2C%22is%22%2C%22it%22%2C%22ja%22%2C%22ko%22%2C%22lt%22%2C%22lv%22%2C%22ms%22%2C%22nb%22%2C%22nl%22%2C%22no%22%2C%22pl%22%2C%22pt%22%2C%22ro%22%2C%22ru%22%2C%22sk%22%2C%22sv%22%2C%22th%22%2C%22tr%22%2C%22uk%22%2C%22vi%22%2C%22zh%22%5D%7D%2C%22ember-cli-mirage%22%3A%7B%22enabled%22%3Afalse%2C%22usingProxy%22%3Afalse%2C%22useDefaultPassthroughs%22%3Atrue%7D%2C%22BREAKPOINTS%22%3A%7B%22large%22%3A%7B%22min%22%3A1069%2C%22max%22%3A1440%2C%22content%22%3A980%7D%2C%22medium%22%3A%7B%22min%22%3A735%2C%22max%22%3A1068%2C%22content%22%3A692%7D%2C%22small%22%3A%7B%22min%22%3A320%2C%22max%22%3A734%2C%22content%22%3A280%7D%7D%2C%22METRICS%22%3A%7B%22variant%22%3A%22web%22%2C%22baseFields%22%3A%7B%22appName%22%3A%22web-experience-app%22%2C%22constraintProfiles%22%3A%5B%22AMPWeb%22%5D%7D%2C%22clickstream%22%3A%7B%22enabled%22%3Atrue%2C%22topic%22%3A%5B%22xp_amp_web_exp%22%5D%2C%22autoTrackClicks%22%3Atrue%7D%2C%22performance%22%3A%7B%22enabled%22%3Atrue%2C%22topic%22%3A%22xp_amp_appstore_perf%22%7D%7D%2C%22MEDIA_SHELF%22%3A%7B%22GRID_CONFIG%22%3A%7B%22books-brick-row%22%3A%7B%22small%22%3A1%2C%22medium%22%3A2%2C%22large%22%3A3%7D%7D%2C%22BREAKPOINTS%22%3A%7B%22large%22%3A%7B%22min%22%3A1069%2C%22max%22%3A1440%2C%22content%22%3A980%7D%2C%22medium%22%3A%7B%22min%22%3A735%2C%22max%22%3A1068%2C%22content%22%3A692%7D%2C%22small%22%3A%7B%22min%22%3A320%2C%22max%22%3A734%2C%22content%22%3A280%7D%7D%7D%2C%22SASSKIT_GENERATOR%22%3A%7B%22VIEWPORT_CONFIG%22%3A%7B%22BREAKPOINTS%22%3A%7B%22large%22%3A%7B%22min%22%3A1069%2C%22max%22%3A1440%2C%22content%22%3A980%7D%2C%22medium%22%3A%7B%22min%22%3A735%2C%22max%22%3A1068%2C%22content%22%3A692%7D%2C%22small%22%3A%7B%22min%22%3A320%2C%22max%22%3A734%2C%22content%22%3A280%7D%7D%7D%7D%2C%22features%22%3A%7B%22BUILD_VARIANT_APPS%22%3Atrue%2C%22BUILD_VARIANT_BOOKS%22%3Afalse%2C%22BUILD_VARIANT_FITNESS%22%3Afalse%2C%22BUILD_VARIANT_PODCASTS%22%3Afalse%2C%22BUILD_VARIANT_DEFAULT%22%3Afalse%2C%22TV%22%3Afalse%2C%22PODCASTS%22%3Afalse%2C%22BOOKS%22%3Afalse%2C%22APPS%22%3Atrue%2C%22ARTISTS%22%3Afalse%2C%22DEEPLINK_ROUTE%22%3Afalse%2C%22EMBER_DATA%22%3Afalse%2C%22CHARTS%22%3Atrue%2C%22FITNESS%22%3Afalse%2C%22SHARE_UI%22%3Afalse%2C%22SEPARATE_RTL_STYLESHEET%22%3Atrue%7D%2C%22ember-cli-content-security-policy%22%3A%7B%22policy%22%3A%22default-src%20'none'%3B%20img-src%20'self'%20https%3A%2F%2F*.apple.com%20https%3A%2F%2F*.mzstatic.com%20data%3A%3B%20style-src%20'self'%20https%3A%2F%2F*.apple.com%20'unsafe-inline'%3B%20font-src%20'self'%20https%3A%2F%2F*.apple.com%3B%20media-src%20'self'%20https%3A%2F%2F*.apple.com%20blob%3A%3B%20connect-src%20'self'%20https%3A%2F%2F*.apple.com%20https%3A%2F%2F*.mzstatic.com%3B%20script-src%20'self'%20https%3A%2F%2F*.apple.com%20'unsafe-eval'%20'sha256-4ywTGAe4rEpoHt8XkjbkdOWklMJ%2F1Py%2Fx6b3%2FaGbtSQ%3D'%3B%20frame-src%20'self'%20https%3A%2F%2F*.apple.com%20itmss%3A%20itms-appss%3A%20itms-bookss%3A%20itms-itunesus%3A%20itms-messagess%3A%20itms-podcasts%3A%20itms-watchs%3A%20macappstores%3A%20musics%3A%20apple-musics%3A%20podcasts%3A%20videos%3A%3B%22%2C%22reportOnly%22%3Afalse%7D%2C%22exportApplicationGlobal%22%3Afalse%7D">
<!-- EMBER_CLI_FASTBOOT_TITLE --><link rel="stylesheet preload" name="fonts" href="//www.apple.com/wss/fonts?families=SF+Pro,v2|SF+Pro+Icons,v1|SF+Pro+Rounded,v1|New+York+Small,v1|New+York+Medium,v1" as="style"><link rel="stylesheet" type="text/css" href="/global-elements/2210.1.0/en_US/ac-global-nav.6ab06e7ad3aaf025138951b5c958ae3b.css" data-global-elements-nav-styles>
<link rel="stylesheet" type="text/css" href="/global-elements/2210.1.0/en_US/ac-global-footer.7fb24d229e183ab411ed7662850ce5a0.css" data-global-elements-footer-styles>
<meta name="ac-gn-search-suggestions-enabled" content="false" data-global-elements-search-suggestions-enabled />
    <title>
      ‎GoodNotes 5 on the App Store
    </title>
      <meta name="keywords" content="GoodNotes 5, Time Base Technology Limited, Productivity,Education, ios apps, mac apps, app, appstore, app store, applications, iphone, ipad, ipod touch, itouch, itunes">

      <meta name="description" content="Read reviews, compare customer ratings, see screenshots, and learn more about GoodNotes 5. Download GoodNotes 5 and enjoy it on your iPhone, iPad, iPod touch, or Mac OS X 10.15 or later.">

      <link rel="canonical" href="https://apps.apple.com/us/app/goodnotes-5/id1444383602">

<!---->
<!---->
<!---->
      <meta name="apple:content_id" content="1444383602">

      <script name="schema:software-application" type="application/ld+json">
        {"@context":"http://schema.org","@type":"SoftwareApplication","name":"GoodNotes 5","description":"Use GoodNotes on Mac to access your digital notes wherever you work. With iCloud, your digital notes will be synced on all your devices, making the GoodNotes Mac app the perfect partner to access your digital notes on your computer.\n\nNEVER LOSE YOUR NOTES\n• Search your handwritten notes, typed text, PDF text, document outlines, folder titles, and document titles.\n• Create unlimited folders and subfolders, or mark your Favorite ones to keep everything organized.\n• Create custom outlines for easier navigation through your documents.\n• Add hyperlinks to external websites, videos, articles to build your knowledge map.\n• Back up all your notes to iCloud, Google Drive, Dropbox, and OneDrive and sync across all devices so you will never lose them.\n\nESCAPE THE LIMITS OF ANALOG PAPER\n• Move, resize, and rotate your handwriting or change colors.\n• Draw perfect shapes and lines with the Shape Tool.\n• Navigate through imported PDFs with existing hyperlinks.\n• Choose to erase the entire stroke, only parts of it, or only highlighters to leave the ink intact.\n• Select to edit or move a specific object with the Lasso Tool.\n• Add, create, or import your stickers, pictures, tables, diagrams, and more with Elements to enrich your notes.\n• Choose from a large set of beautiful covers and useful paper templates, including Blank Paper, Ruled Paper, Cornell Paper, Checklists, To-dos, Planners, Music Paper, Flashcards, and more.\n• Upload any PDF or image as a custom notebook cover or paper template for more customization.\n\nSHARE, COLLABORATE, &amp; PRESENT FROM YOUR MAC\n• Present a lecture, a lesson, a business plan, a brainstorming result, or your group study without distractions when you connect your Mac via AirPlay or HDMI to an external screen.\n• Use Laser Pointer on your iPad to guide your audience’s attention during your presentation.\n• Collaborate with others on the same document using a sharable link.\n\n---\n\nWebsite: www.goodnotes.com\nTwitter: @goodnotesapp\nInstagram: @goodnotes.app\nPinterest: @goodnotesapp\nTikTok: @goodnotesapp","screenshot":["https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/643x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/643x0w.jpg","https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/643x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/643x0w.jpg","https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/643x0w.jpg","https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/04/e5/ae/04e5ae6b-e38e-1481-c99c-a8b7ec0e14a6/dd9fe711-5041-4431-bc7c-4a5ab6a59913_iPhone-6.5-1-Notes__U0028Medicine_U0029.png/300x0w.jpg","https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/af/c2/82/afc2825d-caf6-e8e4-d6f7-c899aa65f6d8/65af2430-3e27-44c2-b59f-4ff7d7454461_iPhone-6.5-2-Search.png/300x0w.jpg","https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/f6/d6/56/f6d65696-4aef-4aff-d2a1-3dd5c1459da1/71865151-f85d-4ca6-9bbc-5a8b49c53c82_iPhone-6.5-4-Flashcards__U0028US_U0029.png/300x0w.jpg","https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/be/ee/22/beee2284-a412-efbb-5370-a18c75b7b890/9ab7d69d-37eb-4d74-82ef-67b23d61110e_iPhone-6.5-5-Organization.png/300x0w.jpg","https://is3-ssl.mzstatic.com/image/thumb/Purple112/v4/53/ac/8d/53ac8dda-4390-a033-6f13-7031c51627d3/6b3a9737-dbfa-4dde-a339-66361cdef4bc_iPhone-6.5-11-Comments.png/300x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/7f/b7/61/7fb761ba-a59b-c261-a9ed-bbbad503bb5d/6e192d7e-6850-49e0-ad5d-fc926563399e_iPhone-6.5-6-Import.png/300x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/41/55/52/4155526a-4348-d93d-3175-1eb7d932359e/b96488fd-1c25-4fd5-90f2-624199f6433f_iPhone-6.5-7-iCloud.png/300x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/92/9f/27/929f27f4-2fae-8c67-938b-1a8a438d11e3/78afc62d-255b-4975-8708-c1c98e1c18a9_iPhone-6.5-8-Presentation__U0028Chemistry_U0029.png/300x0w.jpg","https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/6c/55/e0/6c55e081-bf34-56e5-2754-f9891a5490ee/b25d24c6-2ada-4f85-a797-8da34a1d03c9_iPhone-6.5-9-Dark-Mode.png/300x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/1e/69/27/1e6927c0-cce6-d625-403c-e815707f309c/d9f3f6db-0fda-41e0-8a3b-03591e8be118_iPad-12.9-1-Hand-Writing__U0028Medicine_U0029.png/643x0w.jpg","https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/43/95/83/439583a9-1c56-e0cb-d4a9-48cea3400889/48477cc9-1fa5-40cf-a73b-cfa9f7df2092_iPad-12.9-3-Annotation.png/643x0w.jpg","https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/27/af/91/27af91fd-9838-784f-c459-d9b2258efedd/90d8430b-b079-478c-a032-3b42a4c565b3_iPad-12.9-2-Search-And-Find.png/643x0w.jpg","https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/8c/c5/0f/8cc50f8f-f7fc-c7cb-1f05-9fe8f91780ee/c25f2a81-2756-42b7-8a35-423d1147aa57_iPad-12.9-11-External-Links.png/643x0w.jpg","https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/fa/f8/4e/faf84e05-c8b9-cf14-581a-72ef034c322c/25527e51-db19-4e69-bc7a-be9a52c1ea06_iPad-12.9-8-Customize-Notebooks.png/643x0w.jpg","https://is1-ssl.mzstatic.com/image/thumb/PurpleSource112/v4/8d/0f/eb/8d0febdd-d48d-a4da-fb45-48dc75371785/ba044476-0402-48bb-a96c-dcf21828466e_iPad-12.9-6-Monthly-Planner__U0028Student_U0029.png/643x0w.jpg","https://is2-ssl.mzstatic.com/image/thumb/Purple122/v4/2b/9f/ac/2b9fac79-2229-0b8a-69f9-82f597f1c702/5a7269b2-2adc-49db-83f8-0f2cbd1e31b5_iPad-12.9-13-Custom-Outline.png/643x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/35/18/97/351897bd-416b-d86f-2453-ce52773daa99/75361243-eada-4922-b2c5-df86c28da2ad_iPad-12.9-5-Split-Screens.png/643x0w.jpg","https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/15/24/4d/15244da0-fc0d-cc82-bc4f-9a3b99dd820b/846604eb-fe29-4775-972e-b932dc1ae13b_iPad-12.9-9-Teaching-And-Presentation__U0028Chemistry_U0029.png/643x0w.jpg","https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/40/5b/5f/405b5fd0-69a4-c07f-9df6-4a2a593dca98/cf59e802-2648-4d9e-9d11-fc5079185728_iPad-12.9-10-Dark-Mode.png/643x0w.jpg"],"image":"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/1200x630wa.png","applicationCategory":"Productivity","datePublished":"Jan 15, 2019","operatingSystem":"macOS 10.15 or later. Requires iOS 13.0 or later. Compatible with iPhone, iPad, and iPod touch.","author":{"@type":"Person","name":"Time Base Technology Limited","url":"https://apps.apple.com/us/developer/time-base-technology-limited/id424587624"},"aggregateRating":{"@type":"AggregateRating","ratingValue":4.8,"reviewCount":60632},"offers":{"@type":"Offer","price":0,"category":"free"}}
      </script>

<!---->
        <meta property="og:title" content="‎GoodNotes 5">
        <meta property="og:description" content="‎Use GoodNotes on Mac to access your digital notes wherever you work. With iCloud, your digital notes will be synced on all your devices, making the GoodNotes Mac app the perfect partner to access your digital notes on your computer.

NEVER LOSE YOUR NOTES
• Search your handwritten notes, typed text,…">
        <meta property="og:site_name" content="App Store">
        <meta property="og:url" content="https://apps.apple.com/us/app/goodnotes-5/id1444383602">
        <meta property="og:image" content="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/1200x630wa.png">
        <meta property="og:image:alt" content="GoodNotes 5 on the App Store">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="630">
        <meta property="og:image:secure_url" content="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/1200x630wa.png">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_US">

        <meta name="twitter:title" content="‎GoodNotes 5">
        <meta name="twitter:description" content="‎Use GoodNotes on Mac to access your digital notes wherever you work. With iCloud, your digital notes will be synced on all your devices, making the GoodNotes Mac app the perfect partner to access your digital notes on your computer.

NEVER LOSE YOUR NOTES
• Search your handwritten notes, typed text,…">
        <meta name="twitter:site" content="@AppStore">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/1200x600wa.png">
        <meta name="twitter:image:alt" content="GoodNotes 5 on the App Store">

<!---->
    <meta name="version" content="2230.0.0">
    <script src="https://js-cdn.music.apple.com/musickit/v2/amp/musickit.js?t=1657541227088"></script>

    <link integrity="" rel="stylesheet" href="/assets/web-experience-app-real-d19a35965975e3b00cb6a2c99e51bb54.css" data-rtl="/assets/web-experience-rtl-app-e651f966b4685ef372f10db4c8a7abb7.css">

    
  </head>
  <body class="no-js no-touch">
    <script type="x/boundary" id="fastboot-body-start"></script><aside id="ac-gn-segmentbar" class="ac-gn-segmentbar" lang="en-US" dir="ltr" data-strings="{ &apos;exit&apos;: &apos;Exit&apos;, &apos;view&apos;: &apos;{%STOREFRONT%} Store Home&apos;, &apos;segments&apos;: { &apos;smb&apos;: &apos;Business Store Home&apos;, &apos;eduInd&apos;: &apos;Education Store Home&apos;, &apos;other&apos;: &apos;Store Home&apos; } }"></aside>
<input type="checkbox" id="ac-gn-menustate" class="ac-gn-menustate">
<nav id="ac-globalnav" class="no-js" role="navigation" aria-label="Global" data-hires="false" data-analytics-region="global nav" lang="en-US" dir="ltr" data-www-domain="www.apple.com" data-store-locale="us" data-store-root-path="/us" data-store-api="https://www.apple.com/[storefront]/shop/bag/status" data-search-locale="en_US" data-search-suggestions-api="https://www.apple.com/search-services/suggestions/" data-search-defaultlinks-api="https://www.apple.com/search-services/suggestions/defaultlinks/">
	<div class="ac-gn-content">
		<ul class="ac-gn-header">
			<li class="ac-gn-item ac-gn-menuicon">
				<a href="#ac-gn-menustate" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-open" id="ac-gn-menuanchor-open">
					<span class="ac-gn-menuanchor-label">Global Nav Open Menu</span>
				</a>
				<a href="#" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-close" id="ac-gn-menuanchor-close">
					<span class="ac-gn-menuanchor-label">Global Nav Close Menu</span>
				</a>
				<label class="ac-gn-menuicon-label" for="ac-gn-menustate" aria-hidden="true">
					<span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-top">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-top"></span>
					</span>
					<span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-bottom">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-bottom"></span>
					</span>
				</label>
			</li>
			<li class="ac-gn-item ac-gn-apple">
				<a class="ac-gn-link ac-gn-link-apple" href="https://www.apple.com/" data-analytics-title="apple home" id="ac-gn-firstfocus-small">
					<span class="ac-gn-link-text">Apple</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-bag ac-gn-bag-small" id="ac-gn-bag-small">
				<div class="ac-gn-bag-wrapper">
					<a class="ac-gn-link ac-gn-link-bag" href="https://www.apple.com/us/shop/goto/bag" data-analytics-title="bag" data-analytics-click="bag" aria-label="Shopping Bag" data-string-badge="Shopping Bag with item count :">
						<span class="ac-gn-link-text">Shopping Bag</span>
					</a>
					<span class="ac-gn-bag-badge" aria-hidden="true" data-analytics-title="bag" data-analytics-click="bag">
						<span class="ac-gn-bag-badge-separator"></span>
						<span class="ac-gn-bag-badge-number"></span>
						<span class="ac-gn-bag-badge-unit">+</span>
					</span>
				</div>
				<span class="ac-gn-bagview-caret ac-gn-bagview-caret-large"></span>
			</li>
		</ul>
		<div class="ac-gn-search-placeholder-container" role="search">
			<div class="ac-gn-search ac-gn-search-small">
				<a id="ac-gn-link-search-small" class="ac-gn-link" href="https://www.apple.com/us/search" data-analytics-title="search" data-analytics-intrapage-link aria-label="Search apple.com">
					<div class="ac-gn-search-placeholder-bar">
						<div class="ac-gn-search-placeholder-input">
							<div class="ac-gn-search-placeholder-input-text" aria-hidden="true">
								<div class="ac-gn-link-search ac-gn-search-placeholder-input-icon"></div>
								<span class="ac-gn-search-placeholder">Search apple.com</span>
							</div>
						</div>
						<div class="ac-gn-searchview-close ac-gn-searchview-close-small ac-gn-search-placeholder-searchview-close">
							<span class="ac-gn-searchview-close-cancel" aria-hidden="true">Cancel</span>
						</div>
					</div>
				</a>
			</div>
		</div>
		<ul class="ac-gn-list">
			<li class="ac-gn-item ac-gn-apple">
				<a class="ac-gn-link ac-gn-link-apple" href="https://www.apple.com/" data-analytics-title="apple home" id="ac-gn-firstfocus">
					<span class="ac-gn-link-text">Apple</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-store">
				<a class="ac-gn-link ac-gn-link-store" href="https://www.apple.com/us/shop/goto/store" data-analytics-title="store">
					<span class="ac-gn-link-text">Store</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-mac">
				<a class="ac-gn-link ac-gn-link-mac" href="https://www.apple.com/mac/" data-analytics-title="mac">
					<span class="ac-gn-link-text">Mac</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-ipad">
				<a class="ac-gn-link ac-gn-link-ipad" href="https://www.apple.com/ipad/" data-analytics-title="ipad">
					<span class="ac-gn-link-text">iPad</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
				<a class="ac-gn-link ac-gn-link-iphone" href="https://www.apple.com/iphone/" data-analytics-title="iphone">
					<span class="ac-gn-link-text">iPhone</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-watch">
				<a class="ac-gn-link ac-gn-link-watch" href="https://www.apple.com/watch/" data-analytics-title="watch">
					<span class="ac-gn-link-text">Watch</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-airpods">
				<a class="ac-gn-link ac-gn-link-airpods" href="https://www.apple.com/airpods/" data-analytics-title="airpods">
					<span class="ac-gn-link-text">AirPods</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-tvhome">
				<a class="ac-gn-link ac-gn-link-tvhome" href="https://www.apple.com/tv-home/" data-analytics-title="tv and home">
					<span class="ac-gn-link-text">TV &amp; Home</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-onlyonapple">
				<a class="ac-gn-link ac-gn-link-onlyonapple" href="https://www.apple.com/services/" data-analytics-title="only on apple">
					<span class="ac-gn-link-text">Only on Apple</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-accessories">
				<a class="ac-gn-link ac-gn-link-accessories" href="https://www.apple.com/us/shop/goto/buy_accessories" data-analytics-title="accessories">
					<span class="ac-gn-link-text">Accessories</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-support">
				<a class="ac-gn-link ac-gn-link-support" href="https://support.apple.com" data-analytics-title="support">
					<span class="ac-gn-link-text">Support</span>
				</a>
			</li>
			<li class="ac-gn-item ac-gn-item-menu ac-gn-search" role="search">
				<a id="ac-gn-link-search" class="ac-gn-link ac-gn-link-search" href="https://www.apple.com/us/search" data-analytics-title="search" data-analytics-intrapage-link aria-label="Search apple.com"></a>
			</li>
			<li class="ac-gn-item ac-gn-bag" id="ac-gn-bag">
				<div class="ac-gn-bag-wrapper">
					<a class="ac-gn-link ac-gn-link-bag" href="https://www.apple.com/us/shop/goto/bag" data-analytics-title="bag" data-analytics-click="bag" aria-label="Shopping Bag" data-string-badge="Shopping Bag with item count : {%BAGITEMCOUNT%}">
						<span class="ac-gn-link-text">Shopping Bag</span>
					</a>
					<span class="ac-gn-bag-badge" aria-hidden="true" data-analytics-title="bag" data-analytics-click="bag">
						<span class="ac-gn-bag-badge-separator"></span>
						<span class="ac-gn-bag-badge-number"></span>
						<span class="ac-gn-bag-badge-unit">+</span>
					</span>
				</div>
				<span class="ac-gn-bagview-caret ac-gn-bagview-caret-large"></span>
			</li>
		</ul>
		<aside id="ac-gn-searchview" class="ac-gn-searchview" role="search" data-analytics-region="search">
			<div class="ac-gn-searchview-content">
				<div class="ac-gn-searchview-bar">
					<div class="ac-gn-searchview-bar-wrapper">
						<form id="ac-gn-searchform" class="ac-gn-searchform" action="https://www.apple.com/us/search" method="get">
							<div class="ac-gn-searchform-wrapper">
								<input id="ac-gn-searchform-input" class="ac-gn-searchform-input" type="text" aria-label="Search apple.com" placeholder="Search apple.com" autocorrect="off" autocapitalize="off" autocomplete="off" spellcheck="false" role="combobox" aria-autocomplete="list" aria-expanded="true" aria-owns="quicklinks suggestions">
								<input id="ac-gn-searchform-src" type="hidden" name="src" value="itunes_serp">
								<button id="ac-gn-searchform-submit" class="ac-gn-searchform-submit" type="submit" disabled aria-label="Submit Search"> </button>
								<button id="ac-gn-searchform-reset" class="ac-gn-searchform-reset" type="reset" disabled aria-label="Clear Search">
									<span class="ac-gn-searchform-reset-background"></span>
								</button>
							</div>
						</form>
						<button id="ac-gn-searchview-close-small" class="ac-gn-searchview-close ac-gn-searchview-close-small" aria-label="Cancel Search">
							<span class="ac-gn-searchview-close-cancel" aria-hidden="true">
								Cancel
							</span>
						</button>
					</div>
				</div>
				<aside id="ac-gn-searchresults" class="ac-gn-searchresults" data-string-quicklinks="Quick Links" data-string-suggestions="Suggested Searches" data-string-noresults></aside>
			</div>
			<button id="ac-gn-searchview-close" class="ac-gn-searchview-close" aria-label="Cancel Search">
				<span class="ac-gn-searchview-close-wrapper">
					<span class="ac-gn-searchview-close-left"></span>
					<span class="ac-gn-searchview-close-right"></span>
				</span>
			</button>
		</aside>
		<aside class="ac-gn-bagview" data-analytics-region="bag">
			<div class="ac-gn-bagview-scrim">
				<span class="ac-gn-bagview-caret ac-gn-bagview-caret-small"></span>
			</div>
			<div class="ac-gn-bagview-content" id="ac-gn-bagview-content">
			</div>
		</aside>
	</div>
</nav>
<div class="ac-gn-blur"></div>
<div id="ac-gn-curtain" class="ac-gn-curtain"></div>
<div id="ac-gn-placeholder" class="ac-nav-placeholder"></div>
<div class="ember-view">
  <!---->
<!---->
<!---->
<!---->
<!---->
<!---->


  <main class="selfclear is-mac-theme">
    <!---->

  <input id="localnav-menustate" class="localnav-menustate" type="checkbox">

<nav id="localnav" class="css-sticky we-localnav localnav   we-localnav--mas" data-sticky>
  <div class="localnav-wrapper">
    <div class="localnav-background we-localnav__background"></div>
    <div class="localnav-content we-localnav__content">
      <div class="localnav-title we-localnav__title">
        <a href="https://www.apple.com/macos/" data-we-link-to-exclude><span class="we-localnav__title__product" data-test-we-localnav-store-title>Mac App Store</span></a> <span class="we-localnav__title__qualifier" data-test-we-localnav-preview-title>Preview</span>
      </div>
      <div class="localnav-menu we-localnav__menu we-localnav__menu--mac">
<!---->        <div class="localnav-actions we-localnav__actions">
<!---->            
<!---->
        </div>
      </div>
    </div>
  </div>
</nav>
<span class="we-localnav__shim" aria-hidden="true"></span>
<label id="localnav-curtain" for="localnav-menustate"></label>



<div class="animation-wrapper is-visible">
  
<!---->
<!---->
<!---->
    <section class="l-content-width section section--hero product-hero">
  <div class="l-row">
    <div class="product-hero__media l-column small-5 medium-4 large-3 small-valign-top">
        <picture class="we-artwork we-artwork--downloaded product-hero__artwork we-artwork--fullwidth we-artwork--not-round we-artwork--macos-app-icon" style="--background-color: #000000;" id="ember176285">

        <source srcset="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/246x0w.webp 246w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/217x0w.webp 217w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/230x0w.webp 230w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/492x0w.webp 492w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/434x0w.webp 434w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/460x0w.webp 460w" sizes="(max-width: 734px) 246px, (min-width: 735px) and (max-width: 1068px) 217px, 230px" type="image/webp">

      <source srcset="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/246x0w.png 246w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/217x0w.png 217w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/230x0w.png 230w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/492x0w.png 492w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/434x0w.png 434w, https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/460x0w.png 460w" sizes="(max-width: 734px) 246px, (min-width: 735px) and (max-width: 1068px) 217px, 230px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" class="we-artwork__image ember176285 " alt role="presentation" height="246" width="246">


  <style>
    .ember176285, #ember176285::before {
           width: 246px;
           height: 246px;
         }
         .ember176285::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176285, #ember176285::before {
           width: 217px;
           height: 217px;
         }
         .ember176285::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176285, #ember176285::before {
           width: 230px;
           height: 230px;
         }
         .ember176285::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

    </div>

    <div class="l-column small-7 medium-8 large-9 small-valign-top">
      <header class="product-header app-header product-header--padded-start">
<!---->
        <h1 class="product-header__title app-header__title">
          GoodNotes 5
            <span class="badge badge--product-title">4+</span>
        </h1>

          <h2 class="product-header__subtitle app-header__subtitle">
            Note-Taking &amp; PDF Markup
          </h2>

        <h2 class="product-header__identity app-header__identity">
          <a href="https://apps.apple.com/us/developer/time-base-technology-limited/id424587624" class="link" data-metrics-click="{&quot;actionDetails&quot;:{&quot;type&quot;:&quot;developer&quot;},&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/developer/time-base-technology-limited/id424587624&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;424587624&quot;}" dir="auto">
            Time Base Technology Limited
          </a>
        </h2>

<!---->
          <ul class="product-header__list app-header__list">
              <li class="product-header__list__item">
                <ul class="inline-list inline-list--mobile-compact">
                    <a href="https://apps.apple.com/us/charts/iphone/productivity-apps/6007" class="inline-list__item" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/charts/iphone/productivity-apps/6007&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;LinkToTopCharts&quot;}" data-we-link-to-exclude>
                      #84 in Productivity
                    </a>
                </ul>
              </li>

              <li class="product-header__list__item app-header__list__item--user-rating">
                <ul class="inline-list inline-list--mobile-compact">
                  <li class="inline-list__item">
                    <figure class="we-star-rating" aria-label="4.8 out of 5">
  <span class="we-star-rating-stars-outlines">
    <span class="we-star-rating-stars we-star-rating-stars-5"></span>
  </span>
    <figcaption class="we-rating-count star-rating__count">4.8 • 60.6K Ratings</figcaption>
</figure>

                  </li>
                </ul>
              </li>
          </ul>

        <ul class="product-header__list app-header__list">
<!---->
          <li class="product-header__list__item">
            <ul class="inline-list inline-list--mobile-compact">
                <li class="inline-list__item inline-list__item--bulleted app-header__list__item--price">Free</li>

                <li class="inline-list__item inline-list__item--bulleted app-header__list__item--in-app-purchase">Offers In-App Purchases</li>
                          </ul>
          </li>

<!---->
<!---->        </ul>

<!---->
<!---->
<!---->
<!---->      </header>
    </div>
  </div>
</section>


<!---->
    <section class="l-content-width section section--bordered">
      <div class="section__nav section__nav--align-start">
        <h2 class="section__headline">Screenshots</h2>
        <nav class="gallery-nav">
          <ul class="gallery-nav__items">
              <li class="gallery-nav__item">
                <a id="ember176286" class="ember-view link link--no-decoration is-active" href="/us/app/goodnotes-5/id1444383602?platform=mac">
                  Mac
                </a>
              </li>
              <li class="gallery-nav__item">
                <a id="ember176287" class="ember-view link link--no-decoration" href="/us/app/goodnotes-5/id1444383602?platform=iphone">
                  iPhone
                </a>
              </li>
              <li class="gallery-nav__item">
                <a id="ember176288" class="ember-view link link--no-decoration" href="/us/app/goodnotes-5/id1444383602?platform=ipad">
                  iPad
                </a>
              </li>
          </ul>
        </nav>
      </div>
    <div class="we-screenshot-viewer">
  <div class="we-screenshot-viewer__screenshots">
    <ul class="l-row l-row--peek we-screenshot-viewer__screenshots-list">
          <li class="l-column small-4 medium-4 large-4">
            <picture class="we-artwork we-artwork--downloaded we-artwork--fullwidth we-artwork--screenshot-platform-mac we-artwork--screenshot-version-mac we-artwork--screenshot-orientation-landscape" style="--background-color: #e8ecf2;" id="ember176289">

        <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/643x0w.webp 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/217x0w.webp 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/313x0w.webp 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/1286x0w.webp 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/434x0w.webp 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/626x0w.webp 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/webp">

      <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/643x0w.jpg 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/217x0w.jpg 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/313x0w.jpg 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/1286x0w.jpg 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/434x0w.jpg 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/626x0w.jpg 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/jpeg">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176289  we-artwork__image--lazyload" alt role="presentation" height="401" width="643">


  <style>
    .ember176289, #ember176289::before {
           width: 643px;
           height: 401px;
         }
         .ember176289::before {
           padding-top: 62.363919129082426%;
         }
@media (min-width: 735px) {
           .ember176289, #ember176289::before {
           width: 217px;
           height: 135px;
         }
         .ember176289::before {
           padding-top: 62.21198156682027%;
         }
         }
@media (min-width: 1069px) {
           .ember176289, #ember176289::before {
           width: 313px;
           height: 195px;
         }
         .ember176289::before {
           padding-top: 62.30031948881789%;
         }
         }
  </style>
</picture>

          </li>
          <li class="l-column small-4 medium-4 large-4">
            <picture class="we-artwork we-artwork--downloaded we-artwork--fullwidth we-artwork--screenshot-platform-mac we-artwork--screenshot-version-mac we-artwork--screenshot-orientation-landscape" style="--background-color: #000000;" id="ember176290">

        <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/643x0w.webp 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/217x0w.webp 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/313x0w.webp 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/1286x0w.webp 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/434x0w.webp 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/626x0w.webp 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/webp">

      <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/643x0w.jpg 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/217x0w.jpg 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/313x0w.jpg 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/1286x0w.jpg 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/434x0w.jpg 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/626x0w.jpg 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/jpeg">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176290  we-artwork__image--lazyload" alt role="presentation" height="401" width="643">


  <style>
    .ember176290, #ember176290::before {
           width: 643px;
           height: 401px;
         }
         .ember176290::before {
           padding-top: 62.363919129082426%;
         }
@media (min-width: 735px) {
           .ember176290, #ember176290::before {
           width: 217px;
           height: 135px;
         }
         .ember176290::before {
           padding-top: 62.21198156682027%;
         }
         }
@media (min-width: 1069px) {
           .ember176290, #ember176290::before {
           width: 313px;
           height: 195px;
         }
         .ember176290::before {
           padding-top: 62.30031948881789%;
         }
         }
  </style>
</picture>

          </li>
          <li class="l-column small-4 medium-4 large-4">
            <picture class="we-artwork we-artwork--downloaded we-artwork--fullwidth we-artwork--screenshot-platform-mac we-artwork--screenshot-version-mac we-artwork--screenshot-orientation-landscape" style="--background-color: #ffffff;" id="ember176291">

        <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/643x0w.webp 643w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/217x0w.webp 217w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/313x0w.webp 313w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/1286x0w.webp 1286w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/434x0w.webp 434w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/626x0w.webp 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/webp">

      <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/643x0w.png 643w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/217x0w.png 217w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/313x0w.png 313w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/1286x0w.png 1286w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/434x0w.png 434w, https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/626x0w.png 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176291  we-artwork__image--lazyload" alt role="presentation" height="401" width="643">


  <style>
    .ember176291, #ember176291::before {
           width: 643px;
           height: 401px;
         }
         .ember176291::before {
           padding-top: 62.363919129082426%;
         }
@media (min-width: 735px) {
           .ember176291, #ember176291::before {
           width: 217px;
           height: 135px;
         }
         .ember176291::before {
           padding-top: 62.21198156682027%;
         }
         }
@media (min-width: 1069px) {
           .ember176291, #ember176291::before {
           width: 313px;
           height: 195px;
         }
         .ember176291::before {
           padding-top: 62.30031948881789%;
         }
         }
  </style>
</picture>

          </li>
          <li class="l-column small-4 medium-4 large-4">
            <picture class="we-artwork we-artwork--downloaded we-artwork--fullwidth we-artwork--screenshot-platform-mac we-artwork--screenshot-version-mac we-artwork--screenshot-orientation-landscape" style="--background-color: #feffff;" id="ember176292">

        <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/643x0w.webp 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/217x0w.webp 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/313x0w.webp 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/1286x0w.webp 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/434x0w.webp 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/626x0w.webp 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/webp">

      <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/643x0w.jpg 643w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/217x0w.jpg 217w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/313x0w.jpg 313w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/1286x0w.jpg 1286w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/434x0w.jpg 434w, https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/626x0w.jpg 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/jpeg">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176292  we-artwork__image--lazyload" alt role="presentation" height="401" width="643">


  <style>
    .ember176292, #ember176292::before {
           width: 643px;
           height: 401px;
         }
         .ember176292::before {
           padding-top: 62.363919129082426%;
         }
@media (min-width: 735px) {
           .ember176292, #ember176292::before {
           width: 217px;
           height: 135px;
         }
         .ember176292::before {
           padding-top: 62.21198156682027%;
         }
         }
@media (min-width: 1069px) {
           .ember176292, #ember176292::before {
           width: 313px;
           height: 195px;
         }
         .ember176292::before {
           padding-top: 62.30031948881789%;
         }
         }
  </style>
</picture>

          </li>
          <li class="l-column small-4 medium-4 large-4">
            <picture class="we-artwork we-artwork--downloaded we-artwork--fullwidth we-artwork--screenshot-platform-mac we-artwork--screenshot-version-mac we-artwork--screenshot-orientation-landscape" style="--background-color: #0a1a3c;" id="ember176293">

        <source srcset="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/643x0w.webp 643w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/217x0w.webp 217w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/313x0w.webp 313w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/1286x0w.webp 1286w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/434x0w.webp 434w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/626x0w.webp 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/webp">

      <source srcset="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/643x0w.png 643w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/217x0w.png 217w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/313x0w.png 313w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/1286x0w.png 1286w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/434x0w.png 434w, https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/626x0w.png 626w" sizes="(max-width: 734px) 643px, (min-width: 735px) and (max-width: 1068px) 217px, 313px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176293  we-artwork__image--lazyload" alt role="presentation" height="401" width="643">


  <style>
    .ember176293, #ember176293::before {
           width: 643px;
           height: 401px;
         }
         .ember176293::before {
           padding-top: 62.363919129082426%;
         }
@media (min-width: 735px) {
           .ember176293, #ember176293::before {
           width: 217px;
           height: 135px;
         }
         .ember176293::before {
           padding-top: 62.21198156682027%;
         }
         }
@media (min-width: 1069px) {
           .ember176293, #ember176293::before {
           width: 313px;
           height: 195px;
         }
         .ember176293::before {
           padding-top: 62.30031948881789%;
         }
         }
  </style>
</picture>

          </li>
    </ul>
<!---->  </div>
</div>

  </section>


  <section class="l-content-width section section--bordered">
    <div class="section__description">
      <h2 class="section__headline visuallyhidden">Description</h2>
      <div class="l-row">
          <div class="we-truncate we-truncate--multi-line we-truncate--interactive  l-column small-12 medium-9 large-8" dir>
        

        <p dir="false" data-test-bidi>Use GoodNotes on Mac to access your digital notes wherever you work. With iCloud, your digital notes will be synced on all your devices, making the GoodNotes Mac app the perfect partner to access your digital notes on your computer.<br /><br />NEVER LOSE YOUR NOTES<br />• Search your handwritten notes, typed text, PDF text, document outlines, folder titles, and document titles.<br />• Create unlimited folders and subfolders, or mark your Favorite ones to keep everything organized.<br />• Create custom outlines for easier navigation through your documents.<br />• Add hyperlinks to external websites, videos, articles to build your knowledge map.<br />• Back up all your notes to iCloud, Google Drive, Dropbox, and OneDrive and sync across all devices so you will never lose them.<br /><br />ESCAPE THE LIMITS OF ANALOG PAPER<br />• Move, resize, and rotate your handwriting or change colors.<br />• Draw perfect shapes and lines with the Shape Tool.<br />• Navigate through imported PDFs with existing hyperlinks.<br />• Choose to erase the entire stroke, only parts of it, or only highlighters to leave the ink intact.<br />• Select to edit or move a specific object with the Lasso Tool.<br />• Add, create, or import your stickers, pictures, tables, diagrams, and more with Elements to enrich your notes.<br />• Choose from a large set of beautiful covers and useful paper templates, including Blank Paper, Ruled Paper, Cornell Paper, Checklists, To-dos, Planners, Music Paper, Flashcards, and more.<br />• Upload any PDF or image as a custom notebook cover or paper template for more customization.<br /><br />SHARE, COLLABORATE, &amp; PRESENT FROM YOUR MAC<br />• Present a lecture, a lesson, a business plan, a brainstorming result, or your group study without distractions when you connect your Mac via AirPlay or HDMI to an external screen.<br />• Use Laser Pointer on your iPad to guide your audience’s attention during your presentation.<br />• Collaborate with others on the same document using a sharable link.<br /><br />---<br /><br />Website: www.goodnotes.com<br />Twitter: @goodnotesapp<br />Instagram: @goodnotes.app<br />Pinterest: @goodnotesapp<br />TikTok: @goodnotesapp</p>
    


<!----></div>


      </div>
    </div>
  </section>

<!---->
  <section class="l-content-width section section--bordered whats-new">
    <div class="section__nav section__nav--small">
      <h2 class="whats-new__headline">What’s New</h2>
        <div class="version-history">
<!---->
<!----></div>

    </div>
    <div class="l-row whats-new__content">
        <div class="l-column small-12 medium-3 large-4 small-valign-top whats-new__latest">
          <div class="l-row">
            <time data-test-we-datetime datetime="2022-08-08T00:00:00.000Z" aria-label="August 8, 2022" class="" >Aug 8, 2022</time>
            <p class="l-column small-6 medium-12 whats-new__latest__version">Version 5.9.32</p>
          </div>
        </div>
      <div class="l-column small-12 medium-9 large-8 small-valign-top">
          <div class="we-truncate we-truncate--multi-line we-truncate--interactive " dir>
        

        <p dir="false" data-test-bidi>- Title suggestions when you leave an Untitled document<br />- Preserve typing attributes when deleting to a new line in a text box<br />- Fixing bugs in the account creation process<br />- Improvements on the notes collection flow</p>
    


<!----></div>


      </div>
    </div>
  </section>

      <section class="l-content-width section section--bordered">
      <div class="section__nav">
        <h2 class="section__headline">
          Ratings and Reviews
        </h2>

        <!---->
      </div>

        <div class="we-customer-ratings lockup">
  <div class="l-row">
    <div class="we-customer-ratings__stats l-column small-4 medium-6 large-4">
      <div class="we-customer-ratings__averages"><span class="we-customer-ratings__averages__display">4.8</span> out of 5</div>
        <div class="we-customer-ratings__count small-hide medium-show">60.6K Ratings</div>
    </div>
    <div class=" l-column small-8 medium-6 large-4">
      <figure class="we-star-bar-graph">
          <div class="we-star-bar-graph__row">
            <span class="we-star-bar-graph__stars we-star-bar-graph__stars--5"></span>
            <div class="we-star-bar-graph__bar">
              <div class="we-star-bar-graph__bar__foreground-bar" style="width: 89%;"></div>
            </div>
          </div>
          <div class="we-star-bar-graph__row">
            <span class="we-star-bar-graph__stars we-star-bar-graph__stars--4"></span>
            <div class="we-star-bar-graph__bar">
              <div class="we-star-bar-graph__bar__foreground-bar" style="width: 8%;"></div>
            </div>
          </div>
          <div class="we-star-bar-graph__row">
            <span class="we-star-bar-graph__stars we-star-bar-graph__stars--3"></span>
            <div class="we-star-bar-graph__bar">
              <div class="we-star-bar-graph__bar__foreground-bar" style="width: 1%;"></div>
            </div>
          </div>
          <div class="we-star-bar-graph__row">
            <span class="we-star-bar-graph__stars we-star-bar-graph__stars--2"></span>
            <div class="we-star-bar-graph__bar">
              <div class="we-star-bar-graph__bar__foreground-bar" style="width: 0%;"></div>
            </div>
          </div>
          <div class="we-star-bar-graph__row">
            <span class="we-star-bar-graph__stars "></span>
            <div class="we-star-bar-graph__bar">
              <div class="we-star-bar-graph__bar__foreground-bar" style="width: 1%;"></div>
            </div>
          </div>
      </figure>
        <p class="we-customer-ratings__count medium-hide">60.6K Ratings</p>
    </div>
  </div>
</div>


      <div class="l-row l-row--peek">
              
              <div id="ember176295" class="small-valign-top l-column--equal-height l-column--small-hide small-hide medium-show l-column small-4 medium-6 large-4">
  <div>
<!---->
<!----></div>


  
                <div class="we-editor-notes lockup">
    <div class="we-editor-notes__editor we-editor-notes--editor-choice">
      <h3 class="we-editor-notes__editor__editor-choice">Editors’ Choice</h3>
    </div>

      <div class="we-truncate we-truncate--multi-line we-truncate--interactive " dir>
        

        <p dir="false" data-test-bidi>GoodNotes 5 is a combination digital notepad and PDF markup tool. And it’s very good at both. As a notepad, GoodNotes covers all the bases. Create notes with your keyboard or handwrite them with Apple Pencil. The app transforms your hand-drawn shapes into geometrically perfect ones. It’s also capable of recognizing your handwriting (even when you can’t) and converting it to text. </p>
    


<!----></div>


</div>

              
</div>

          
    
              <div id="ember176297" class="small-valign-top l-column--equal-height l-column small-4 medium-6 large-4">
  <div>
<!---->
<!----></div>


  
                <div aria-labelledby="we-customer-review-13497" class="we-customer-review lockup">
  <figure class="we-star-rating we-customer-review__rating we-star-rating--large" aria-label="5 out of 5">
  <span class="we-star-rating-stars-outlines">
    <span class="we-star-rating-stars we-star-rating-stars-5"></span>
  </span>
<!----></figure>


  <div class="we-customer-review__header we-customer-review__header--user">
      <span class="we-truncate we-truncate--single-line  we-customer-review__user" dir="ltr">
    Jewelsummoner
</span>



    <span class="we-customer-review__separator">, </span>

    <time data-test-customer-review-date datetime="2022-04-22T00:05:59.000Z" aria-label="April 21, 2022" class="we-customer-review__date" >04/21/2022</time>
  </div>

    <h3 class="we-truncate we-truncate--single-line  we-customer-review__title" dir="ltr" id="we-customer-review-13497">
    This is an excellent app! I only have one pet peeve…
</h3>



      <blockquote class="we-truncate we-truncate--multi-line we-truncate--interactive  we-customer-review__body" dir>
        

        <p dir="false" data-test-bidi>Honestly, I really love using this app! It is very well thought out and I can use it for many different kinds of workflows. Great app if you are considering starting/ transitioning to digital note-taking/ journaling. I enjoy taking handwritten notes in it with my apple pencil and being able to easily import pdfs that I can mark up is great. I enjoy the elements feature, which allows me to add custom sticker that I use all the time as post-its or stickers for journaling. Plus there are som many other excellent features, such as being able to search in your hand written notes and the ability to back-up your notebooks, to name a few.<br /><br />I only have one pet peeve when it comes to this app.  It does have a typing feature, but it’s a bit clunky and not very intuitive. When I am doing journaling, it is very nice to put text exactly where I want it, so I don’t want the feature to go away. But having to use the text boxes for more type-intensive tasks can be a bit annoying at times. I would be nice if there was a feature you could enable within the notebook where if you could type in a notebook like a normal word document. My work around is typing something ahead of time in a word document and then importing it into the app, but honestly, it would be nice if I didn’t have to do that (maybe I am being too picky)… Otherwise, this a really excellent, solid app that I recommend highly to folks for note-taking.</p>
    


<!----></blockquote>



<!----></div>

              
</div>

          
    
              <div id="ember176299" class="small-valign-top l-column--equal-height l-column small-4 medium-6 large-4">
  <div>
<!---->
<!----></div>


  
                <div aria-labelledby="we-customer-review-13498" class="we-customer-review lockup">
  <figure class="we-star-rating we-customer-review__rating we-star-rating--large" aria-label="5 out of 5">
  <span class="we-star-rating-stars-outlines">
    <span class="we-star-rating-stars we-star-rating-stars-5"></span>
  </span>
<!----></figure>


  <div class="we-customer-review__header we-customer-review__header--user">
      <span class="we-truncate we-truncate--single-line  we-customer-review__user" dir="ltr">
    Swipe69
</span>



    <span class="we-customer-review__separator">, </span>

    <time data-test-customer-review-date datetime="2022-02-18T20:52:19.000Z" aria-label="February 18, 2022" class="we-customer-review__date" >02/18/2022</time>
  </div>

    <h3 class="we-truncate we-truncate--single-line  we-customer-review__title" dir="ltr" id="we-customer-review-13498">
    Amazing
</h3>



      <blockquote class="we-truncate we-truncate--multi-line we-truncate--interactive  we-customer-review__body" dir>
        

        <p dir="false" data-test-bidi>I absolutely love this app. I use this more for pdf creation and personal journaling than anything else. If I were to add or change anything, it would be to include a colored pencil or crayon  writing utensil among the hi-liter, and various pen-pen, brush-pen, etc. I may be mistaken, but doesn’t the primary photo used to advertise GoodNotes is, in fact, written and drawn with what looks to be a colored pencil. How odd that is not a function. <br /><br />Also, voice recorder that syncs with ur written notes, as similarly done by “Colla-note” and “notability” would be a highly desired note taking tool. Other features that would rock my world would to insert in-notebook hyperlinks so that, say for example, I nay make a table of contents at the start if my 300 page notebook. It would be fabulous if I could make some text or a insert picture that would hyperlink me to page 288 where I want to read my notes on socio-economic hierarchies. Something like Collanote did. And also the ability to embed videos or audio in a note would really be beneficial.<br /><br />Despite that slight suggestion party I just threw out there, GoodNotes, is the note taking app I continue to consistently return to. No other note app seems to so effortlessly and perfectly change, adjust, create, or mark up pdfs as well as GoodNotes. No other note app has such an amazing array of paper to write on. Thank you GoodNotes. You are some damned Good notes! You are my most used app among all my devices.</p>
    


<!----></blockquote>



<!----></div>

              
</div>

          

      </div>

        <div class="l-row l-row--margin-top medium-hide">
              
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          
    
<!---->          

        </div>
    </section>


<!---->
<!---->
<!---->
  <section class="l-content-width section section--bordered app-privacy">
  <div class="section__nav">
    <h2 class="section__headline section__headline--app-privacy">
      App Privacy
    </h2>

    <div class="app-privacy--modal privacy-type--modal">
<!---->
<!----></div>


  </div>

  <p>
    The developer, <span class="app-privacy__developer-name">Time Base Technology Limited</span>, indicated that the app’s privacy practices may include handling of data as described below. For more information, see the <a href="https://goodnotes.com/privacy-policy/">developer’s privacy policy</a>.
  </p>

  <div class="app-privacy__cards">
      <div class="app-privacy__card">
        <div class="privacy-type__icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" aria-hidden="true"><path d="M7.36 17l3.208 3.208A24.23 24.23 0 007.39 32.235c0 13.44 10.93 24.374 24.366 24.374 4.374 0 8.483-1.159 12.035-3.186L47 56.631A28.616 28.616 0 0131.756 61C15.874 61 3 48.122 3 32.235A28.637 28.637 0 017.36 17zM9.77 6.642l47.588 47.606a2.204 2.204 0 010 3.11 2.206 2.206 0 01-3.111 0L6.632 9.753c-.829-.8-.856-2.283 0-3.11.826-.828 2.253-.886 3.138 0zM32.238 3C48.123 3 61 15.878 61 31.761c0 5.597-1.599 10.82-4.364 15.239l-3.208-3.209a24.221 24.221 0 003.182-12.03c0-13.437-10.934-24.37-24.372-24.37a24.223 24.223 0 00-12.03 3.18L17 7.363A28.628 28.628 0 0132.238 3zm-7.492 31L36 45H18.373C17.44 45 17 44.418 17 43.572c0-2.224 2.5-7.11 7.746-9.572zm6.915-20C35.733 14 39 17.634 39 22.002c0 2.419-.874 4.529-2.281 5.998L26 16.923C27.343 15.145 29.376 14 31.661 14z"/></svg>
        </div>
        <h3 class="privacy-type__heading">Data Not Linked to You</h3>
        <p class="privacy-type__description">The following data may be collected but it is not linked to your identity:</p>
          <ul class="privacy-type__items">
              <li classs="privacy-type__item">
                <span class="privacy-type__grid">
                  <span class="privacy-type__grid-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" class="privacy-type__glyph privacy-type__glyph--identifiers" aria-hidden="true"><path d="M52.367 24h-14.76C36.704 24 36 23.342 36 22.51c0-.835.705-1.51 1.606-1.51h14.761c.905 0 1.633.675 1.633 1.51 0 .832-.728 1.49-1.633 1.49m0 10h-14.76C36.704 34 36 33.35 36 32.52c0-.85.705-1.52 1.606-1.52h14.761c.905 0 1.633.67 1.633 1.52 0 .83-.728 1.48-1.633 1.48m0 9h-14.76C36.704 43 36 42.33 36 41.484c0-.83.705-1.484 1.606-1.484h14.761c.905 0 1.633.654 1.633 1.484C54 42.33 53.272 43 52.367 43m-24.04 0H12.66C10.7 43 10 42.459 10 41.401 10 38.288 14.028 34 20.493 34 26.973 34 31 38.288 31 41.401 31 42.46 30.305 43 28.328 43m-7.321-22C23.673 21 26 23.31 26 26.425 26 29.58 23.686 32 21.007 32 18.314 32 16 29.58 16 26.452 15.987 23.359 18.327 21 21.007 21m32.158-10h-42.33C5.645 11 3 13.566 3 18.645V45.33C3 50.408 5.644 53 10.835 53h42.33C58.355 53 61 50.408 61 45.329V18.645C61 13.592 58.356 11 53.165 11"/></svg>
                  </span>
                  <span class="privacy-type__grid-content privacy-type__data-category-heading">Identifiers</span>
                </span>
              </li>
              <li classs="privacy-type__item">
                <span class="privacy-type__grid">
                  <span class="privacy-type__grid-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" class="privacy-type__glyph privacy-type__glyph--usage-data" aria-hidden="true"><path d="M49.925 53.844h7.098c3.287 0 4.978-1.572 4.978-4.692V14.52c0-3.12-1.69-4.717-4.978-4.717h-7.098c-3.239 0-4.93 1.596-4.93 4.717v34.632c0 3.12 1.691 4.692 4.93 4.692zm-21.412 0h7.097c3.287 0 4.955-1.572 4.955-4.692V21.475c0-3.12-1.668-4.717-4.955-4.717h-7.097c-3.264 0-4.955 1.596-4.955 4.717v27.677c0 3.12 1.691 4.692 4.955 4.692zm-21.413 0h7.074c3.287 0 4.978-1.572 4.978-4.692V28.406c0-3.12-1.691-4.716-4.978-4.716H7.1c-3.264 0-4.955 1.596-4.955 4.716v20.746c0 3.12 1.691 4.692 4.955 4.692z"/></svg>
                  </span>
                  <span class="privacy-type__grid-content privacy-type__data-category-heading">Usage Data</span>
                </span>
              </li>
              <li classs="privacy-type__item">
                <span class="privacy-type__grid">
                  <span class="privacy-type__grid-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" class="privacy-type__glyph privacy-type__glyph--diagnostics" aria-hidden="true"><path d="M29.47 61.996h5.234c1.48 0 2.56-.882 2.873-2.332l1.48-6.258c1.109-.37 2.19-.797 3.128-1.252l5.49 3.357c1.223.768 2.617.654 3.64-.37l3.67-3.64c1.024-1.025 1.166-2.504.341-3.727l-3.356-5.433c.483-.995.91-2.02 1.223-3.043l6.314-1.508c1.45-.313 2.304-1.393 2.304-2.872v-5.149c0-1.45-.853-2.531-2.304-2.844l-6.257-1.508c-.37-1.166-.825-2.218-1.223-3.1l3.356-5.518c.796-1.223.711-2.617-.341-3.64l-3.727-3.67c-1.052-.967-2.303-1.138-3.555-.427l-5.575 3.442c-.91-.483-1.962-.91-3.129-1.28l-1.479-6.343c-.313-1.45-1.393-2.332-2.873-2.332h-5.233c-1.48 0-2.56.882-2.901 2.332l-1.48 6.286c-1.109.37-2.19.797-3.157 1.309l-5.518-3.414c-1.251-.71-2.531-.568-3.584.427l-3.697 3.67c-1.053 1.023-1.166 2.417-.342 3.64l3.328 5.518c-.37.882-.825 1.934-1.194 3.1l-6.258 1.508c-1.45.313-2.304 1.394-2.304 2.844v5.149c0 1.479.853 2.56 2.304 2.872l6.315 1.508c.312 1.024.739 2.048 1.194 3.043L8.85 47.774c-.853 1.223-.682 2.702.342 3.726l3.64 3.641c1.024 1.024 2.446 1.138 3.67.37l5.46-3.357c.968.455 2.02.882 3.13 1.252l1.479 6.258c.34 1.45 1.422 2.332 2.9 2.332zm2.618-19.683c-5.518 0-10.04-4.551-10.04-10.07 0-5.489 4.522-10.011 10.04-10.011 5.518 0 10.04 4.522 10.04 10.012 0 5.518-4.522 10.069-10.04 10.069z"/></svg>
                  </span>
                  <span class="privacy-type__grid-content privacy-type__data-category-heading">Diagnostics</span>
                </span>
              </li>
          </ul>
      </div>
  </div>

    <p class="app-privacy__learn-more">Privacy practices may vary, for example, based on the features you use or your age. <a href="https://apps.apple.com/story/id1538632801">Learn More</a></p>
</section>


<section class="l-content-width section section--bordered section--information">
  <div>
    <h2 class="section__headline">Information</h2>
    <dl class="information-list information-list--app medium-columns l-row">
        <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
          <dt class="information-list__item__term medium-valign-top">Seller</dt>
          <dd class="information-list__item__definition">
            Time Base Technology Limited
          </dd>
        </div>
        <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
          <dt class="information-list__item__term medium-valign-top">Size</dt>
          <dd class="information-list__item__definition" aria-label="423.3 megabytes">423.3 MB</dd>
        </div>
        <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
          <dt class="information-list__item__term medium-valign-top">Category</dt>
          <dd class="information-list__item__definition">
              <a href="https://itunes.apple.com/us/genre/id6007" class="link" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://itunes.apple.com/us/genre/id6007&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;GenrePage&quot;}">
                Productivity
              </a>
          </dd>
        </div>
      <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
        <dt class="information-list__item__term medium-valign-top">Compatibility</dt>
          <dd class="information-list__item__definition">
              <dl class="information-list__item__definition__item">
                <dt class="information-list__item__definition__item__term">
                  iPhone
                </dt>
                <dd class="information-list__item__definition__item__definition">Requires iOS 13.0 or later.
                </dd>
              </dl>
              <dl class="information-list__item__definition__item">
                <dt class="information-list__item__definition__item__term">
                  iPad
                </dt>
                <dd class="information-list__item__definition__item__definition">Requires iPadOS 13.0 or later.
                </dd>
              </dl>
              <dl class="information-list__item__definition__item">
                <dt class="information-list__item__definition__item__term">
                  iPod touch
                </dt>
                <dd class="information-list__item__definition__item__definition">Requires iOS 13.0 or later.
                </dd>
              </dl>
              <dl class="information-list__item__definition__item">
                <dt class="information-list__item__definition__item__term">
                  Mac
                </dt>
                <dd class="information-list__item__definition__item__definition">Requires macOS 10.15 or later.
                </dd>
              </dl>
          </dd>
      </div>
<!---->      <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
        <dt class="information-list__item__term medium-valign-top">Languages</dt>
          <dd class="we-truncate we-truncate--multi-line we-truncate--interactive  information-list__item__definition" dir>
        

                    <p data-test-bidi>English, Dutch, French, German, Italian, Japanese, Korean, Portuguese, Russian, Simplified Chinese, Spanish, Thai, Traditional Chinese, Turkish</p>

    


<!----></dd>


      </div>
      <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
        <dt class="information-list__item__term medium-valign-top">Age Rating</dt>
        <dd class="information-list__item__definition">
            4+

<!---->        </dd>
      </div>
<!---->      <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
        <dt class="information-list__item__term medium-valign-top">Copyright</dt>
        <dd class="information-list__item__definition information-list__item__definition--copyright">© 2011-2022 GoodNotes Limited</dd>
      </div>
        <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
          <dt class="information-list__item__term medium-valign-top">Price</dt>
          <dd class="information-list__item__definition">Free</dd>
        </div>
        <div class="information-list__item l-column small-12 medium-6 large-4 small-valign-top">
          <dt class="information-list__item__term medium-valign-top">In-App Purchases</dt>
          <dd class="information-list__item__definition">
            <ol role="table" class="list-with-numbers">
              <div>
    
                <li class="list-with-numbers__item">
                  <span class="list-with-numbers__item__title"><span class="truncate-single-line truncate-single-line--block">Full Version Unlock</span></span>
                  <span class="list-with-numbers__item__price small-hide medium-show-tablecell">$7.99</span>
                </li>
              
<!----></div>

            </ol>
          </dd>
        </div>
      
    </dl>
  </div>
  <div class="small-hide medium-show">
    <ul class="inline-list inline-list--app-extensions">
        <li class="inline-list__item inline-list__item--margin-inline-end-large">
          <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionDetails&quot;:{&quot;type&quot;:&quot;developer&quot;},&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;@@url@@&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;@@id@@&quot;}" href="https://www.goodnotes.com">
            Developer Website
          </a>
        </li>
        <li class="inline-list__item inline-list__item--margin-inline-end-large">
          <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;LinkToAppSupport&quot;}" href="https://support.goodnotes.com/hc/en-us/categories/360000080575-GoodNotes-5">
            App Support
          </a>
        </li>
<!---->        <li class="inline-list__item inline-list__item--margin-inline-end-large">
          <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;LinkToPrivacyPolicy&quot;}" href="https://goodnotes.com/privacy-policy/">
            Privacy Policy
          </a>
        </li>
    </ul>
  </div>
</section>

<section class="section section--link-list l-content-width medium-hide">
  <ul class="link-list link-list--a">
      <li class="link-list__item link-list__item--a">
        <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionDetails&quot;:{&quot;type&quot;:&quot;developer&quot;},&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;@@url@@&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;@@id@@&quot;}" href="https://www.goodnotes.com">
          Developer Website
        </a>
      </li>
      <li class="link-list__item link-list__item--a">
        <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;LinkToAppSupport&quot;}" href="https://support.goodnotes.com/hc/en-us/categories/360000080575-GoodNotes-5">
          App Support
        </a>
      </li>
<!---->      <li class="link-list__item link-list__item--a">
        <a class="link icon icon-after icon-external" data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;targetType&quot;:&quot;link&quot;,&quot;targetId&quot;:&quot;LinkToPrivacyPolicy&quot;}" href="https://goodnotes.com/privacy-policy/">
          Privacy Policy
        </a>
      </li>
  </ul>
</section>

  <section class="l-content-width section section--bordered">
    <div class="section__nav">
      <h2 class="section__headline">Supports</h2>
    </div>
    <ul class="supports-list l-row">
        <li class="supports-list__item l-column l-column--grid small-12 medium-6 large-4">
          <img src="/assets/images/supports/supports-FamilySharing@2x-f58f31bc78fe9fe7be3565abccbecb34.png" class="supports-list__item__artwork" alt>
          <div class="supports-list__item__copy">
              <h3 class="we-truncate we-truncate--single-line  supports-list__item__copy__heading" dir="ltr">
    Family Sharing
</h3>


              <h4 class="we-truncate we-truncate--multi-line we-truncate--interactive  supports-list__item__copy__description" dir>
        

                    <p data-test-bidi>Some in‑app purchases, including subscriptions, may be shareable with your family group when Family Sharing is enabled.</p>

    


<!----></h4>


          </div>
        </li>
    </ul>
  </section>

    <section class="l-content-width section section--bordered">
      <div class="section__nav">
        <h2 class="section__headline">
          Featured In
        </h2>
        <!---->
      </div>

      <div class="l-row l-row--peek">
            
    
            <a href="https://apps.apple.com/us/story/id1447882511" class="we-lockup targeted-link l-column small-4 medium-6 large-4 we-lockup--shelf-align-top" aria-label="GoodNotes 5 Does It All. Take smart notes and mark up PDFs in the same place.." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/story/id1447882511&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1447882511&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfFeaturedIn&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth" style="--background-color: #eeede8;" dir="ltr" id="ember176302">

        <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/560x374fo.webp 560w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/336x224fo.webp 336w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/313x209fo.webp 313w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/1120x747fo.webp 1120w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/672x448fo.webp 672w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/626x418fo.webp 626w" sizes="(max-width: 734px) 560px, (min-width: 735px) and (max-width: 1068px) 336px, 313px" type="image/webp">

      <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/560x374fo.jpg 560w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/336x224fo.jpg 336w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/313x209fo.jpg 313w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/1120x747fo.jpg 1120w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/672x448fo.jpg 672w, https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/626x418fo.jpg 626w" sizes="(max-width: 734px) 560px, (min-width: 735px) and (max-width: 1068px) 336px, 313px" type="image/jpeg">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176302  we-artwork__image--lazyload" alt role="presentation" height="373.3333333333333" width="560">


  <style>
    .ember176302, #ember176302::before {
           width: 560px;
           height: 373.3333333333333px;
         }
         .ember176302::before {
           padding-top: 66.66666666666666%;
         }
@media (min-width: 735px) {
           .ember176302, #ember176302::before {
           width: 336px;
           height: 224px;
         }
         .ember176302::before {
           padding-top: 66.66666666666666%;
         }
         }
@media (min-width: 1069px) {
           .ember176302, #ember176302::before {
           width: 313px;
           height: 208.66666666666666px;
         }
         .ember176302::before {
           padding-top: 66.66666666666666%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
        <div class="truncate-single-line we-lockup__eyebrow">APP
OF THE
DAY</div>

<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--single-line  targeted-link__target" dir="ltr">
    GoodNotes 5 Does It All
</div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Take smart notes and mark up PDFs in the same place.
</div>


<!---->    </div>
  </div>
</a>


        



      </div>
    </section>

<!---->
    <section class="l-content-width section section--bordered">
      <div class="section__nav">
        <h2 class="section__headline">
          You Might Also Like
        </h2>
        <!---->
      </div>

      <div class="l-row l-row--peek">
            
    
            <a href="https://apps.apple.com/us/app/quicknotes-x/id1021085778" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="QuickNotes X. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/quicknotes-x/id1021085778&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1021085778&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #ffffff;" dir="ltr" id="ember176303">

        <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/320x0w.webp 320w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/157x0w.webp 157w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/146x0w.webp 146w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/640x0w.webp 640w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/314x0w.webp 314w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/320x0w.png 320w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/157x0w.png 157w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/146x0w.png 146w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/640x0w.png 640w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/314x0w.png 314w, https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176303  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176303, #ember176303::before {
           width: 320px;
           height: 320px;
         }
         .ember176303::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176303, #ember176303::before {
           width: 157px;
           height: 157px;
         }
         .ember176303::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176303, #ember176303::before {
           width: 146px;
           height: 146px;
         }
         .ember176303::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>QuickNotes X</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        

    
    
            <a href="https://apps.apple.com/us/app/nebo-notes-pdf-annotations/id1119601770" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="Nebo: Notes &amp; PDF Annotations. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/nebo-notes-pdf-annotations/id1119601770&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1119601770&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #ffffff;" dir="ltr" id="ember176304">

        <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/320x0w.webp 320w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/157x0w.webp 157w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/146x0w.webp 146w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/640x0w.webp 640w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/314x0w.webp 314w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/320x0w.png 320w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/157x0w.png 157w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/146x0w.png 146w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/640x0w.png 640w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/314x0w.png 314w, https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176304  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176304, #ember176304::before {
           width: 320px;
           height: 320px;
         }
         .ember176304::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176304, #ember176304::before {
           width: 157px;
           height: 157px;
         }
         .ember176304::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176304, #ember176304::before {
           width: 146px;
           height: 146px;
         }
         .ember176304::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>Nebo: Notes &amp; PDF Annotations</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        

    
    
            <a href="https://apps.apple.com/us/app/notion-notes-docs-tasks/id1232780281" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="Notion - notes, docs, tasks. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/notion-notes-docs-tasks/id1232780281&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1232780281&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #ffffff;" dir="ltr" id="ember176305">

        <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/320x0w.webp 320w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/157x0w.webp 157w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/146x0w.webp 146w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/640x0w.webp 640w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/314x0w.webp 314w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/320x0w.png 320w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/157x0w.png 157w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/146x0w.png 146w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/640x0w.png 640w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/314x0w.png 314w, https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176305  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176305, #ember176305::before {
           width: 320px;
           height: 320px;
         }
         .ember176305::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176305, #ember176305::before {
           width: 157px;
           height: 157px;
         }
         .ember176305::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176305, #ember176305::before {
           width: 146px;
           height: 146px;
         }
         .ember176305::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>Notion - notes, docs, tasks</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        

    
    
            <a href="https://apps.apple.com/us/app/notes-writer-take-good-notes/id1423643723" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="Notes Writer -Take Good Notes!. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/notes-writer-take-good-notes/id1423643723&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1423643723&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #ffffff;" dir="ltr" id="ember176306">

        <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/320x0w.webp 320w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/157x0w.webp 157w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/146x0w.webp 146w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/640x0w.webp 640w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/314x0w.webp 314w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/320x0w.png 320w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/157x0w.png 157w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/146x0w.png 146w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/640x0w.png 640w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/314x0w.png 314w, https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176306  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176306, #ember176306::before {
           width: 320px;
           height: 320px;
         }
         .ember176306::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176306, #ember176306::before {
           width: 157px;
           height: 157px;
         }
         .ember176306::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176306, #ember176306::before {
           width: 146px;
           height: 146px;
         }
         .ember176306::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>Notes Writer -Take Good Notes!</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        

    
    
            <a href="https://apps.apple.com/us/app/penbook/id1473064295" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="Penbook. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/penbook/id1473064295&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1473064295&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #ccd4d7;" dir="ltr" id="ember176307">

        <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/320x0w.webp 320w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/157x0w.webp 157w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/146x0w.webp 146w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/640x0w.webp 640w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/314x0w.webp 314w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/320x0w.png 320w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/157x0w.png 157w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/146x0w.png 146w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/640x0w.png 640w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/314x0w.png 314w, https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176307  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176307, #ember176307::before {
           width: 320px;
           height: 320px;
         }
         .ember176307::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176307, #ember176307::before {
           width: 157px;
           height: 157px;
         }
         .ember176307::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176307, #ember176307::before {
           width: 146px;
           height: 146px;
         }
         .ember176307::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>Penbook</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        

    
    
            <a href="https://apps.apple.com/us/app/pencil-planner-calendar-pro/id1505638148" class="we-lockup targeted-link l-column small-2 medium-3 large-2 we-lockup--shelf-align-top we-lockup--in-app-shelf" aria-label="Pencil Planner &amp; Calendar Pro. Productivity." data-metrics-click="{&quot;actionType&quot;:&quot;navigate&quot;,&quot;actionUrl&quot;:&quot;https://apps.apple.com/us/app/pencil-planner-calendar-pro/id1505638148&quot;,&quot;targetType&quot;:&quot;card&quot;,&quot;targetId&quot;:&quot;1505638148&quot;}" data-metrics-location="{&quot;locationType&quot;:&quot;shelfCustomersAlsoBoughtApps&quot;}">
<!---->      <div class="we-lockup__overlay">
          <picture class="we-artwork we-artwork--downloaded we-lockup__artwork we-artwork--lockup we-artwork--fullwidth we-artwork--ios-app-icon" style="--background-color: #6780ff;" dir="ltr" id="ember176308">

        <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/320x0w.webp 320w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/157x0w.webp 157w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/146x0w.webp 146w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/640x0w.webp 640w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/314x0w.webp 314w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/292x0w.webp 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/webp">

      <source srcset="https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/320x0w.png 320w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/157x0w.png 157w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/146x0w.png 146w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/640x0w.png 640w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/314x0w.png 314w, https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/292x0w.png 292w" sizes="(max-width: 734px) 320px, (min-width: 735px) and (max-width: 1068px) 157px, 146px" type="image/png">

      <img src="/assets/artwork/1x1-42817eea7ade52607a760cbee00d1495.gif" decoding="async" loading="lazy" class="we-artwork__image ember176308  we-artwork__image--lazyload" alt role="presentation" height="320" width="320">


  <style>
    .ember176308, #ember176308::before {
           width: 320px;
           height: 320px;
         }
         .ember176308::before {
           padding-top: 100%;
         }
@media (min-width: 735px) {
           .ember176308, #ember176308::before {
           width: 157px;
           height: 157px;
         }
         .ember176308::before {
           padding-top: 100%;
         }
         }
@media (min-width: 1069px) {
           .ember176308, #ember176308::before {
           width: 146px;
           height: 146px;
         }
         .ember176308::before {
           padding-top: 100%;
         }
         }
  </style>
</picture>

        <div class="we-lockup__material"></div>
        <div class="we-lockup__joe-color"></div>
      </div>

  <div class="we-lockup__copy">
<!---->    <div class="we-lockup__text">
<!---->
<!---->
        <div class="we-lockup__title ">
              <div class="we-truncate we-truncate--multi-line  targeted-link__target" dir>
        

        <p dir="false" data-test-bidi>Pencil Planner &amp; Calendar Pro</p>
    


<!----></div>


        </div>

          <div class="we-truncate we-truncate--single-line  we-lockup__subtitle" dir="ltr">
    Productivity
</div>


<!---->    </div>
  </div>
</a>


        



      </div>
    </section>


<!---->

<!----></div>



  </main>
</div><footer id="ac-globalfooter" class="no-js" role="contentinfo" lang="en-US" dir="ltr"><div class="ac-gf-content"><section class="ac-gf-footer">
	<div class="ac-gf-footer-shop" x-ms-format-detection="none">
		More ways to shop: <a href="https://www.apple.com/retail/" data-analytics-title="find an apple store">Find an Apple Store</a> or <a href="https://locate.apple.com/" data-analytics-title="other retailers or resellers" data-analytics-exit-link>other retailer</a> near you. <span class="nowrap">Or call 1-800-MY-APPLE.</span>
	</div>
	<div class="ac-gf-footer-locale">
		<a class="ac-gf-footer-locale-link" href="https://www.apple.com/choose-country-region/" title="Choose your country or region" aria-label="Choose your country or region" data-analytics-title="choose your country">Choose your country or region</a>
	</div>
	<div class="ac-gf-footer-legal">
		<div class="ac-gf-footer-legal-copyright">Copyright &#xA9; 2022 Apple Inc. All rights reserved.
		</div>
		<div class="ac-gf-footer-legal-links">
			<a class="ac-gf-footer-legal-link" href="https://www.apple.com/legal/privacy/" data-analytics-title="privacy policy">Privacy Policy</a>
			<a class="ac-gf-footer-legal-link" href="https://www.apple.com/legal/internet-services/terms/site.html" data-analytics-title="terms of use">Terms of Use</a>
			<a class="ac-gf-footer-legal-link" href="https://www.apple.com/us/shop/goto/help/sales_refunds" data-analytics-title="sales and refunds">Sales and Refunds</a>
			<a class="ac-gf-footer-legal-link" href="https://www.apple.com/legal/" data-analytics-title="legal">Legal</a>
			<a class="ac-gf-footer-legal-link" href="https://www.apple.com/sitemap/" data-analytics-title="site map">Site Map</a>
		</div>
	</div>
</section>
</div></footer>
<script type="fastboot/shoebox" id="shoebox-language-code">"en-us"</script><script type="fastboot/shoebox" id="shoebox-ember-localizer">{"en-us":{"WEA.AppBundlePages.AppBundle":"App Bundle @@appPrice@@","WEA.AppBundlePages.NumberAppsInBundle.Many":"@@count@@ Apps in This Bundle","WEA.AppBundlePages.PurchasedSeparately":"Purchased Separately: @@price@@","WEA.AppEventPages.Meta.Keywords":"App Store","WEA.AppEventPages.Meta.Title":"@@softwareName@@: @@description@@","WEA.AppPages.AX.ViewIn":"View On","WEA.AppPages.AdditionalScreenshots":"Additional Screenshots","WEA.AppPages.AgeRating":"Age Rating","WEA.AppPages.AgeRating.Value":"@@ageRating@@, Made for Ages @@min@@–@@max@@","WEA.AppPages.AppPrivacy.Description":"The developer, @@developer@@, indicated that the app’s privacy practices may include handling of data as described below. ","WEA.AppPages.AppPrivacy.Description.ManagePrivacy":"Learn how the developer lets you @@managePrivacyLink@@.","WEA.AppPages.AppPrivacy.Description.ManagePrivacy.Link":"manage your privacy choices","WEA.AppPages.AppPrivacy.Description.NoDetails":"The developer, @@developer@@, has not provided details about its privacy practices and handling of data to Apple. ","WEA.AppPages.AppPrivacy.Description.NotVerifiedByApple":"This information has not been verified by Apple. ","WEA.AppPages.AppPrivacy.Description.PrivacyDefinitions":"To help you better understand the developer’s responses, see @@privacyDefinitionsLink@@.","WEA.AppPages.AppPrivacy.Description.PrivacyDefinitions.Link":"Privacy Definitions and Examples","WEA.AppPages.AppPrivacy.Description.PrivacyPolicy":"For more information, see the @@privacyPolicyLink@@.","WEA.AppPages.AppPrivacy.Description.PrivacyPolicy.Link":"developer’s privacy policy","WEA.AppPages.AppPrivacy.LearnMore":"Privacy practices may vary, for example, based on the features you use or your age. @@learnMoreLink@@","WEA.AppPages.AppPrivacy.LearnMore.Link":"Learn More","WEA.AppPages.AppPrivacy.Title":"App Privacy","WEA.AppPages.AppPrivacy.Types.NoDetails.Description":"The developer will be required to provide privacy details when they submit their next app update.","WEA.AppPages.AppPrivacy.Types.NoDetails.Title":"No Details Provided","WEA.AppPages.AppStore.Header":"App Store","WEA.AppPages.AppSupport":"App Support","WEA.AppPages.AppleTV":"Apple TV","WEA.AppPages.AppleWatch":"Apple Watch","WEA.AppPages.Arcade.Branding.Title":"Apple Arcade","WEA.AppPages.Arcade.Disclaimer":"*New subscribers only. Sign-up required. Subscription automatically renews for a monthly fee after trial.","WEA.AppPages.Arcade.Editorial.Author":"App Store Editors","WEA.AppPages.Arcade.Link.URL":"https://www.apple.com/apple-arcade/","WEA.AppPages.Arcade.UpsellBanner.CTA.Text":"Try It Free*","WEA.AppPages.Arcade.UpsellBanner.Text":"No ads.","WEA.AppPages.Arcade.UpsellBanner.Text2":"No in-app purchases.","WEA.AppPages.Arcade.UpsellBanner.Title":"Play over 200 games.","WEA.AppPages.Arcade.UpsellFooter.CTA.Text":"Try It Free*","WEA.AppPages.Arcade.UpsellFooter.Text":"No ads.","WEA.AppPages.Arcade.UpsellFooter.Text2":"No in-app purchases.","WEA.AppPages.Arcade.UpsellFooter.Title":"Play over 200 games.","WEA.AppPages.Availability.iOS":"This app is available only on the App Store for iPhone and iPad.","WEA.AppPages.Availability.iOS.Bundle":"This app bundle is available only on the App Store for iPhone and iPad.","WEA.AppPages.Availability.iOS.iPad":"This app is available only on the App Store for iPad.","WEA.AppPages.Availability.iOS.iPhone":"This app is available only on the App Store for iPhone.","WEA.AppPages.Availability.iOS.isiOS":"Open the App Store to buy and download apps.","WEA.AppPages.Availability.iOSmacOS":"This app is available only on the App Store for iPhone, iPad, and Mac.","WEA.AppPages.Availability.iOSmacOSwatchOS":"This app is available only on the App Store for iPhone, iPad, Mac, and Apple Watch.","WEA.AppPages.Availability.iOStvOS":"This app is available only on the App Store for iPhone, iPad, and Apple TV.","WEA.AppPages.Availability.iOStvOSmacOS":"This app is available only on the App Store for iPhone, iPad, Mac, and Apple TV.","WEA.AppPages.Availability.iOStvOSmacOSwatchOS":"This app is available only on the App Store for iPhone, iPad, Mac, Apple Watch, and Apple TV.","WEA.AppPages.Availability.iOStvOSwatchOS":"This app is available only on the App Store for iPhone, iPad, Apple Watch, and Apple TV.","WEA.AppPages.Availability.iOSwatchOS":"This app is available only on the App Store for iPhone, iPad, and Apple Watch.","WEA.AppPages.Availability.iOSwatchOS.NoiPad":"This app is available only on the App Store for iPhone and Apple Watch.","WEA.AppPages.Availability.macOS":"Open the Mac App Store to buy and download apps.","WEA.AppPages.Availability.macOS.Bundle":"This app bundle is available only on the Mac App Store.","WEA.AppPages.Availability.tvOS":"This app is available only on the App Store for Apple TV.","WEA.AppPages.Availability.watchOS":"This app is available only on the App Store for Apple Watch.","WEA.AppPages.CTA.AppStore.AX":"View in App Store","WEA.AppPages.CTA.AppStore.Action":"View in","WEA.AppPages.CTA.AppStore.App":"App Store","WEA.AppPages.CTA.AppleSchool.AX":"View in Apple School","WEA.AppPages.CTA.AppleSchool.Action":"View in","WEA.AppPages.CTA.AppleSchool.App":"Apple School","WEA.AppPages.CTA.MacAppStore.AX":"View in Mac App Store","WEA.AppPages.CTA.MacAppStore.Action":"View in","WEA.AppPages.CTA.MacAppStore.App":"Mac App Store","WEA.AppPages.Category":"Category","WEA.AppPages.Compatibility":"Compatibility","WEA.AppPages.Copyright":"Copyright","WEA.AppPages.CustomerReviews.Title":"Ratings and Reviews","WEA.AppPages.CustomersAlsoBought.Title":"You Might Also Like","WEA.AppPages.Description.Header":"Description","WEA.AppPages.DesignedFor.iPad":"Designed for iPad","WEA.AppPages.DesignedFor.iPhone":"Designed for iPhone","WEA.AppPages.DeveloperResponse":"Developer Response","WEA.AppPages.DeveloperWebsite":"Developer Website","WEA.AppPages.EditReview":"To edit your review of this app, use an iPhone or iPad to view the app on the App Store.","WEA.AppPages.EditReview.macOS.OldmacOS":"To edit your review, view this app in the Mac App Store.","WEA.AppPages.EditorsChoice":"Editors’ Choice","WEA.AppPages.EditorsNotes.Header":"Editors’ Notes","WEA.AppPages.Eula.Error":"Something went wrong. Try again.","WEA.AppPages.Eula.Title":"@@appName@@ @@version@@ License Agreement","WEA.AppPages.Events.Status.Ended":"EVENT ENDED","WEA.AppPages.Events.Status.HappeningNow":"HAPPENING NOW","WEA.AppPages.Events.Status.Live":"LIVE","WEA.AppPages.Events.Status.NowAvailable":"NOW AVAILABLE","WEA.AppPages.Events.Title":"Events","WEA.AppPages.ExpectedReleaseDate":"Expected @@expectedReleaseDate@@","WEA.AppPages.ExternalPurchaseBanner.CTA.Text":"Learn More","WEA.AppPages.ExternalPurchaseBanner.Text":"This app doesn't support the App Store's private and secure payment system. It uses external purchases.","WEA.AppPages.ExternalPurchaseBanner.Text.NL":"All payments in this app will be managed by the developer. This app uses external purchases.","WEA.AppPages.ExternalPurchaseInformation.CTA.Text":"Learn More","WEA.AppPages.ExternalPurchaseInformation.Text":"All purchases in this app will be managed by the developer. Your stored App Store payment method and related features, such as subscription management, Ask to Buy, Family Sharing, and refund requests, will not be available. Apple is not responsible for the privacy or security of transactions made with this developer.","WEA.AppPages.ExternalPurchaseInformation.Text.NL":"All purchases in this app will be processed by a service provider selected by the developer “@@developerName@@.” The developer will be responsible for the payment methods and related features such as subscriptions and refunds. App Store features, such as your stored App Store payment method, subscription management, and refund requests, will not be available.","WEA.AppPages.ExternalPurchaseInformation.Text.noDeveloper.NL":"All purchases in this app will be processed by a service provider selected by the developer. The developer will be responsible for the payment methods and related features such as subscriptions and refunds. App Store features, such as your stored App Store payment method, subscription management, and refund requests, will not be available.","WEA.AppPages.ExternalPurchaseInformation.Title":"External Purchases","WEA.AppPages.FB.siteName.desktopApp":"Mac App Store","WEA.AppPages.FB.siteName.iosSoftware":"App Store","WEA.AppPages.FB.siteName.mobileSoftwareBundle":"App Store","WEA.AppPages.FamilySharing":"Family Sharing","WEA.AppPages.FeaturedIn.Title":"Featured In","WEA.AppPages.GameController":"Game Controller","WEA.AppPages.InAppPurchase.Title":"In-App Purchases","WEA.AppPages.InAppPurchases.Title":"In-App Purchases","WEA.AppPages.Information.Title":"Information","WEA.AppPages.Languages":"Languages","WEA.AppPages.LicenseAgreement":"License Agreement","WEA.AppPages.Location.Description":"This app may use your location even when it isn’t open, which can decrease battery life.","WEA.AppPages.Location.Title":"Location","WEA.AppPages.MacAppStore.Header":"Mac App Store","WEA.AppPages.Meta.MacAppPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ for macOS @@minimumOSVersion@@ or later and enjoy it on your Mac.","WEA.AppPages.Meta.MacAppPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, mac apps, app store, appstore, applications, itunes","WEA.AppPages.Meta.MacAppStorePageTitle":"@@softwareName@@ on the Mac App Store","WEA.AppPages.Meta.PageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your iPhone, iPad, and iPod touch.","WEA.AppPages.Meta.PageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes","WEA.AppPages.Meta.iOSmacOSPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your iPhone, iPad, iPod touch, or Mac OS X @@minimumOSVersion@@ or later.","WEA.AppPages.Meta.iOSmacOSPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, ios apps, mac apps, app, appstore, app store, applications, iphone, ipad, ipod touch, itouch, itunes","WEA.AppPages.Meta.iOSmacOSwatchOSPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your iPhone, iPad, iPod touch, Mac OS X @@minimumOSVersion@@ or later, or Apple Watch.","WEA.AppPages.Meta.iOSmacOSwatchOSPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, ios apps, mac apps, watchos apps, app, appstore, app store, applications, iphone, ipad, ipod touch, itouch, itunes, apple watch","WEA.AppPages.Meta.iOStvOSmacOSPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your iPhone, iPad, iPod touch, Mac OS X @@minimumOSVersion@@ or later, or Apple TV.","WEA.AppPages.Meta.iOStvOSmacOSPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, ios apps, tvos apps, mac apps, app, appstore, app store, applications, iphone, ipad, ipod touch, itouch, itunes, appletv, apple tv","WEA.AppPages.Meta.iOStvOSmacOSwatchOSPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your iPhone, iPad, iPod touch, Mac OS X @@minimumOSVersion@@ or later, Apple TV, or Apple Watch.","WEA.AppPages.Meta.iOStvOSmacOSwatchOSPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, ios apps, tvos apps, mac apps, watchos apps, app, appstore, app store, applications, iphone, ipad, ipod touch, itouch, itunes, appletv, apple tv, apple watch","WEA.AppPages.Meta.title":"@@softwareName@@ on the App Store","WEA.AppPages.Meta.tvOSAppPageMetaDescriptionLine":"Read reviews, compare customer ratings, see screenshots, and learn more about @@softwareName@@. Download @@softwareName@@ and enjoy it on your Apple TV.","WEA.AppPages.Meta.tvOSAppPageMetaKeywords":"@@softwareName@@, @@developerName@@, @@categoryNames@@, tvos apps, app, appstore, app store, appletv, apple tv","WEA.AppPages.Meta.tvOSAppPagetitle":"@@softwareName@@ on the App Store","WEA.AppPages.MoreAppsByDeveloperName":"More Apps by @@developerName@@","WEA.AppPages.MoreByThisDeveloper.Title":"More By This Developer","WEA.AppPages.OffersAppleWatchApp":"Offers Apple Watch App","WEA.AppPages.OffersAppleWatchAppForiPhone":"Offers Apple Watch App for iPhone","WEA.AppPages.OffersExternalPurchases":"Offers External Purchases","WEA.AppPages.OffersInAppPurchases":"Offers In-App Purchases","WEA.AppPages.OffersiMessageApp":"Offers iMessage App","WEA.AppPages.OffersiMessageAppForiOS":"Offers iMessage App for iOS","WEA.AppPages.OnlyForAppleTV":"Only for Apple TV","WEA.AppPages.OnlyForiMessage":"Only for iMessage","WEA.AppPages.Optional":"Optional","WEA.AppPages.PreOrder":"Pre-Order","WEA.AppPages.PreOrderDisclaimer":"This content may change without notice, and the final product may be different.","WEA.AppPages.Price.Title":"Price","WEA.AppPages.PrivacyPolicy":"Privacy Policy","WEA.AppPages.RankInGenre":"#@@rank@@ in @@genreName@@","WEA.AppPages.Rating":"Rating","WEA.AppPages.Ratings.Title":"Ratings","WEA.AppPages.RatingsReviews.Title":"Ratings and Reviews","WEA.AppPages.Required":"Required","WEA.AppPages.Screenshots":"Screenshots","WEA.AppPages.Screenshots.appleTV":"Apple TV","WEA.AppPages.Screenshots.appleTVScreenshots":"Apple TV Screenshots","WEA.AppPages.Screenshots.appleWatch":"Apple Watch","WEA.AppPages.Screenshots.appleWatchScreenshots":"Apple Watch Screenshots","WEA.AppPages.Screenshots.ipad":"iPad","WEA.AppPages.Screenshots.ipadScreenshots":"iPad Screenshots","WEA.AppPages.Screenshots.iphone":"iPhone","WEA.AppPages.Screenshots.iphoneScreenshots":"iPhone Screenshots","WEA.AppPages.Screenshots.mac":"Mac","WEA.AppPages.Screenshots.messages":"iMessage","WEA.AppPages.Screenshots.messagesScreenshots":"iMessage Screenshots","WEA.AppPages.Seller":"Seller","WEA.AppPages.Size":"Size","WEA.AppPages.StandAloneForWatch":"Only on Apple Watch","WEA.AppPages.Subscriptions.FreeTrial":"Free Trial","WEA.AppPages.Subscriptions.PayAsYouGo":"@@price@@ Trial","WEA.AppPages.Subscriptions.PayUpFront":"@@price@@ Trial","WEA.AppPages.Subscriptions.Title":"Subscriptions","WEA.AppPages.Supports.FamilySharing.Description":"Up to six family members can use this app with Family Sharing enabled.","WEA.AppPages.Supports.FamilySharing.Description.withIAP":"Some in‑app purchases, including subscriptions, may be shareable with your family group when Family Sharing is enabled.","WEA.AppPages.Supports.FamilySharing.Title":"Family Sharing","WEA.AppPages.Supports.GameCenter.Description":"Challenge friends and check leaderboards and achievements.","WEA.AppPages.Supports.GameCenter.Title":"Game Center","WEA.AppPages.Supports.GameController.Description":"Play this game with your favorite compatible controller.","WEA.AppPages.Supports.GameController.Title":"Game Controllers","WEA.AppPages.Supports.Siri.Description":"Get things done within this app using just your voice.","WEA.AppPages.Supports.Siri.Title":"Siri","WEA.AppPages.Supports.Title":"Supports","WEA.AppPages.Supports.Wallet.Description":"Get all of your passes, tickets, cards, and more in one place.","WEA.AppPages.Supports.Wallet.Title":"Wallet","WEA.AppPages.TopInAppPurchases.Title":"In-App Purchases","WEA.AppPages.TopInCategory.Title":"Top Apps In @@categoryName@@","WEA.AppPages.Twitter.site.desktopApp":"@MacAppStore","WEA.AppPages.Updated":"Updated","WEA.AppPages.Version":"Version","WEA.AppPages.VersionHistory.Title":"Version History","WEA.AppPages.ViewIn":"View On:","WEA.AppPages.WhatsNew.Header":"What’s New","WEA.CarrierPages.UpdateOS.Instructions.MacOS":"For Mac, go to System Preferences \u003e Software Update","WEA.CarrierPages.UpdateOS.Instructions.One":"Once your device is updated, go back and use the link from your carrier to activate this subscription.","WEA.CarrierPages.UpdateOS.Instructions.iOS":"For iPhone and iPad, go to Settings \u003e General \u003e Software Update","WEA.CarrierPages.UpdateOS.Meta.Description":"Update your iPhone, iPad, or Mac to the latest OS to activate the subscription included with your carrier plan.","WEA.CarrierPages.UpdateOS.Meta.Title":"Update device to latest OS to activate subscription","WEA.CarrierPages.UpdateOS.Title":"Update your device to the latest OS to activate this subscription.","WEA.Charts.AppFilter.Button":"App Categories","WEA.Charts.AppFilter.Tab":"Apps","WEA.Charts.Apps.AppType.Apps":"apps","WEA.Charts.Apps.AppType.Games":"games","WEA.Charts.Apps.DeviceType.iPad":"iPad","WEA.Charts.Apps.DeviceType.iPhone":"iPhone","WEA.Charts.Apps.Games.top-free-games":"Top Free Games","WEA.Charts.Apps.Games.top-paid-games":"Top Paid Games","WEA.Charts.Apps.Meta.CategoryChartDescription":"Explore top @@chart@@ @@category@@ @@baseCategory@@ for your @@deviceType@@ on the App Store, like @@entity1@@, @@entity2@@, and more.","WEA.Charts.Apps.Meta.CategoryChartKeywords":"@@chart@@, @@deviceType@@, @@category@@, @@baseCategory@@, app store, top charts, apple","WEA.Charts.Apps.Meta.CategoryChartTitle":"Top @@chart@@ @@deviceType@@ @@category@@ @@baseCategory@@ on the App Store - Apple (@@storefront@@)","WEA.Charts.Apps.Meta.CategoryDescription":"Explore top @@deviceType@@ @@category@@ @@baseCategory@@ on the App Store, like @@entity1@@, @@entity2@@, and more.","WEA.Charts.Apps.Meta.CategoryKeywords":"best @@deviceType@@ @@category@@ @@baseCategory@@, @@deviceType@@, @@category@@, @@baseCategory@@, app store, top charts, apple","WEA.Charts.Apps.Meta.CategoryTitle":"Top @@deviceType@@ @@category@@ @@baseCategory@@ on the App Store - Apple (@@storefront@@)","WEA.Charts.Apps.Meta.Chart.Free":"free","WEA.Charts.Apps.Meta.Chart.Paid":"paid","WEA.Charts.Apps.Meta.ChartDescription":"Explore top @@chart@@ @@deviceType@@ @@baseCategory@@ on the App Store, like @@entity1@@, @@entity2@@, and more.","WEA.Charts.Apps.Meta.ChartKeywords":"best @@chart@@ @@deviceType@@ @@baseCategory@@, @@chart@@, @@deviceType@@, @@baseCategory@@, app store, top charts, apple","WEA.Charts.Apps.Meta.ChartTitle":"Top @@chart@@ @@deviceType@@ @@baseCategory@@ on the App Store - Apple (@@storefront@@)","WEA.Charts.Apps.Meta.Description":"Find and download top apps and games for @@deviceType@@ on the App Store.","WEA.Charts.Apps.Meta.Keywords":"best @@deviceType@@ apps, best @@deviceType@@ games, app store, top charts, apple","WEA.Charts.Apps.Meta.Title":"@@deviceType@@ Top Charts on the App Store - Apple (@@storefront@@)","WEA.Charts.Apps.Title":"Top Charts","WEA.Charts.AudioBooks.Meta.Description":"Browse the list of most popular and best selling audiobooks on Apple Books. Find the top charts for best audiobooks to listen across all genres.","WEA.Charts.AudioBooks.Meta.GenreDescription":"Find the best @@genre@@ audiobooks to listen on Apple Books. View the list of top free audiobooks and the best selling titles including @@entity1@@, @@entity2@@ and more.","WEA.Charts.AudioBooks.Meta.GenreKeywords":"top @@genre@@ audiobooks, top @@genre@@ audiobooks on iphone, ipad, Mac, Apple Books","WEA.Charts.AudioBooks.Meta.GenreTitle":"Top @@genre@@ audiobooks - Apple Books (@@storefront@@)","WEA.Charts.AudioBooks.Meta.Keywords":"top audiobooks, top audiobooks on iphone, ipad, Mac, Apple Books","WEA.Charts.AudioBooks.Meta.Title":"Top audiobooks - Apple Books (@@storefront@@)","WEA.Charts.AudioBooks.Title":"Top Audiobooks","WEA.Charts.Books.Meta.ChartDescription":"Find the best @@genre@@ (@@chart@@) books to read on Apple Books. View the list of top titles including @@entity1@@, @@entity2@@ and more.","WEA.Charts.Books.Meta.ChartKeywords":"top @@chart@@ @@genre@@ books, top @@chart@@ @@genre@@ books on iphone, ipad, Mac, Apple Books","WEA.Charts.Books.Meta.ChartTitle":"Top @@genre@@ books (@@chart@@) - Apple Books (@@storefront@@)","WEA.Charts.Books.Meta.Description":"Browse the list of most popular and best selling books on Apple Books. Find the top charts for best books to read across all genres.","WEA.Charts.Books.Meta.GenreDescription":"Find the best @@genre@@ books to read on Apple Books. View the list of top free books and the best selling titles including @@entity1@@, @@entity2@@ and more.","WEA.Charts.Books.Meta.GenreKeywords":"top @@genre@@ books, top @@genre@@ books on iphone, ipad, Mac, Apple Books","WEA.Charts.Books.Meta.GenreTitle":"Top @@genre@@ books - Apple Books (@@storefront@@)","WEA.Charts.Books.Meta.Keywords":"top books, top books on iphone, ipad, Mac, Apple Books","WEA.Charts.Books.Meta.Title":"Top books - Apple Books (@@storefront@@)","WEA.Charts.Books.Title":"Top Books","WEA.Charts.Categories.Button":"Categories","WEA.Charts.Common.Rank":"Number @@rank@@","WEA.Charts.GameFilter.Button":"Game Categories","WEA.Charts.GameFilter.Tab":"Games","WEA.Charts.GenreFilter.Button":"Genre Charts","WEA.Charts.LocalNav.Links.TopAudiobooks":"Top Audiobooks","WEA.Charts.LocalNav.Links.TopBooks":"Top Books","WEA.Common.Accessibility.LocalNavClose":"Local Nav Close Menu","WEA.Common.Accessibility.LocalNavOpen":"Local Nav Open Menu","WEA.Common.Authentication.Login":"Sign In","WEA.Common.Authentication.Logout":"Sign Out","WEA.Common.AverageRating":"@@rating@@ out of @@ratingTotal@@","WEA.Common.Close":"Close","WEA.Common.Comma":", ","WEA.Common.DotSeparator":"@@string1@@ · @@string2@@","WEA.Common.Duration":"Duration","WEA.Common.Ellipsis.Animated":"\u003cspan\u003e.\u003c/span\u003e\u003cspan\u003e.\u003c/span\u003e\u003cspan\u003e.\u003c/span\u003e","WEA.Common.Errors.404":"The content you’ve requested isn’t currently available.","WEA.Common.Errors.404.Title":"Content Not Available","WEA.Common.FileSize.GB":"@@count@@ GB","WEA.Common.FileSize.GB.AX.one":"1 gigabyte","WEA.Common.FileSize.GB.AX.other":"@@count@@ gigabytes","WEA.Common.FileSize.GB.Unit":"GB","WEA.Common.FileSize.KB":"@@count@@ KB","WEA.Common.FileSize.KB.AX.one":"1 kilobyte","WEA.Common.FileSize.KB.AX.other":"@@count@@ kilobytes","WEA.Common.FileSize.KB.Unit":"KB","WEA.Common.FileSize.MB":"@@count@@ MB","WEA.Common.FileSize.MB.AX.one":"1 megabyte","WEA.Common.FileSize.MB.AX.other":"@@count@@ megabytes","WEA.Common.FileSize.MB.Unit":"MB","WEA.Common.FileSize.byte.Unit":"byte","WEA.Common.FileSize.byte.one":"1 byte","WEA.Common.FileSize.byte.other":"@@count@@ bytes","WEA.Common.Free":"Free","WEA.Common.Get":"GET","WEA.Common.Hours.Unit.Short":"hr","WEA.Common.Hours.abbr.one":"1 hr","WEA.Common.Hours.abbr.other":"@@count@@ hr","WEA.Common.Hours.one":"1 Hour","WEA.Common.Hours.other":"@@count@@ Hours","WEA.Common.Languages.AD":"AD","WEA.Common.Languages.AudioTrack":"AudioTrack","WEA.Common.Languages.CC":"CC","WEA.Common.Languages.Dolby51":"Dolby Digital 5.1","WEA.Common.Languages.Dolby51Plus":"Dolby Digital Plus 5.1","WEA.Common.Languages.Dolby71Plus":"Dolby Digital Plus 7.1","WEA.Common.Languages.DolbyAtmos":"Dolby Atmos","WEA.Common.Languages.SDH":"SDH","WEA.Common.Languages.Subtitled":"Subtitled","WEA.Common.Languages.hasAudioDescription":"AD","WEA.Common.Languages.hasClosedCaptioning":"CC","WEA.Common.Languages.hasDolby":"Dolby 5.1","WEA.Common.Languages.hasDolby71":"Dolby Digital Plus 7.1","WEA.Common.Languages.hasDolbyAtmos":"Dolby Atmos","WEA.Common.Languages.hasSDH":"SDH","WEA.Common.Languages.hasStereo":"Stereo","WEA.Common.Languages.hasSubtitles":"Subtitles","WEA.Common.Languages.languageDescriptor":"@@languageName@@ (@@languageMeta@@)","WEA.Common.LearnMore":"Learn More","WEA.Common.Listen":"Listen","WEA.Common.Loading":"Loading","WEA.Common.Meta.FB.genericDescription.Books":"From mysteries to memoirs, enjoy books and audiobooks on your iPhone, iPad, iPod touch, or Apple Watch. Read free samples of ebooks and listen to free audiobook previews. Get recent bestsellers, all-time favorites, and more on Apple Books—all ready to instantly download.","WEA.Common.Meta.FB.siteName.AM":"Apple Music","WEA.Common.Meta.FB.siteName.AppleTV":"Apple TV","WEA.Common.Meta.FB.siteName.Apps":"App Store","WEA.Common.Meta.FB.siteName.Books":"Apple Books","WEA.Common.Meta.FB.siteName.MacApps":"Mac App Store","WEA.Common.Meta.FB.siteName.Podcasts":"Apple Podcasts","WEA.Common.Meta.FB.siteName.iTunes":"iTunes","WEA.Common.Meta.Twitter.genericDescription.Books":"From mysteries to memoirs, enjoy books and audiobooks on your iPhone, iPad, iPod touch, or Apple Watch. Read free samples of ebooks and listen to free audiobook previews. Get recent bestsellers, all-time favorites, and more on Apple Books—all ready to instantly download.","WEA.Common.Meta.Twitter.site.AM":"@appleMusic","WEA.Common.Meta.Twitter.site.Apps":"@AppStore","WEA.Common.Meta.Twitter.site.Books":"@AppleBooks","WEA.Common.Meta.Twitter.site.iTunes":"@iTunes","WEA.Common.Minutes.Unit.Short":"min","WEA.Common.Minutes.abbr.one":"1 min","WEA.Common.Minutes.abbr.other":"@@count@@ min","WEA.Common.Minutes.one":"1 Minute","WEA.Common.Minutes.other":"@@count@@ Minutes","WEA.Common.More":"more","WEA.Common.NowPlaying":"NOW PLAYING","WEA.Common.Number":"@@count@@","WEA.Common.OG.SiteName.AppleBooks":"Apple Books","WEA.Common.OG.SiteName.TV":"Apple TV","WEA.Common.OG.SiteName.iTunes":"iTunes","WEA.Common.OpensIn.AppleBooks":"Opens In Apple Books","WEA.Common.Paid":"Paid","WEA.Common.Pause":"PAUSE","WEA.Common.Pause.Alternate":"PAUSE","WEA.Common.Percentage":"@@percentage@@%","WEA.Common.Platforms.AppleTv":"Apple TV","WEA.Common.Platforms.Bundles":"Bundles","WEA.Common.Platforms.Mac":"Mac","WEA.Common.Platforms.Watch":"Apple Watch","WEA.Common.Platforms.iMessage":"iMessage","WEA.Common.Platforms.iPad":"iPad","WEA.Common.Platforms.iPadiPhone":"iPad \u0026 iPhone","WEA.Common.Platforms.iPhone":"iPhone","WEA.Common.Play":"Play","WEA.Common.Play.Alternate":"PLAY","WEA.Common.Playback.Progress":"Playback Progress","WEA.Common.Playback.Skip.Ahead":"Skip Ahead","WEA.Common.Playback.Skip.Back":"Skip Back","WEA.Common.Preview":"PREVIEW","WEA.Common.Preview.VideoName":"Preview @@videoName@@","WEA.Common.Ratings.one":"1 Rating","WEA.Common.Ratings.other":"@@count@@ Ratings","WEA.Common.Ratings.zero":"No Ratings","WEA.Common.Related":"Related","WEA.Common.Released":"Released: @@releaseDate@@","WEA.Common.Roles.AsCharacter":"as @@characterName@@","WEA.Common.Roles.Type.Actor":"Actor","WEA.Common.Roles.Type.Advisor":"Advisor","WEA.Common.Roles.Type.Cast":"Cast","WEA.Common.Roles.Type.Creator":"Creator","WEA.Common.Roles.Type.Director":"Director","WEA.Common.Roles.Type.Guest":"Guest","WEA.Common.Roles.Type.GuestStar":"Guest Star","WEA.Common.Roles.Type.Host":"Host","WEA.Common.Roles.Type.Music":"Music","WEA.Common.Roles.Type.Performer":"Performer","WEA.Common.Roles.Type.Producer":"Producer","WEA.Common.Roles.Type.Voice":"Voice","WEA.Common.Roles.Type.Writer":"Writer","WEA.Common.Seconds.abbr.one":"1 sec","WEA.Common.Seconds.abbr.other":"@@count@@ sec","WEA.Common.Seconds.one":"1 Second","WEA.Common.Seconds.other":"@@count@@ Seconds","WEA.Common.SeeAll.Button":"See All","WEA.Common.SeeAll.Title.Generic":"@@parentName@@ - @@sectionTitle@@","WEA.Common.SeeAll.Title.Item":"@@itemName@@ - @@productName@@ - @@sectionTitle@@","WEA.Common.SeeAll.Title.Product":"@@artistName@@ — @@sectionTitle@@","WEA.Common.SeeDetails.Button":"See Details","WEA.Common.SentenceDelimiter":".","WEA.Common.SeparatorDuration":"@@hours@@ @@minutes@@","WEA.Common.SeparatorDuration.Short":"@@string1@@:@@string2@@","WEA.Common.SeparatorGeneric":"@@string1@@, @@string2@@","WEA.Common.Share.CloseMenu.AX":"Close sharing menu","WEA.Common.Share.Embed.AX":"Copy the embed code for the @@contentType@@ @@mediaTitle@@ by @@name@@","WEA.Common.Share.EmbedCopied":"Embed Code Copied","WEA.Common.Share.Link.AX":"Copy a link to the @@contentType@@ @@mediaTitle@@ by @@name@@","WEA.Common.Share.LinkCopied":"Link Copied","WEA.Common.Share.OpenMenu.AX":"Open sharing menu","WEA.Common.Share.Social.AX":"Share the @@contentType@@ @@mediaTitle@@ by @@name@@ on @@network@@","WEA.Common.Shuffle":"Shuffle","WEA.Common.TV.CTA.AM":"Watch on Mac, PC, or Android with Apple Music","WEA.Common.TV.CTA.AX":"Watch on Apple TV","WEA.Common.Time.AvailableIn":"AVAILABLE IN @@minutes@@ MIN","WEA.Common.Time.DayOfTheWeek.Friday":"FRI @@time@@","WEA.Common.Time.DayOfTheWeek.Monday":"MON @@time@@","WEA.Common.Time.DayOfTheWeek.Saturday":"SAT @@time@@","WEA.Common.Time.DayOfTheWeek.Sunday":"SUN @@time@@","WEA.Common.Time.DayOfTheWeek.Thursday":"THU @@time@@","WEA.Common.Time.DayOfTheWeek.Tuesday":"TUE @@time@@","WEA.Common.Time.DayOfTheWeek.Wednesday":"WED @@time@@","WEA.Common.Time.StartsIn":"STARTS IN @@minutes@@ MIN","WEA.Common.Time.Today":"TODAY @@time@@","WEA.Common.Time.Tomorrow":"TOMORROW @@time@@","WEA.Common.Time.Tonight":"TONIGHT @@time@@","WEA.Common.TogglePlay":"Play/Pause","WEA.Common.TrackList.Album":"ALBUM","WEA.Common.TrackList.Artist":"ARTIST","WEA.Common.TrackList.Price":"PRICE","WEA.Common.TrackList.Time":"TIME","WEA.Common.TrackList.TimeRemaining":"Time remaining","WEA.Common.TrackList.Track":"TITLE","WEA.Common.VideoSubType.concert":"CONCERT","WEA.Common.VideoSubType.episode":"EPISODE","WEA.Common.VideoSubType.episodebonus":"EPISODE BONUS","WEA.Common.VideoSubType.feature":"FEATURE","WEA.Common.VideoSubType.seasonbonus":"SEASON BONUS","WEA.Common.VideoSubType.short":"SHORT","WEA.Common.VideoSubType.specialty":"SPECIALTY","WEA.Common.VideoSubType.tvextra":"EXTRA","WEA.Common.VideoSubType.tvinterview":"INTERVIEW","WEA.Common.VideoSubType.tvtrailer":"TRAILER","WEA.Common.View":"VIEW","WEA.Common.Yes":"Yes","WEA.DeepLinkPages.Social.Invite.Description.AM":"Discover music with friends","WEA.DeepLinkPages.Social.Invite.Title.AM":"Start Sharing on Apple Music","WEA.DeepLinkPages.Social.Subscribe.Description.AM":"Play and download all the music you want.","WEA.DeepLinkPages.Social.Subscribe.Title.AM":"Try Apple Music","WEA.DeveloperPages.FB.siteName.developer":"App Store","WEA.DeveloperPages.Messages.NoApps":"There are currently no apps available.","WEA.DeveloperPages.Meta.Description.ManyApps":"Download apps by @@developerName@@, including @@listing1@@, @@listing2@@, @@listing3@@, and many more.","WEA.DeveloperPages.Meta.Description.NoApps":"Download apps by @@developerName@@","WEA.DeveloperPages.Meta.Description.OneApp":"Download apps by @@developerName@@, including @@listing1@@.","WEA.DeveloperPages.Meta.Description.ThreeApps":"Download apps by @@developerName@@, including @@listing1@@, @@listing2@@, and @@listing3@@.","WEA.DeveloperPages.Meta.Description.TwoApps":"Download apps by @@developerName@@, including @@listing1@@ and @@listing2@@.","WEA.DeveloperPages.Meta.PageKeywords":"download, @@developerName@@, apps on iphone, ipad, iMessage, Apple Watch, Mac","WEA.DeveloperPages.Meta.Title":"@@developerName@@ Apps on the App Store","WEA.EditorialItemProductPages.AppOfTheDay":"APP OF THE DAY","WEA.EditorialItemProductPages.Banner.Copy":"This story can only be viewed on the App Store in iOS 11 or later on your iPhone or iPad. @@link@@","WEA.EditorialItemProductPages.Banner.Copy.NewmacOS":"This story can be viewed only in the App Store. @@link@@","WEA.EditorialItemProductPages.Banner.Copy.NonmacOS":"This story can be viewed only in the App Store. @@link@@","WEA.EditorialItemProductPages.Banner.Copy.OldmacOS":"To view this story, @@link@@","WEA.EditorialItemProductPages.Banner.Headline":"Get the Full Experience","WEA.EditorialItemProductPages.Banner.Headline.NewmacOS":"Get the Full Experience","WEA.EditorialItemProductPages.Banner.Headline.NonmacOS":"Get the Full Experience","WEA.EditorialItemProductPages.Banner.Headline.OldmacOS":"Get the Full Experience","WEA.EditorialItemProductPages.Banner.Link.Text":"Learn more.","WEA.EditorialItemProductPages.Banner.Link.Text.NewmacOS":"Learn more.","WEA.EditorialItemProductPages.Banner.Link.Text.NonmacOS":"Learn more.","WEA.EditorialItemProductPages.Banner.Link.Text.OldmacOS":"upgrade to the latest macOS.","WEA.EditorialItemProductPages.Banner.Link.URL":"https://www.apple.com/ios/app-store/","WEA.EditorialItemProductPages.Banner.Link.URL.NewmacOS":"https://www.apple.com/macos/mojave-preview/#mac-app-store","WEA.EditorialItemProductPages.Banner.Link.URL.NonmacOS":"https://www.apple.com/macos/mojave-preview/#mac-app-store","WEA.EditorialItemProductPages.Banner.Link.URL.OldmacOS":"macappstore://itunes.apple.com/WebObjects/MZStore.woa/wa/storeFront","WEA.EditorialItemProductPages.CTA.Headline":"Get the Full Experience","WEA.EditorialItemProductPages.CTA.Link.Url":"https://www.apple.com/ios/app-store/","WEA.EditorialItemProductPages.CTA.Text":"This story can only be viewed on the App Store in iOS 11 or later on your iPhone or iPad.","WEA.EditorialItemProductPages.CTA.Text.AX":"VIEW @@appName@@","WEA.EditorialItemProductPages.FB.siteName.iosSoftware":"App Store","WEA.EditorialItemProductPages.FB.siteName.macOs":"Mac App Store","WEA.EditorialItemProductPages.GameOfTheDay":"GAME OF THE DAY","WEA.EditorialItemProductPages.InAppPurchase":"IN-APP PURCHASE","WEA.EditorialItemProductPages.InAppPurchase.Title":"@@purchaseName@@ for @@appName@@","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaDescription":"Learn about @@storyTitle@@ on Mac App Store. Download @@featuredAppName@@ and enjoy it on your Mac.","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaDescription.Collection.ManyMore":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, and many more on Mac App Store. Enjoy these apps on your Mac.","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaDescription.Collection.One":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@ and many more on Mac App Store. Enjoy these apps on your Mac.","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaDescription.Collection.Three":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, and many more on Mac App Store. Enjoy these apps on your Mac.","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaDescription.Collection.Two":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, and many more on Mac App Store. Enjoy these apps on your Mac.","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaKeywords":"@@storyTitle@@, @@featuredAppName@@, @@applicationCategory@@, app, appstore, app store, Mac","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaKeywords.Collection.One":"@@storyTitle@@, @@featuredAppName1@@, @@applicationCategory@@, app, appstore, app store, Mac","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaKeywords.Collection.Three":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, @@applicationCategory@@, app, appstore, app store, Mac","WEA.EditorialItemProductPages.MacAppStore.MacOS.Meta.PageMetaKeywords.Collection.Two":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@applicationCategory@@, app, appstore, app store, Mac","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaDescription":"Learn about @@storyTitle@@ on Mac App Store. Download @@featuredAppName@@ and enjoy it on your iPhone, iPad, and Mac.","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaDescription.Collection.ManyMore":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, and many more on Mac App Store. Enjoy these apps on your iPhone, iPad, and Mac.","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaDescription.Collection.One":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@ and many more on Mac App Store. Enjoy these apps on your iPhone, iPad, and Mac.","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaDescription.Collection.Three":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, and many more on Mac App Store. Enjoy these apps on your iPhone, iPad, and Mac.","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaDescription.Collection.Two":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, and many more on Mac App Store. Enjoy these apps on your iPhone, iPad, and Mac.","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaKeywords":"@@storyTitle@@, @@featuredAppName@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes, Mac","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaKeywords.Collection.One":"@@storyTitle@@, @@featuredAppName1@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes, Mac","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaKeywords.Collection.Three":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes, Mac","WEA.EditorialItemProductPages.MacAppStore.Meta.PageMetaKeywords.Collection.Two":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes, Mac","WEA.EditorialItemProductPages.Meta.PageMetaDescription":"Learn about @@storyTitle@@ on App Store. ","WEA.EditorialItemProductPages.Meta.PageMetaDescription.Collection.ManyMore":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, and many more on App Store. Enjoy these apps on your iPhone, iPad, and iPod touch.","WEA.EditorialItemProductPages.Meta.PageMetaDescription.Collection.One":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@ and many more on App Store. Enjoy these apps on your iPhone, iPad, and iPod touch.","WEA.EditorialItemProductPages.Meta.PageMetaDescription.Collection.Three":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, and @@featuredAppName3@@ on App Store. Enjoy these apps on your iPhone, iPad, and iPod touch.","WEA.EditorialItemProductPages.Meta.PageMetaDescription.Collection.Two":"Learn about collection @@storyTitle@@ featuring @@featuredAppName1@@, @@featuredAppName2@@, and many more on App Store. Enjoy these apps on your iPhone, iPad, and iPod touch.","WEA.EditorialItemProductPages.Meta.PageMetaKeywords":"@@storyTitle@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes","WEA.EditorialItemProductPages.Meta.PageMetaKeywords.Collection.One":"@@storyTitle@@, @@featuredAppName1@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes","WEA.EditorialItemProductPages.Meta.PageMetaKeywords.Collection.Three":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@featuredAppName3@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes","WEA.EditorialItemProductPages.Meta.PageMetaKeywords.Collection.Two":"@@storyTitle@@, @@featuredAppName1@@, @@featuredAppName2@@, @@applicationCategory@@, ios apps, app, appstore, app store, iphone, ipad, ipod touch, itouch, itunes","WEA.EditorialItemProductPages.Meta.title":"@@storyTitle@@ : App Store Story","WEA.EditorialItemProductPages.Social.title.AOTD":"App of the Day: @@appName@@","WEA.EditorialItemProductPages.Social.title.ASA":"@@storyTitle@@: @@appName@@","WEA.EditorialItemProductPages.Social.title.GOTD":"Game of the Day: @@appName@@","WEA.EditorialItemProductPages.Social.title.IAP":"In-App Purchase: @@appName@@","WEA.Error.Carrier.Install.AM":"To Start listening, install Apple Music on your device","WEA.Error.Carrier.Installed":"Already have the app?","WEA.Error.Carrier.Open.Action":"Get it on @@appname@@","WEA.Error.Generic.AppleMusicLogo.Text":"Apple Music","WEA.Error.Generic.GooglePlay":"Google Play","WEA.Error.Generic.Install.AM":"If this link does not work, you might need to @@installLink@@.","WEA.Error.Generic.Install.AM.Link.Text":"install Apple Music","WEA.Error.Generic.Meta.PageKeywords":"Apple Music","WEA.Error.Generic.Meta.PageKeywords.AppleBooks":"Apple Books","WEA.Error.Generic.Meta.PageKeywords.iTunes":"iTunes Store","WEA.Error.Generic.Meta.PageTitle":"Connecting to Apple Music.","WEA.Error.Generic.Open.AM":"Open in Apple Music","WEA.Error.Generic.Open.Action.AM":"Open in","WEA.Error.Generic.Subtitle":"If you don’t have iTunes, @@downloadLink@@. If you have iTunes and it doesn’t open automatically, try opening it from your dock or Windows task bar.","WEA.Error.Generic.Subtitle.DownloadLink.Text":"download it for free","WEA.Error.Generic.Subtitle.DownloadLink.URL":"http://www.apple.com/itunes/download/","WEA.Error.Generic.Title":"Connecting to Apple Music","WEA.Error.Generic.Title.AppleBooks":"Connecting to Apple Books","WEA.Error.Generic.Title.Apps":"Connecting to App Store","WEA.Error.Generic.Title.Connecting":"Connecting","WEA.Error.Generic.Title.Podcasts":"Connecting to Apple Podcasts","WEA.Error.Generic.Title.iTunes":"Connecting to the iTunes Store.","WEA.Error.NativeMissing.AM.Subtitle":"You need iTunes to use Apple Music","WEA.Error.NativeMissing.Other.AM":"Get Apple Music on iOS, Android, Mac, and Windows","WEA.Error.NativeMissing.Other.LearnMore.link":"https://www.apple.com/itunes/download/","WEA.Error.NativeMissing.Other.iTunes":"Get iTunes on iOS, Android, Mac, and Windows","WEA.Error.NativeMissing.iTunes.Download.link":"https://www.apple.com/itunes/download/","WEA.Error.NativeMissing.iTunes.Download.text":"Download iTunes","WEA.Error.NativeMissing.iTunes.TV.Subtitle":"Download iTunes below to start watching.","WEA.Error.NativeMissing.iTunes.Title":"We could not find iTunes on your computer.","WEA.Error.NotFound.Meta.Title":"This content can’t be found.","WEA.Error.NotFound.general":"The page you're looking for cannot be found.","WEA.GroupingPages.CTA.GoToWebsite":"Go To","WEA.InvoicePages.Receipts.Instructions":"Try again on an iPhone, iPad, or Mac, or use iTunes on a PC","WEA.InvoicePages.Receipts.Meta.Description":"You can't turn off receipts on this device.","WEA.InvoicePages.Receipts.Meta.Title":"Receipts","WEA.InvoicePages.Receipts.Title":"You can't turn off receipts on this device.","WEA.LocalNav.CTA.AppName.TV":"Apple TV app","WEA.LocalNav.CTA.DownloadiTunes":"Download iTunes","WEA.LocalNav.CTA.DownloadiTunes.url":"https://www.apple.com/itunes/download/","WEA.LocalNav.CTA.FreeTrial":"Try It Now","WEA.LocalNav.CTA.FreeTrial.url":"https://itunes.apple.com/subscribe?app=music","WEA.LocalNav.CTA.iTunesStore.TV":"Learn More","WEA.LocalNav.Preview.AM":"Preview","WEA.LocalNav.Preview.AppStore":"Preview","WEA.LocalNav.Preview.Books":"Preview","WEA.LocalNav.Preview.MAS":"Preview","WEA.LocalNav.Preview.Podcasts":"Preview","WEA.LocalNav.Preview.TV":"Preview","WEA.LocalNav.Preview.iBooks":"Preview","WEA.LocalNav.Preview.iTunes":"Preview","WEA.LocalNav.Store.AM":"Apple Music","WEA.LocalNav.Store.AppStore":"App Store","WEA.LocalNav.Store.Books":"Apple Books","WEA.LocalNav.Store.MAS":"Mac App Store","WEA.LocalNav.Store.Podcasts":"Apple Podcasts","WEA.LocalNav.Store.TV":"Apple TV","WEA.LocalNav.Store.iBooks":"Apple Books","WEA.LocalNav.Store.iTunes":"iTunes","WEA.LocalNav.Title.Preview.AM":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.AppStore":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.Books":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.MAS":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.Podcasts":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.TV":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.iBooks":"@@product@@ @@qualifier@@","WEA.LocalNav.Title.Preview.iTunes":"@@product@@ @@qualifier@@","WEA.MusicPages.Riaa.Explicit.AX":"Parental Advisory: Explicit Lyrics.","WEA.PaddleNav.Next.AX":"Next","WEA.PaddleNav.Previous.AX":"Previous","WEA.StarRating.Separator":" • ","WFA.Common.Meta.Twitter.site":"@Apple"}}</script><script type="fastboot/shoebox" id="shoebox-media-api-cache-apps">{".v1.catalog.us.apps.1444383602.l.en-us.platform.web.additionalplatforms.appletv.2cipad.2ciphone.2cmac.extend.custompromotionaltext.2ccustomscreenshotsbytype.2cdescription.2cdeveloperinfo.2cdistributionkind.2ceditorialvideo.2cfilesizebydevice.2cmessagesscreenshots.2cprivacy.2cprivacypolicyurl.2crequirementsbydevicefamily.2csupporturlforlanguage.2cversionhistory.2cwebsiteurl.include.app-events.2cgenres.2cdeveloper.2creviews.2cmerchandised-in-apps.2ccustomers-also-bought-apps.2cdeveloper-other-apps.2ctop-in-apps.2crelated-editorial-items.limit.5bmerchandised-in-apps.5d.20.omit.5bresource.5d.autos.meta.robots.sparselimit.5bapps.3arelated-editorial-items.5d.10":"{\"x\":1660230713653,\"d\":[{\"id\":\"1444383602\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1444383602?l=en-US\",\"attributes\":{\"distributionKind\":\"APP_STORE\",\"artistName\":\"Time Base Technology Limited\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/goodnotes-5/id1444383602\",\"isIOSBinaryMacOSCompatible\":false,\"privacy\":{\"privacyTypes\":[{\"privacyType\":\"Data Not Linked to You\",\"identifier\":\"DATA_NOT_LINKED_TO_YOU\",\"description\":\"The following data may be collected but it is not linked to your identity:\",\"dataCategories\":[{\"dataCategory\":\"Identifiers\",\"identifier\":\"IDENTIFIERS\"},{\"dataCategory\":\"Usage Data\",\"identifier\":\"USAGE_DATA\"},{\"dataCategory\":\"Diagnostics\",\"identifier\":\"DIAGNOSTICS\"}]}]},\"reviewsRestricted\":false,\"deviceFamilies\":[\"mac\",\"iphone\",\"ipad\",\"ipod\"],\"chartPositions\":{\"appStore\":{\"position\":84,\"genreName\":\"Productivity\",\"genre\":6007,\"chart\":\"top-free\",\"chartLink\":\"https://apps.apple.com/us/charts/iphone/productivity-apps/6007\"}},\"isPreorder\":false,\"platformAttributes\":{\"osx\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/e4/39/2de4394a-cb8b-89e2-712c-34eb07ba241e/AppIcon-0-2x_U007euniversal-0-0-0-4-0-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"fefefe\",\"textColor2\":\"e0f5fa\",\"textColor3\":\"cbcbcb\",\"textColor4\":\"b3c4c8\"},\"hasMessagesExtension\":false,\"versionHistory\":[{\"versionDisplay\":\"5.9.32\",\"releaseNotes\":\"- Title suggestions when you leave an Untitled document\\n- Preserve typing attributes when deleting to a new line in a text box\\n- Fixing bugs in the account creation process\\n- Improvements on the notes collection flow\",\"releaseDate\":\"2022-08-08\",\"releaseTimestamp\":\"2022-08-08T09:32:32Z\"},{\"versionDisplay\":\"5.9.30\",\"releaseNotes\":\"- Fix a bug where a document will be unshared if moved to a new folder\\n- Other bug fixes and improvements\",\"releaseDate\":\"2022-08-01\",\"releaseTimestamp\":\"2022-08-01T09:39:58Z\"},{\"versionDisplay\":\"5.9.28\",\"releaseNotes\":\"- Show notebook indicator in the template editor.\\n- Use the right language to recognize text with the lasso tool.\",\"releaseDate\":\"2022-07-27\",\"releaseTimestamp\":\"2022-07-27T08:34:21Z\"},{\"versionDisplay\":\"5.9.26\",\"releaseNotes\":\"- vertical scrolling is now smoother\\n- we will now animate correctly when a new page is inserted\",\"releaseDate\":\"2022-07-19\",\"releaseTimestamp\":\"2022-07-19T14:34:49Z\"},{\"versionDisplay\":\"5.9.25\",\"releaseNotes\":\"- Fix scrolling lag\\n- Fix crash in searches inside a document\\n- Improved notes upload process with meta prefill, thumbnail preview, and update copies.\",\"releaseDate\":\"2022-07-07\",\"releaseTimestamp\":\"2022-07-07T19:27:31Z\"},{\"versionDisplay\":\"5.9.22\",\"releaseNotes\":\"- Cut, copy and add comments from the Image context menu.\\n- Several improvements to the notes upload process.\\n- Improvements on handwriting recognition for Japanese and Simplified Chinese.\",\"releaseDate\":\"2022-06-29\",\"releaseTimestamp\":\"2022-06-29T06:52:37Z\"},{\"versionDisplay\":\"5.9.20\",\"releaseNotes\":\"- Improved link sharing UX and performance\\n- Improved content menu to prevent overlapping toolbar\",\"releaseDate\":\"2022-06-21\",\"releaseTimestamp\":\"2022-06-21T07:19:03Z\"},{\"versionDisplay\":\"5.9.18\",\"releaseNotes\":\"This week, we published our handwriting recognition engine, making several symbol improvements in Japanese and traditional Chinese.\",\"releaseDate\":\"2022-06-07\",\"releaseTimestamp\":\"2022-06-07T09:55:05Z\"},{\"versionDisplay\":\"5.9.16\",\"releaseNotes\":\"We continue working on providing the best experience to our users. In this version, the main changes are:\\n\\n- Make handwriting recognition stronger.\\n- Improvements to the precision eraser to make it more robust in some scenarios.\\n- Improved translations.\\n- Prevent the deletion of text boxes after tapping on backspace.\",\"releaseDate\":\"2022-06-01\",\"releaseTimestamp\":\"2022-06-01T16:30:29Z\"},{\"versionDisplay\":\"5.9.14\",\"releaseNotes\":\"We continue working on providing the best experience to our users. In this version, the main changes are:\\n\\n- New onboarding experience.\\n- Improved translations.\",\"releaseDate\":\"2022-05-24\",\"releaseTimestamp\":\"2022-05-24T10:46:16Z\"},{\"versionDisplay\":\"5.9.12\",\"releaseNotes\":\"- the shared menu is always shown when dragging text\",\"releaseDate\":\"2022-05-09\",\"releaseTimestamp\":\"2022-05-09T16:42:25Z\"},{\"versionDisplay\":\"5.9.10\",\"releaseNotes\":\"- Improved textbox UX\\n- Improved link handling\\n- Fixed text alignment on some actionable buttons\\n- Improved handwriting recognition\",\"releaseDate\":\"2022-05-03\",\"releaseTimestamp\":\"2022-05-03T04:07:29Z\"},{\"versionDisplay\":\"5.9.8\",\"releaseNotes\":\"- Improvements in Japanese and Chinese handwritten recognition\",\"releaseDate\":\"2022-04-25\",\"releaseTimestamp\":\"2022-04-25T07:59:15Z\"},{\"versionDisplay\":\"5.9.6\",\"releaseNotes\":\"- Text tool UX improvements\\n- Fixes an issue with restoring purchases\\n- Fixes a crash that may occur with auto backup\",\"releaseDate\":\"2022-04-19\",\"releaseTimestamp\":\"2022-04-19T10:34:43Z\"},{\"versionDisplay\":\"5.9.4\",\"releaseNotes\":\"Bug fixes and stability improvements\",\"releaseDate\":\"2022-04-12\",\"releaseTimestamp\":\"2022-04-12T06:28:54Z\"},{\"versionDisplay\":\"5.9.3\",\"releaseNotes\":\"This update fixes battery drain and heat issues for some users\",\"releaseDate\":\"2022-04-10\",\"releaseTimestamp\":\"2022-04-10T01:44:30Z\"},{\"versionDisplay\":\"5.9.0\",\"releaseNotes\":\"Download GoodNotes 5 for free on iPad, Mac, and iPhone and create your first 3 notebooks for free.\\n\\nGet the full GoodNotes experience with a One-Time Unlock In-App Purchase (IAP):\\n- Create unlimited notebooks\\n- Unlock handwriting recognition to search handwritten notes\\n- Import documents via e-mail\\n- Prioritized email support\",\"releaseDate\":\"2022-04-05\",\"releaseTimestamp\":\"2022-04-05T10:02:48Z\"},{\"versionDisplay\":\"5.8.13\",\"releaseNotes\":\"- Fixed a crash when scrolling and drawing for users in certain countries\",\"releaseDate\":\"2022-04-01\",\"releaseTimestamp\":\"2022-04-01T01:38:42Z\"},{\"versionDisplay\":\"5.8.12\",\"releaseNotes\":\"Better, faster, stronger. We cleaned up behind the curtain, so your GoodNotes is even better.\",\"releaseDate\":\"2022-03-29\",\"releaseTimestamp\":\"2022-03-29T10:38:51Z\"},{\"versionDisplay\":\"5.8.10\",\"releaseNotes\":\"Better, faster, stronger. We cleaned up behind the curtain, so your GoodNotes is even better.\",\"releaseDate\":\"2022-03-26\",\"releaseTimestamp\":\"2022-03-26T14:56:36Z\"},{\"versionDisplay\":\"5.8.8\",\"releaseNotes\":\"This version includes several performance and stability improvements:\\n- Fix selection on the library.\\n- Add the ability to clear past searches and open.\\n- Hide the previous eraser cursor when initiating a new touch event.\\n- Improve animations and performance in the documents viewer.\\n- Improvements to the scale and position for external display when mirroring.\\n- Fix page view mouse wheel scrolling on macOS 12.3.\",\"releaseDate\":\"2022-03-22\",\"releaseTimestamp\":\"2022-03-22T21:45:00Z\"},{\"versionDisplay\":\"5.8.6\",\"releaseNotes\":\"• Re-introducing the Precision Eraser: edit your notes more precisely with the ability to erase pixel by pixel.\\n• Combine pages and documents: now you can easily move pages from one notebook to another, or combine entire documents into one\\n    1. Under each document thumbnail collection, you can:\\n        - select multiple pages and move them to another document.\\n        - drag and drop multiple pages to rearrange your pagination.\\n    2. From the Document tab, you can move and combine the entire document into another document.\\n    3. New QuickNotes now can be moved to another document when done.\\n\\n• Bug fixes:\\n\\t- fixed an infinite load when dragging a page thumbnail onto a page on Mac  \\n\\t- phrase matches will appear first in search results\\n\\t- improved page animations\\n\\t- comments will be preserved when moving pages\\n\\t- text will no longer outgrow their buttons\\n\\t- fixed a crash when deleting pages\",\"releaseDate\":\"2022-03-01\",\"releaseTimestamp\":\"2022-03-01T10:38:12Z\"},{\"versionDisplay\":\"5.8.2\",\"releaseNotes\":\"- Fixed extra padding for toolbar on iPad mini\\n- Improved edit links and text links visibility\\n- Improved supportID copying\\n- Fixed some visual glitches when exporting document\\n- Fixed wrong page order when duplicate document\",\"releaseDate\":\"2022-02-15\",\"releaseTimestamp\":\"2022-02-15T08:07:12Z\"},{\"versionDisplay\":\"5.8.0\",\"releaseNotes\":\"Performance improvements in orientation switch and large documents.\",\"releaseDate\":\"2022-02-08\",\"releaseTimestamp\":\"2022-02-08T09:51:55Z\"},{\"versionDisplay\":\"5.7.60\",\"releaseNotes\":\"Fixes search results aren't highlighted on PDFs\",\"releaseDate\":\"2022-02-01\",\"releaseTimestamp\":\"2022-02-01T09:46:08Z\"}],\"hasPrivacyPolicyText\":true,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"Dutch\",\"tag\":\"nl\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Thai\",\"tag\":\"th\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"}],\"customAttributes\":{\"default\":{\"default\":{\"customScreenshotsByType\":{\"mac\":[{\"width\":2880,\"height\":1800,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/4e/cc/59/4ecc59f6-338b-5b40-5bfa-ab8e9dc8b852/pr_source.jpg/{w}x{h}{c}.{f}\",\"bgColor\":\"e8ecf2\",\"textColor1\":\"06090c\",\"textColor2\":\"382c23\",\"textColor3\":\"33363a\",\"textColor4\":\"5b524c\"},{\"width\":2880,\"height\":1800,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/d7/b6/e5/d7b6e58c-7aca-9458-0d27-f6c686a0ef91/pr_source.jpg/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"ffffff\",\"textColor2\":\"b0c5f5\",\"textColor3\":\"cbcbcb\",\"textColor4\":\"8d9dc4\"},{\"width\":2880,\"height\":1800,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/43/d8/30/43d830b1-374b-ae49-b544-e64908b22991/pr_source.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"2e3044\",\"textColor3\":\"333333\",\"textColor4\":\"585969\"},{\"width\":2880,\"height\":1800,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/22/12/c5/2212c577-5a58-5f13-a9b0-8e4ec889c9a2/pr_source.jpg/{w}x{h}{c}.{f}\",\"bgColor\":\"feffff\",\"textColor1\":\"000000\",\"textColor2\":\"272a42\",\"textColor3\":\"333333\",\"textColor4\":\"525567\"},{\"width\":2880,\"height\":1800,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/5d/c7/34/5dc734a9-a973-ebaf-1ce7-13c143b81ebf/pr_source.png/{w}x{h}{c}.{f}\",\"bgColor\":\"0a1a3c\",\"textColor1\":\"ffffff\",\"textColor2\":\"ff8a89\",\"textColor3\":\"cdd1d7\",\"textColor4\":\"cd7379\"}]}}}},\"privacyPolicyUrl\":\"https://goodnotes.com/privacy-policy/\",\"supportsGameController\":false,\"releaseDate\":\"2019-01-15\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":851337271,\"supportURLForLanguage\":\"https://support.goodnotes.com/hc/en-us/categories/360000080575-GoodNotes-5\",\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Features124/v4/2a/34/5e/2a345ec2-5eb1-277f-2dd9-97b74b3fe2bc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"68abff\",\"textColor1\":\"111212\",\"textColor2\":\"121210\",\"textColor3\":\"1f2a35\",\"textColor4\":\"202a34\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"macOS 10.15 or later\",\"subtitle\":\"Note-Taking \u0026 PDF Markup\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.goodnotesapp.x\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2011-2022 GoodNotes Limited\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"10.15\",\"hasFamilyShareableInAppPurchases\":true,\"runsOnIntel\":true,\"editorialNotes\":{\"standard\":\"GoodNotes 5 is a combination digital notepad and PDF markup tool. And it’s very good at both. As a notepad, GoodNotes covers all the bases. Create notes with your keyboard or handwrite them with Apple Pencil. The app transforms your hand-drawn shapes into geometrically perfect ones. It’s also capable of recognizing your handwriting (even when you can’t) and converting it to text. \",\"short\":\"‣ For an app that lets you convert handwriting into text, try Goodnotes.\",\"tagline\":\"Collaborate on your notes\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Time Base Technology Limited\",\"preflightPackageUrl\":\"https://osxapps.itunes.apple.com/itunes-assets/Purple112/v4/40/ab/85/40ab85cd-fb02-5c65-f858-81b5eb909f8b/qvi4051038314050308806.pfpkg\",\"isSiriSupported\":false,\"requiresRosetta\":false,\"languageList\":[\"English\",\"Dutch\",\"French\",\"German\",\"Italian\",\"Japanese\",\"Korean\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Thai\",\"Traditional Chinese\",\"Turkish\"],\"messagesScreenshots\":{},\"description\":{\"standard\":\"Use GoodNotes on Mac to access your digital notes wherever you work. With iCloud, your digital notes will be synced on all your devices, making the GoodNotes Mac app the perfect partner to access your digital notes on your computer.\\n\\nNEVER LOSE YOUR NOTES\\n• Search your handwritten notes, typed text, PDF text, document outlines, folder titles, and document titles.\\n• Create unlimited folders and subfolders, or mark your Favorite ones to keep everything organized.\\n• Create custom outlines for easier navigation through your documents.\\n• Add hyperlinks to external websites, videos, articles to build your knowledge map.\\n• Back up all your notes to iCloud, Google Drive, Dropbox, and OneDrive and sync across all devices so you will never lose them.\\n\\nESCAPE THE LIMITS OF ANALOG PAPER\\n• Move, resize, and rotate your handwriting or change colors.\\n• Draw perfect shapes and lines with the Shape Tool.\\n• Navigate through imported PDFs with existing hyperlinks.\\n• Choose to erase the entire stroke, only parts of it, or only highlighters to leave the ink intact.\\n• Select to edit or move a specific object with the Lasso Tool.\\n• Add, create, or import your stickers, pictures, tables, diagrams, and more with Elements to enrich your notes.\\n• Choose from a large set of beautiful covers and useful paper templates, including Blank Paper, Ruled Paper, Cornell Paper, Checklists, To-dos, Planners, Music Paper, Flashcards, and more.\\n• Upload any PDF or image as a custom notebook cover or paper template for more customization.\\n\\nSHARE, COLLABORATE, \u0026 PRESENT FROM YOUR MAC\\n• Present a lecture, a lesson, a business plan, a brainstorming result, or your group study without distractions when you connect your Mac via AirPlay or HDMI to an external screen.\\n• Use Laser Pointer on your iPad to guide your audience’s attention during your presentation.\\n• Collaborate with others on the same document using a sharable link.\\n\\n---\\n\\nWebsite: www.goodnotes.com\\nTwitter: @goodnotesapp\\nInstagram: @goodnotes.app\\nPinterest: @goodnotesapp\\nTikTok: @goodnotesapp\"},\"websiteUrl\":\"https://www.goodnotes.com\",\"runsOnAppleSilicon\":true,\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1444383602\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851337271\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":423289894}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1444383602\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851337271\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":423289894}]}]},\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Purple122/v4/8d/e1/b6/8de1b640-4193-a945-6aab-3332a885cfc4/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-sRGB-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"181818\",\"textColor2\":\"272727\",\"textColor3\":\"464646\",\"textColor4\":\"525252\"},\"hasMessagesExtension\":false,\"versionHistory\":[{\"versionDisplay\":\"5.9.32\",\"releaseNotes\":\"- Title suggestions when you leave an Untitled document\\n- Preserve typing attributes when deleting to a new line in a text box\\n- Fixing bugs in the account creation process\\n- Improvements on the notes collection flow\",\"releaseDate\":\"2022-08-08\",\"releaseTimestamp\":\"2022-08-08T09:30:42Z\"},{\"versionDisplay\":\"5.9.30\",\"releaseNotes\":\"- Fix a bug where a document will be unshared if moved to a new folder\\n- Other bug fixes and improvements\",\"releaseDate\":\"2022-08-01\",\"releaseTimestamp\":\"2022-08-01T09:38:46Z\"},{\"versionDisplay\":\"5.9.28\",\"releaseNotes\":\"- Show notebook indicator in the template editor.\\n- Use the right language to recognize text with the lasso tool.\",\"releaseDate\":\"2022-07-27\",\"releaseTimestamp\":\"2022-07-27T08:33:08Z\"},{\"versionDisplay\":\"5.9.26\",\"releaseNotes\":\"- vertical scrolling is now smoother\\n- we will now animate correctly when a new page is inserted\",\"releaseDate\":\"2022-07-19\",\"releaseTimestamp\":\"2022-07-19T14:33:41Z\"},{\"versionDisplay\":\"5.9.25\",\"releaseNotes\":\"- Fix scrolling lag\\n- Fix crash in searches inside a document\\n- Improved notes upload process with meta prefill, thumbnail preview, and update copies.\\n- For some users, the app was stuck preparing the library during the first launch. It is fixed now.\\n\",\"releaseDate\":\"2022-07-06\",\"releaseTimestamp\":\"2022-07-06T20:32:30Z\"},{\"versionDisplay\":\"5.9.24\",\"releaseNotes\":\"- Fix scrolling lag\\n- Fix crash in searches inside a document\\n- Improved notes upload process with meta prefill, thumbnail preview, and update copies.\",\"releaseDate\":\"2022-07-06\",\"releaseTimestamp\":\"2022-07-06T02:29:40Z\"},{\"versionDisplay\":\"5.9.22\",\"releaseNotes\":\"- Cut, copy and add comments from the Image context menu.\\n- Several improvements to the notes upload process.\\n- Improvements on handwriting recognition for Japanese and Simplified Chinese.\",\"releaseDate\":\"2022-06-29\",\"releaseTimestamp\":\"2022-06-29T06:51:34Z\"},{\"versionDisplay\":\"5.9.20\",\"releaseNotes\":\"- Improved link sharing UX and performance\\n- Improved content menu to prevent overlapping toolbar\",\"releaseDate\":\"2022-06-21\",\"releaseTimestamp\":\"2022-06-21T07:18:33Z\"},{\"versionDisplay\":\"5.9.18\",\"releaseNotes\":\"This week, we published our handwriting recognition engine, making several symbol improvements in Japanese and traditional Chinese.\",\"releaseDate\":\"2022-06-07\",\"releaseTimestamp\":\"2022-06-07T09:43:47Z\"},{\"versionDisplay\":\"5.9.16\",\"releaseNotes\":\"We continue working on providing the best experience to our users. In this version, the main changes are:\\n\\n- Make handwriting recognition stronger.\\n- Improvements to the precision eraser to make it more robust in some scenarios.\\n- Improved translations.\\n- Prevent the deletion of text boxes after tapping on backspace.\",\"releaseDate\":\"2022-06-01\",\"releaseTimestamp\":\"2022-06-01T16:29:38Z\"},{\"versionDisplay\":\"5.9.14\",\"releaseNotes\":\"We continue working on providing the best experience to our users. In this version, the main changes are:\\n\\n- New onboarding experience.\\n- Improved translations.\",\"releaseDate\":\"2022-05-24\",\"releaseTimestamp\":\"2022-05-24T10:45:01Z\"},{\"versionDisplay\":\"5.9.12\",\"releaseNotes\":\"- the shared menu is always shown when dragging text\",\"releaseDate\":\"2022-05-09\",\"releaseTimestamp\":\"2022-05-09T16:41:21Z\"},{\"versionDisplay\":\"5.9.10\",\"releaseNotes\":\"- Improved textbox UX\\n- Improved link handling\\n- Fixed text alignment on some actionable buttons\\n- Improved handwriting recognition\",\"releaseDate\":\"2022-05-03\",\"releaseTimestamp\":\"2022-05-03T04:06:19Z\"},{\"versionDisplay\":\"5.9.8\",\"releaseNotes\":\"- Improvements in Japanese and Chinese handwritten recognition\",\"releaseDate\":\"2022-04-25\",\"releaseTimestamp\":\"2022-04-25T07:58:23Z\"},{\"versionDisplay\":\"5.9.6\",\"releaseNotes\":\"- Text tool UX improvements\\n- Fixes an issue with restoring purchases\\n- Fixes a crash that may occur with auto backup\",\"releaseDate\":\"2022-04-19\",\"releaseTimestamp\":\"2022-04-19T10:23:30Z\"},{\"versionDisplay\":\"5.9.4\",\"releaseNotes\":\"Bug fixes and stability improvements\",\"releaseDate\":\"2022-04-12\",\"releaseTimestamp\":\"2022-04-12T06:27:08Z\"},{\"versionDisplay\":\"5.9.3\",\"releaseNotes\":\"This update fixes battery drain and heat issues for some users\",\"releaseDate\":\"2022-04-10\",\"releaseTimestamp\":\"2022-04-10T01:43:16Z\"},{\"versionDisplay\":\"5.9.1\",\"releaseNotes\":\"- Fix a crash that prevents users from upgrading to full version\",\"releaseDate\":\"2022-04-06\",\"releaseTimestamp\":\"2022-04-06T11:02:38Z\"},{\"versionDisplay\":\"5.9.0\",\"releaseNotes\":\"Download GoodNotes 5 for free on iPad, Mac, and iPhone and create your first 3 notebooks for free.\\n\\nGet the full GoodNotes experience with a One-Time Unlock In-App Purchase (IAP):\\n- Create unlimited notebooks\\n- Unlock handwriting recognition to search handwritten notes\\n- Import documents via e-mail\\n- Prioritized email support\",\"releaseDate\":\"2022-04-05\",\"releaseTimestamp\":\"2022-04-05T10:03:35Z\"},{\"versionDisplay\":\"5.8.13\",\"releaseNotes\":\"- Fixed a crash when scrolling and drawing for users in certain countries\",\"releaseDate\":\"2022-04-01\",\"releaseTimestamp\":\"2022-04-01T01:37:35Z\"},{\"versionDisplay\":\"5.8.12\",\"releaseNotes\":\"- Fixing lag in zoom window when using horizontal direction.\\n- Fix unexpected toolbar leading space.\\n- Memory optimization for scan documents.\\n- Minor improvement related to text tool when editing in compact mode.\\n- Minor adjustments in the layout for paging view.\",\"releaseDate\":\"2022-03-29\",\"releaseTimestamp\":\"2022-03-29T10:37:24Z\"},{\"versionDisplay\":\"5.8.10\",\"releaseNotes\":\"No longer crash on iOS 15.4 when creating a new notebook.\",\"releaseDate\":\"2022-03-27\",\"releaseTimestamp\":\"2022-03-27T14:56:06Z\"},{\"versionDisplay\":\"5.8.8\",\"releaseNotes\":\"This version includes several performance and stability improvements:\\n\\n- Add the ability to clear past searches and open.\\n- Hide the previous eraser cursor when initiating a new touch event.\\n- Improve animations and performance in the documents viewer.\\n- Improvements to the scale and position for external display when mirroring.\",\"releaseDate\":\"2022-03-22\",\"releaseTimestamp\":\"2022-03-22T21:44:18Z\"},{\"versionDisplay\":\"5.8.7\",\"releaseNotes\":\"- Fixed an infinite load when dragging a page thumbnail onto a page on Mac  \\n- Phrase matches will appear first in search results\\n- Improved page animations\\n- Comments will be preserved when moving pages\\n- Text will no longer outgrow their buttons\\n- Fixed a crash when deleting pages\\n- Fixed a crash in handwriting recognition\",\"releaseDate\":\"2022-03-03\",\"releaseTimestamp\":\"2022-03-03T07:51:59Z\"},{\"versionDisplay\":\"5.8.6\",\"releaseNotes\":\"- fixed an infinite load when dragging a page thumbnail onto a page on Mac  \\n- phrase matches will appear first in search results\\n- improved page animations\\n- comments will be preserved when moving pages\\n- text will no longer outgrow their buttons\\n- fixed a crash when deleting pages\\n- fixed a crash in handwriting recognition\",\"releaseDate\":\"2022-03-01\",\"releaseTimestamp\":\"2022-03-01T10:37:13Z\"}],\"hasPrivacyPolicyText\":true,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"Dutch\",\"tag\":\"nl\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Thai\",\"tag\":\"th\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"}],\"customAttributes\":{\"default\":{\"default\":{\"customScreenshotsByType\":{\"ipadPro_2018\":[{\"width\":2048,\"height\":2732,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/1e/69/27/1e6927c0-cce6-d625-403c-e815707f309c/d9f3f6db-0fda-41e0-8a3b-03591e8be118_iPad-12.9-1-Hand-Writing__U0028Medicine_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/43/95/83/439583a9-1c56-e0cb-d4a9-48cea3400889/48477cc9-1fa5-40cf-a73b-cfa9f7df2092_iPad-12.9-3-Annotation.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/27/af/91/27af91fd-9838-784f-c459-d9b2258efedd/90d8430b-b079-478c-a032-3b42a4c565b3_iPad-12.9-2-Search-And-Find.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"000072\",\"textColor3\":\"333333\",\"textColor4\":\"33338e\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/8c/c5/0f/8cc50f8f-f7fc-c7cb-1f05-9fe8f91780ee/c25f2a81-2756-42b7-8a35-423d1147aa57_iPad-12.9-11-External-Links.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/fa/f8/4e/faf84e05-c8b9-cf14-581a-72ef034c322c/25527e51-db19-4e69-bc7a-be9a52c1ea06_iPad-12.9-8-Customize-Notebooks.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"181a1e\",\"textColor3\":\"333333\",\"textColor4\":\"46484b\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/PurpleSource112/v4/8d/0f/eb/8d0febdd-d48d-a4da-fb45-48dc75371785/ba044476-0402-48bb-a96c-dcf21828466e_iPad-12.9-6-Monthly-Planner__U0028Student_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"20283a\",\"textColor3\":\"333333\",\"textColor4\":\"4c5361\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Purple122/v4/2b/9f/ac/2b9fac79-2229-0b8a-69f9-82f597f1c702/5a7269b2-2adc-49db-83f8-0f2cbd1e31b5_iPad-12.9-13-Custom-Outline.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/35/18/97/351897bd-416b-d86f-2453-ce52773daa99/75361243-eada-4922-b2c5-df86c28da2ad_iPad-12.9-5-Split-Screens.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"232837\",\"textColor3\":\"333333\",\"textColor4\":\"4f535f\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/15/24/4d/15244da0-fc0d-cc82-bc4f-9a3b99dd820b/846604eb-fe29-4775-972e-b932dc1ae13b_iPad-12.9-9-Teaching-And-Presentation__U0028Chemistry_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"ffffff\",\"textColor2\":\"ecc59b\",\"textColor3\":\"d4d4d5\",\"textColor4\":\"c5a685\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/40/5b/5f/405b5fd0-69a4-c07f-9df6-4a2a593dca98/cf59e802-2648-4d9e-9d11-fc5079185728_iPad-12.9-10-Dark-Mode.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"c1fe06\",\"textColor2\":\"299bfb\",\"textColor3\":\"a2d30e\",\"textColor4\":\"2984d2\"}],\"ipadPro\":[{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/4f/9c/59/4f9c5955-ca19-5e76-c1cd-d19f869876c8/69d17d36-bffe-40c5-88b1-90eb0cc661eb_iPad-12.9-1-Hand-Writing__U0028Medicine_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Purple112/v4/6c/25/88/6c258858-ab3a-0357-4b8b-6895eb4caaca/c7aa4c5c-f7a7-4a6b-a62e-41750d01a2a0_iPad-12.9-3-Annotation.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/d3/cf/76/d3cf7619-9760-a82d-c5e8-7d82de46c805/6137afd0-6436-46fc-b7d6-6450e632d353_iPad-12.9-2-Search-And-Find.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"000072\",\"textColor3\":\"333333\",\"textColor4\":\"33338e\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/24/86/b6/2486b60f-2f5f-50bc-64a6-7595c53a82fd/50cf9830-1e19-4a34-b233-2d24f40d4185_iPad-12.9-11-External-Links.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple112/v4/ce/66/13/ce6613ae-1a0f-39c4-2b29-93ea6c1f856c/47633ee9-b9da-4e6b-82b1-663b3300d847_iPad-12.9-8-Customize-Notebooks.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"181a1e\",\"textColor3\":\"333333\",\"textColor4\":\"46484b\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/PurpleSource122/v4/48/ef/e9/48efe968-d79b-64f9-c47c-145f39ef9baf/64d3372a-f09d-493e-ac9d-6079682688da_iPad-12.9-6-Monthly-Planner__U0028Student_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"20283a\",\"textColor3\":\"333333\",\"textColor4\":\"4c5361\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/87/05/fa/8705faee-0995-09bc-319b-9542b23e3ab3/8d64acf4-138f-4467-97e9-1c70f5d1d196_iPad-12.9-13-Custom-Outline.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"222839\",\"textColor3\":\"333333\",\"textColor4\":\"4e5360\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/d1/82/b7/d182b790-7015-cf8e-b221-58f483c76dd2/8f37d40c-516f-42b1-adc8-aa2edb19497d_iPad-12.9-5-Split-Screens.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"232837\",\"textColor3\":\"333333\",\"textColor4\":\"4f535f\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Purple112/v4/5b/a3/df/5ba3dfa3-dcfe-9da0-b5cb-fc33a6a51a66/4f7df55f-14da-470f-83ce-c167dc418484_iPad-12.9-9-Teaching-And-Presentation__U0028Chemistry_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"ffffff\",\"textColor2\":\"ecc59b\",\"textColor3\":\"d4d4d5\",\"textColor4\":\"c5a685\"},{\"width\":2048,\"height\":2732,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/PurpleSource112/v4/53/bb/44/53bb44ec-6b13-e7a3-6b84-3883c7542cb1/0923f66f-1c15-4932-86ef-db739134e121_iPad-12.9-10-Dark-Mode.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"c1fe06\",\"textColor2\":\"299bfb\",\"textColor3\":\"a2d30e\",\"textColor4\":\"2984d2\"}],\"iphone6+\":[{\"width\":1242,\"height\":2208,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/22/86/2a/22862ac8-37ac-4f1a-a425-da0477b03186/cd82e9b5-32e5-4259-b8b0-9f56891961a1_iPhone-5.5-1-Notes__U0028Medicine_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"232839\",\"textColor3\":\"333333\",\"textColor4\":\"4f5361\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple112/v4/a7/31/87/a73187d4-d3b5-3b65-b28e-282adb9d2c36/db167625-53a3-41c8-b8c1-48e4809bd755_iPhone-5.5-2-Search.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"1c1c1c\",\"textColor3\":\"333333\",\"textColor4\":\"4a4a4a\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/75/7b/07/757b077c-2989-be42-d543-fc88c5864f0f/041cd940-d7e1-49b5-a1c2-696b2790254b_iPhone-5.5-4-Flashcards__U0028US_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"002300\",\"textColor3\":\"333333\",\"textColor4\":\"334f33\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/c9/6d/a7/c96da77e-f1d8-db74-3710-78f3ae8cece5/c7cf4e46-e3b5-4a9a-b0f9-8394638dc3c0_iPhone-5.5-5-Organization.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"1c1c1c\",\"textColor3\":\"333333\",\"textColor4\":\"4a4a4a\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/5e/2b/02/5e2b0299-6371-9d6d-a245-e7a4c0dc791c/440363d4-0793-4b2e-b564-00f7aeac0a80_iPhone-5.5-11-Comments.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"232839\",\"textColor3\":\"333333\",\"textColor4\":\"4f5361\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/d9/e4/94/d9e49426-ad69-66b5-4ac3-2ebf4eb93b46/b65bf14c-6b32-479c-84f8-d5263b4a26c9_iPhone-5.5-6-Import.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"171d27\",\"textColor3\":\"333333\",\"textColor4\":\"454a52\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/67/26/59/67265999-8394-6590-1507-7328fa678fc7/374a4a9d-02e1-4abf-b9e2-27e1d8697217_iPhone-5.5-7-iCloud.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"1c1c1c\",\"textColor3\":\"333333\",\"textColor4\":\"4a4a4a\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/97/2b/33/972b3321-d652-0448-d62a-9db575dda533/d0b64345-328f-421d-8e45-92e129b877f1_iPhone-5.5-8-Presentation__U0028Chemistry_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"ffffff\",\"textColor2\":\"eaf5f8\",\"textColor3\":\"d4d4d5\",\"textColor4\":\"c4cccf\"},{\"width\":1242,\"height\":2208,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Purple122/v4/f1/08/02/f1080221-3e1a-e6c8-5717-e2bc98df1558/42404d33-1c71-469d-82eb-43adf30f1fcb_iPhone-5.5-9-Dark-Mode.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"ffffff\",\"textColor2\":\"209cfe\",\"textColor3\":\"d4d4d5\",\"textColor4\":\"2185d4\"}],\"iphone_6_5\":[{\"width\":1284,\"height\":2778,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/04/e5/ae/04e5ae6b-e38e-1481-c99c-a8b7ec0e14a6/dd9fe711-5041-4431-bc7c-4a5ab6a59913_iPhone-6.5-1-Notes__U0028Medicine_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"232839\",\"textColor3\":\"313131\",\"textColor4\":\"4d525f\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/af/c2/82/afc2825d-caf6-e8e4-d6f7-c899aa65f6d8/65af2430-3e27-44c2-b59f-4ff7d7454461_iPhone-6.5-2-Search.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"1b1b1b\",\"textColor3\":\"313131\",\"textColor4\":\"484848\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/f6/d6/56/f6d65696-4aef-4aff-d2a1-3dd5c1459da1/71865151-f85d-4ca6-9bbc-5a8b49c53c82_iPhone-6.5-4-Flashcards__U0028US_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"001f00\",\"textColor3\":\"313131\",\"textColor4\":\"314b31\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/be/ee/22/beee2284-a412-efbb-5370-a18c75b7b890/9ab7d69d-37eb-4d74-82ef-67b23d61110e_iPhone-6.5-5-Organization.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"1a1b1b\",\"textColor3\":\"313131\",\"textColor4\":\"474747\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple112/v4/53/ac/8d/53ac8dda-4390-a033-6f13-7031c51627d3/6b3a9737-dbfa-4dde-a339-66361cdef4bc_iPhone-6.5-11-Comments.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"232839\",\"textColor3\":\"313131\",\"textColor4\":\"4d525f\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/7f/b7/61/7fb761ba-a59b-c261-a9ed-bbbad503bb5d/6e192d7e-6850-49e0-ad5d-fc926563399e_iPhone-6.5-6-Import.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"161d27\",\"textColor3\":\"313131\",\"textColor4\":\"444951\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/41/55/52/4155526a-4348-d93d-3175-1eb7d932359e/b96488fd-1c25-4fd5-90f2-624199f6433f_iPhone-6.5-7-iCloud.png/{w}x{h}{c}.{f}\",\"bgColor\":\"f9f9f9\",\"textColor1\":\"000000\",\"textColor2\":\"1b1b1b\",\"textColor3\":\"313131\",\"textColor4\":\"484848\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/92/9f/27/929f27f4-2fae-8c67-938b-1a8a438d11e3/78afc62d-255b-4975-8708-c1c98e1c18a9_iPhone-6.5-8-Presentation__U0028Chemistry_U0029.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"e6e6e7\",\"textColor2\":\"f9c2a4\",\"textColor3\":\"c0c0c2\",\"textColor4\":\"cfa38c\"},{\"width\":1284,\"height\":2778,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/6c/55/e0/6c55e081-bf34-56e5-2754-f9891a5490ee/b25d24c6-2ada-4f85-a797-8da34a1d03c9_iPhone-6.5-9-Dark-Mode.png/{w}x{h}{c}.{f}\",\"bgColor\":\"29292e\",\"textColor1\":\"f2f2f3\",\"textColor2\":\"239cfe\",\"textColor3\":\"c9c9cb\",\"textColor4\":\"2485d4\"}]}}}},\"privacyPolicyUrl\":\"https://goodnotes.com/privacy-policy/\",\"supportsGameController\":false,\"releaseDate\":\"2019-01-15\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":851337026,\"supportURLForLanguage\":\"https://support.goodnotes.com/hc/en-us/categories/360000080575-GoodNotes-5\",\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Features124/v4/2a/34/5e/2a345ec2-5eb1-277f-2dd9-97b74b3fe2bc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"68abff\",\"textColor1\":\"111212\",\"textColor2\":\"121210\",\"textColor3\":\"1f2a35\",\"textColor4\":\"202a34\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 13.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Note-Taking \u0026 PDF Markup\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.goodnotesapp.x\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2011-2022 GoodNotes Limited\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"13.0\",\"hasFamilyShareableInAppPurchases\":true,\"editorialNotes\":{\"standard\":\"GoodNotes 5 is a combination digital notepad and PDF markup tool. And it’s very good at both. As a notepad, GoodNotes covers all the bases. Create notes with your keyboard or handwrite them with Apple Pencil. The app transforms your hand-drawn shapes into geometrically perfect ones. It’s also capable of recognizing your handwriting (even when you can’t) and converting it to text. \",\"short\":\"‣ For an app that lets you convert handwriting into text, try Goodnotes.\",\"tagline\":\"Collaborate on your notes\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Time Base Technology Limited\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"Dutch\",\"French\",\"German\",\"Italian\",\"Japanese\",\"Korean\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Thai\",\"Traditional Chinese\",\"Turkish\"],\"messagesScreenshots\":{},\"description\":{\"standard\":\"Take beautiful, searchable handwritten notes with the note-taking app that turns your iPad into digital paper. See why GoodNotes is Editors’ Choice. Download now and unlock on all platforms. Available on the iPad, Mac, and iPhone. \\n\\nWRITE - BETTER THAN REAL PAPER\\n• Enjoy a fluent, precise, and completely natural writing experience thanks to GoodNotes pioneering vector ink engine.\\n• Select and customize your pen color, thickness, pressure sensitivity, and style (fountain pen, ball pen, brush pen, and highlighter).\\n• Write with the Apple Pencil, Logitech Crayon, or other capacitive styluses.\\n• Convert your handwriting to text in the same page or share it with other apps.\\n\\nSEARCH AND FIND - NEVER LOSE YOUR NOTES\\n• Search your handwritten notes, typed text, PDF text, document outlines, folder titles, and document titles.\\n• Create unlimited folders and subfolders, or mark your Favorite ones to keep everything organized.\\n• Create custom outlines for easier navigation through your documents.\\n• Back up all your notes to iCloud, Google Drive, Dropbox, and OneDrive and sync across all devices so you will never lose them.\\n\\nANNOTATE - MARKUP PDFS, LECTURE SLIDES \u0026 DOCUMENTS\\n• Import PDF, PowerPoint, Word, images, photos and more.\\n• Scan any paper documents and import them to your library with built-in OCR.\\n• Annotate documents right on your iPad with customizable pens, highlighters, stickers, and shape tools.\\n• Open notes or documents side by side on iPad for easy referencing.\\n• Use tabs to quickly switch between open documents.\\n• Add hyperlinks to external websites, videos, articles to build your knowledge map.\\n\\nCREATE - UNLOCK YOUR CREATIVITY\\n• Move, resize, zoom, and rotate your handwriting or change colors.\\n• Draw perfect shapes and lines with the Shape Tool.\\n• Support gesture control for faster undo \u0026 redo.\\n• Choose to erase the entire stroke, only parts of it, or only highlighters to leave the ink intact.\\n• Select to edit or move a specific object with the Lasso Tool.\\n• Add, create, or import your stickers, pictures, tables, diagrams, and more with Elements to enrich your notes.\\n• Choose from a large set of beautiful covers and useful paper templates, including Blank Paper, Ruled Paper, Cornell Paper, Checklists, To-dos, Planners, Music Paper, Flashcards, and more.\\n• Upload any PDF or image as a custom notebook cover or paper template for more customization.\\n\\nPRESENT - SHARE \u0026 TEACH FROM YOUR IPAD\\n• Present a lecture, a lesson, a business plan, a brainstorming result, or your group study without distractions when you connect your device via AirPlay or HDMI to an external screen.\\n• Use Laser Pointer on your iPad to guide your audience’s attention during your presentation.\\n• Move around freely and interact with the audience while adding new information to the digital whiteboard.\\n\\nSHARE - COLLABORATE ON THE SAME DOCUMENTS\\n• Collaborate with others on the same document using a sharable link.\\n• Export documents in PDF or image for printing, or share them with others for a fully digital workflow.\\n• Forward important emails with PDF attachments to your unique GoodNotes email address to automatically add them to your library.\\n\\n---\\n\\nDownload GoodNotes 5 for free on iPad, Mac, and iPhone and create your first 3 notebooks for free.\\n\\nGet the full GoodNotes experience with a One-Time Unlock In-App Purchase (IAP):\\n- Create unlimited notebooks\\n- Unlock handwriting recognition to search handwritten notes\\n- Import documents via e-mail\\n- Prioritized email support\\n\\nGoodNotes supports Apple’s Universal Purchase which means you can purchase once to unlock GoodNotes on all your iOS, iPadOS, and macOS devices for free.\\n\\n---\\n\\nWebsite: www.goodnotes.com\\nTwitter: @goodnotesapp\\nInstagram: @goodnotes.app\\nPinterest: @goodnotesapp\\nTikTok: @goodnotesapp\"},\"websiteUrl\":\"https://www.goodnotes.com/\",\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1444383602\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851337026\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":288015360}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1444383602\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851337026\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":288015360}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.8,\"ratingCount\":200080,\"ratingCountList\":[2567,1396,3266,14900,177951],\"ariaLabelForRatings\":\"4.8 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"GoodNotes 5\",\"requirementsByDeviceFamily\":{\"ipod\":{\"deviceFamily\":\"iPod touch\",\"requirementString\":\"Requires iOS 13.0 or later.\"},\"iphone\":{\"deviceFamily\":\"iPhone\",\"requirementString\":\"Requires iOS 13.0 or later.\"},\"ipad\":{\"deviceFamily\":\"iPad\",\"requirementString\":\"Requires iPadOS 13.0 or later.\"},\"mac\":{\"deviceFamily\":\"Mac\",\"requirementString\":\"Requires macOS 10.15 or later.\"}},\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false,\"fileSizeByDevice\":{\"iPhone11,2\":271959040,\"iPhone13,4\":271959040,\"iPhone11,6\":271959040,\"iPhone8,4\":269272064,\"iPhone13,3\":271959040,\"iPhone8,1\":269272064,\"iPhone13,2\":271959040,\"iPhone11,4\":271959040,\"iPhone8,2\":271963136,\"iPhone13,1\":271959040,\"iPad8,12\":269288448,\"iPad8,11\":269288448,\"iPad8,10\":269288448,\"iPhone11,8\":269284352,\"iPad7,6\":269280256,\"iPad7,5\":269280256,\"iPad7,4\":269288448,\"iPad7,3\":269288448,\"iPad7,2\":269288448,\"iPad5,4\":269280256,\"iPad13,10\":269288448,\"iPad7,1\":269288448,\"iPad5,3\":269280256,\"iPad5,2\":269280256,\"iPad5,1\":269280256,\"iPad6,11\":269280256,\"iPad6,12\":269280256,\"MacFamily20,1\":269292544,\"iPad12,1\":268893184,\"iPad12,2\":268893184,\"iPad14,1\":268905472,\"iPad14,2\":268905472,\"iPhone10,3\":271959040,\"iPhone12,1\":269284352,\"iPhone10,2\":271959040,\"iPhone10,1\":269280256,\"iPhone9,4\":271959040,\"iPhone12,5\":271959040,\"iPhone14,3\":271571968,\"iPhone9,2\":271959040,\"iPhone9,3\":269280256,\"iPhone10,6\":271959040,\"iPhone14,2\":271571968,\"iPhone12,3\":271959040,\"iPhone10,5\":271959040,\"iPhone10,4\":269280256,\"iPhone9,1\":269280256,\"iPad7,12\":269280256,\"iPhone14,6\":268893184,\"iPhone12,8\":269280256,\"iPhone14,5\":271571968,\"iPad13,11\":269288448,\"iPhone14,4\":271571968,\"iPad13,17\":268905472,\"universal\":288015360,\"iPod9,1\":269272064,\"iPad13,16\":268905472,\"iPad8,5\":269288448,\"iPad6,7\":269280256,\"iPad8,4\":269288448,\"iPad8,3\":269288448,\"iPad11,1\":269288448,\"iPad6,4\":269288448,\"iPad8,2\":269288448,\"iPad6,3\":269288448,\"iPad8,1\":269288448,\"iPad8,9\":269288448,\"iPad8,8\":269288448,\"iPad8,7\":269288448,\"iPad8,6\":269288448,\"iPad6,8\":269280256,\"iPad7,11\":269280256,\"iPad13,8\":269288448,\"iPad13,9\":269288448,\"iPad13,4\":269288448,\"iPad11,6\":269280256,\"iPad13,5\":269288448,\"iPad11,7\":269280256,\"iPad13,6\":269288448,\"iPad13,7\":269288448,\"iPad11,2\":269288448,\"iPad13,1\":269288448,\"iPad11,3\":269288448,\"iPad13,2\":269288448,\"iPad11,4\":269288448}},\"relationships\":{\"app-events\":{\"href\":\"/v1/catalog/us/apps/1444383602/app-events?l=en-US\",\"data\":[]},\"reviews\":{\"href\":\"/v1/catalog/us/apps/1444383602/reviews?l=en-US\",\"next\":\"/v1/catalog/us/apps/1444383602/reviews?l=en-US\u0026offset=10\",\"data\":[{\"id\":\"8592444634\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"Honestly, I really love using this app! It is very well thought out and I can use it for many different kinds of workflows. Great app if you are considering starting/ transitioning to digital note-taking/ journaling. I enjoy taking handwritten notes in it with my apple pencil and being able to easily import pdfs that I can mark up is great. I enjoy the elements feature, which allows me to add custom sticker that I use all the time as post-its or stickers for journaling. Plus there are som many other excellent features, such as being able to search in your hand written notes and the ability to back-up your notebooks, to name a few.\\n\\nI only have one pet peeve when it comes to this app.  It does have a typing feature, but it’s a bit clunky and not very intuitive. When I am doing journaling, it is very nice to put text exactly where I want it, so I don’t want the feature to go away. But having to use the text boxes for more type-intensive tasks can be a bit annoying at times. I would be nice if there was a feature you could enable within the notebook where if you could type in a notebook like a normal word document. My work around is typing something ahead of time in a word document and then importing it into the app, but honestly, it would be nice if I didn’t have to do that (maybe I am being too picky)… Otherwise, this a really excellent, solid app that I recommend highly to folks for note-taking.\",\"title\":\"This is an excellent app! I only have one pet peeve…\",\"userName\":\"Jewelsummoner\",\"date\":\"2022-04-22T00:05:59Z\",\"isEdited\":false}},{\"id\":\"8370444019\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"I absolutely love this app. I use this more for pdf creation and personal journaling than anything else. If I were to add or change anything, it would be to include a colored pencil or crayon  writing utensil among the hi-liter, and various pen-pen, brush-pen, etc. I may be mistaken, but doesn’t the primary photo used to advertise GoodNotes is, in fact, written and drawn with what looks to be a colored pencil. How odd that is not a function. \\n\\nAlso, voice recorder that syncs with ur written notes, as similarly done by “Colla-note” and “notability” would be a highly desired note taking tool. Other features that would rock my world would to insert in-notebook hyperlinks so that, say for example, I nay make a table of contents at the start if my 300 page notebook. It would be fabulous if I could make some text or a insert picture that would hyperlink me to page 288 where I want to read my notes on socio-economic hierarchies. Something like Collanote did. And also the ability to embed videos or audio in a note would really be beneficial.\\n\\nDespite that slight suggestion party I just threw out there, GoodNotes, is the note taking app I continue to consistently return to. No other note app seems to so effortlessly and perfectly change, adjust, create, or mark up pdfs as well as GoodNotes. No other note app has such an amazing array of paper to write on. Thank you GoodNotes. You are some damned Good notes! You are my most used app among all my devices.\",\"title\":\"Amazing\",\"userName\":\"Swipe69\",\"date\":\"2022-02-18T20:52:19Z\",\"isEdited\":false}},{\"id\":\"6546770352\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"This is the best notetaking app ever. I'm an AP and Honors student in highschool, and I live for this app. Since doing online school this year due to the pandemic, it has changed how I take notes and turned in assignment. I love how GoodNotes is uploaded to the cloud so you can look at your notes no matter where you are or what device you are on. I like typing my science notes on my computer, and I love that I can switch over to my IPad to draw graphs and symbols whenever needed. I love using my Apple pencil to take all my other notes, its perfect for hand writing notes. I love all the different paper types and the covers you can choose from. You can also organize all of your documents into folders. My favorite thing BY FAR is that you can download pretty much ANY pdf or worksheet into GoodNotes and write directly on the document. Especially because of online school, a lot of my teachers post their worksheets on GoogleClassroom, and being able to download the documents and write directly onto it, and then I can just drag and drop the PDF from my desktop into Classroom ITS AMAZING! It saves me so much paper, and has helped me stay so organized with everything in one place. The format and interfacing is incredible, you HAVE to try this note taking app, it's incredible. I would recommend this to ANYONE and EVERYONE. Please get it for yoruself and check it out!\",\"title\":\"I LOVE this app, best notetaking app out there!\",\"userName\":\"emmasromo\",\"date\":\"2020-10-18T03:29:26Z\",\"isEdited\":false}},{\"id\":\"6570278420\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":4,\"review\":\"I was looking for an app where I can annotate pdfs, and switch between the computer and an iPad while doing so. I love the capabilities, and the layout. I like that you can choose covers for notebooks, and that there are even so many different types of \\\"papers\\\" to choose from, from dotted, lined to even Cornell notes. And that you can add pages to pdfs, or can easily take photos and then export them as pdfs. Furthermore, whenever I edit a pdf on the iPad, it would automatically sync to the app on the Mac.\\n\\nMy main problem is with the glitches on the Mac app. Often when selecting the highlighting tool, the highlighting has to be done manually, which is difficult to do on the computer without touch screen. You can also select text and then click highlight, but I would like that capability when I am using the highlighter specifically. The option to select the text would also often not work after a while, and I would have to quit Goodnotes and open it back up again. And finally, there is also no convenient way to underline, which currently would require manually drawing lines under text. The layout of the files is also sometimes not as intuitive for me, and I would like to have the option of seeing my file folders on the side bar as well.\\n\\nBut other than that, I am enjoying using Goodnotes so far, and feel that the pros definitely outweigh the cons (which I hope will be improved soon with updates!)\",\"title\":\"For the most part pretty great\",\"userName\":\"Wobblehobbit\",\"date\":\"2020-10-24T21:05:05Z\",\"isEdited\":false}},{\"id\":\"7639555338\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"I love this app. Previously, I would print every single paper I read because none of the electronic tools I would use allowed me to mark it up the way I like to. GoodNotes essentially gives me all of the tools on my desk - pen, highlighter, etc. and allows me to read and manipulate a document as though it’s really in my hand. There is really no difference, and it’s actually even better because the highlighter can automatically snap to lines of text! I only have two wishes that would make this program better than it already is - which is near perfect: I’d like to be able to store more pen or highlighter colors on the top row of tools. I use lots of different colors for different notes, and it seems like there’s plenty of room (at least on the 12.9 inch iPad) for a few more options to allow for quicker switching. Second, it would be nice to have page hyperlink support. When you’re reading a 300 page manual, it’s a little annoying to swipe back and forth between pages, especially when the table of contents is right there. I know about the goto page feature, but that doesn’t always line up with the page numbers correctly, and it isn’t easy to determine the right page looking through thumbnails. Otherwise, this app is amazing!\",\"title\":\"Fantastic notetaking app that showed me reading and writing on screens was feasible\",\"userName\":\"Killer Fencer\",\"date\":\"2021-07-31T15:33:51Z\",\"isEdited\":false}},{\"id\":\"8348945152\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":4,\"review\":\"After using OneNote for a while and finding it doesn’t have all the features I want like importing PDFs and PowerPoint slides or a really good writing experience, and then trying Notability but not wanting to use a subscription based service, I decided to try GoodNotes. First impressions, very clean app with good organization! Love how the notebooks are more like real notebooks with page separation (you can choose not to separate though) and different page colors and styles as well as some really pretty covers. Also love the writing experience, like all the pen colors and sizes are nice and everything feels smooth and adding pictures are super easy. I’m really glad there’s a search function as well to easily find stuff in my notes.\\n\\nThough, I do have some problems with this app. I wish that I could put more than 3 pen colors at the top, I feel like a few more slots would be great. I would also love if the pen size could stay on each slot if that makes sense, kinda like OneNote, rather than me having the change the pen size every time I switch colors. I think it would be cool if like Notability there could be a voice recording feature, and paper styles with texture like real paper added. This is a smaller issue but it would be cool if some different folder colors were added as well for even better organization :) \\n\\nOverall really like this app but there are some improvements that could be made!\",\"title\":\"Almost perfect!\",\"developerResponse\":{\"id\":28074432,\"body\":\"Thanks for a balanced review! We can see how it would be useful to support all those things like more color slots, favorite pens, different folder colors, etc. and we've noted your request. Please upvote similar ideas on feedback.goodnotes.com. \",\"modified\":\"2022-02-16T03:22:05Z\"},\"userName\":\"stan skz uwu\",\"date\":\"2022-02-12T23:12:19Z\",\"isEdited\":false}},{\"id\":\"6457305063\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"I'd been searching for a replacement for a notebook for a long time before I found GoodNotes, paying for several apps that didn't deliver or have the intuitive controls of a pen and paper. GoodNotes delivers. I use it for EVERYTHING for work and my home life. It's my personal budget planner, my personal calendar, my lesson plan book, my gradebook, my PDF annotater...everything! It's replaced basically all paper in my life. As a teacher, I could not be more thrilled with this app's functionality. The ability to create different notebooks with specific templates (and upload your own) allows it to function as so many different things! As a teacher, I'm a big binder person so I can keep related notebooks and files near each other. With the ability to open new notebooks in tabs within the app, I can keep these files close at hand and jump between them--it's kind of like a big digital binder that stays organized and doesn't get overwhelming with the number of files. The organization of the app and the way you can navigate through it by flipping the pages like you would a notebook makes it my favorite app of all time. AND now that it has apps available for iPad, Mac, and iPhone that all sync up beautifully, I can access everything everywhere! Well done, GoodNotes; thank you!\",\"title\":\"My Favorite App: Best Replacement for a Notebook\",\"userName\":\"saminer\",\"date\":\"2020-09-22T19:22:08Z\",\"isEdited\":false}},{\"id\":\"3854685036\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"I am writing this review to help the developers and potential buyers. First I’m providing this review in March 2019. Things may change later so if you are reading this review you should keep the date and version number in mind. 5.0.20. \\nWe are a large builder which use Blue beam, Procore, “go to meeting” and other related industry software. We found that Good notes 4 was simply the best vehicle for note taking and plan markups available. It truly was a “five star app”. We could be on a “go to meeting” with 15 consultants on a conference call and annotate plans instantly. (What I mean is hand write changes on the pdf plans to give direction. Not edit DWG drawings.) It was fast easy and simple. Superior to other products out there, even the industry specific ones.\\nNow, with Good notes 5 the layout and folder structure have been improved. But, there is a big problem. If we open a set of plans and try to zoom in, the plans pixelate. Plus take an in ordinate amount of time to gain focus. Thus, most of us have stopped using this program. I have Good notes 4 and have gone back to it. I. Don’t know. If the developer is aware or working on a fix but it was a great program. If the developer is aware hope you can fix. I will check the App Store for updates.  Will mark as 2.5 stars and revise rating if able to remedy problem. Good luck.\",\"title\":\"Good notes 4 performs better than Good notes 5\",\"userName\":\"Firstviceroy\",\"date\":\"2019-03-08T00:01:05Z\",\"isEdited\":false}},{\"id\":\"6941659313\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"I’m doing online teaching and would love a notebook app that can enable real time, collaborative editing for lecturing. GoodNotes does provide a feature that may work well. My setup is: I am doing white boarding on my iPad while sharing the screen of my laptop using Zoom. I open a GoodNotes session on the laptop and project the app to Zoom’s shared screen. While the fast collaborative ode works well for newly created document, it becomes really lagged when 1) the document has been opened for a while (20 min to 1 hour), and/or 2) I re-open an existing document that I created a while ago (with fast online editing mode enabled). The latency is too long—ranging from 10 seconds to sub minute— to be acceptable for real time editing. I have to switch to a different app in order for my lecture to proceed as expected.  I love the user experience of GoodNotes’ other features;  but talking about the collaborative editing feature, it really pisses the users off. I’d really demand the team to fix the lagging issue of this feature. I’m not sure if this is due to the backend design options like periodically moving a doc to a slower tier that syncs using the slow iCloud storage or not. But this issue does persists for at least few weeks. Thanks!\",\"title\":\"Would give a 5 star if the collaborative editing mode is stable and truly real time\",\"userName\":\"kc4007\",\"date\":\"2021-02-01T19:54:05Z\",\"isEdited\":false}},{\"id\":\"7282422088\",\"type\":\"user-reviews\",\"attributes\":{\"rating\":5,\"review\":\"For me it's all the little UI decisions that make things wonderful in this app. I like as minimal clutter as possible, hide the UI whenever possible, make it as much like a pad of paper I'm looking at. It feels like a pad of paper because I can swipe between pages. I CANNOT OVERSTATE how much this helps me to not have scrolling pages but instead a finite canvas size that the visual part of my brain can remember. For some reason scrolling I can't remember the shape and thus the location and then from that the information that's at each location. Same thing for swiping between pages to change them, it's probably just me but not being able to do this in the notes app or any other app drives me insane. I don't want to be looking at UI / the titles of other pages. I want a page that is adjacent / above / below my page and nothing else. I don't need any other information in front of me. I love that the Mac application bar at the top is basically hidden and it's only the stoplight in the top left. The fact that I can then move the UI to the bottom of the page so that it's out of the way make it look like I have my paper right there next to my main screen. THANK YOU for making these UI decisions that others may not appreciate but I certainly do :)\",\"title\":\"Closest to pen and paper I've found\",\"userName\":\"Cautious Poke\",\"date\":\"2021-04-30T21:41:23Z\",\"isEdited\":false}}]},\"top-in-apps\":{\"href\":\"/v1/catalog/us/apps/1444383602/top-in-apps?l=en-US\",\"data\":[{\"id\":\"1612402652\",\"type\":\"in-apps\",\"href\":\"/v1/catalog/us/in-apps/1612402652?l=en-US\",\"attributes\":{\"url\":\"https://apps.apple.com/us/app/full-version-unlock/id1444383602\",\"icuLocale\":\"en_US@currency=USD\",\"isMerchandisedEnabled\":false,\"isFamilyShareable\":true,\"offerName\":\"com.goodnotes.one_time_unlock\",\"releaseDate\":\"2009-06-17\",\"name\":\"Full Version Unlock\",\"isMerchandisedVisibleByDefault\":false,\"isSubscription\":false,\"description\":{\"standard\":\"Create unlimited notebooks with all features\"},\"offers\":[{\"buyParams\":\"productType=A\u0026price=7990\u0026salableAdamId=1612402652\u0026pricingParameters=STDQ\u0026pg=default\",\"type\":\"buy\",\"priceFormatted\":\"$7.99\",\"price\":7.99,\"currencyCode\":\"USD\",\"assets\":[]}]}}]},\"developer-other-apps\":{\"href\":\"/v1/catalog/us/apps/1444383602/developer-other-apps?l=en-US\",\"data\":[]},\"related-editorial-items\":{\"href\":\"/v1/catalog/us/apps/1444383602/related-editorial-items?l=en-US\",\"data\":[{\"id\":\"1447882511\",\"type\":\"editorial-items\",\"href\":\"/v1/editorial/us/editorial-items/1447882511?l=en-US\",\"attributes\":{\"url\":\"https://apps.apple.com/us/story/id1447882511\",\"label\":\"APP\\nOF THE\\nDAY\",\"alternateLabel\":\"FEATURED APP\",\"isCanvasAvailable\":true,\"supportsArcade\":false,\"editorialPlatforms\":[\"iphone\",\"ipad\"],\"editorialArtwork\":{\"dayCard\":{\"width\":3524,\"height\":2160,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features122/v4/54/7e/c3/547ec34a-78c7-1bdf-c5d6-0b771d9173b2/source/{w}x{h}{c}.{f}\",\"bgColor\":\"c1f0fe\",\"textColor1\":\"080c0f\",\"textColor2\":\"0a0d0e\",\"textColor3\":\"232d32\",\"textColor4\":\"252e31\"},\"mediaCard\":{\"width\":3524,\"height\":2160,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/f6/cc/2c/f6cc2cb2-18fe-1dde-bfc8-f2be9dce06d4/source/{w}x{h}{c}.{f}\",\"bgColor\":\"eeede8\",\"textColor1\":\"0e0d0c\",\"textColor2\":\"160e02\",\"textColor3\":\"3b3a38\",\"textColor4\":\"413b30\"}},\"kind\":\"App\",\"displayStyle\":\"Branded\",\"ignoreITunesShortNotes\":false,\"editorialNotes\":{\"name\":\"GoodNotes 5 Does It All\",\"short\":\"Take smart notes and mark up PDFs in the same place.\",\"tagline\":\"\",\"long\":\"\"},\"editorialLocale\":\"en-US\",\"cardDisplayStyle\":\"AppOfTheDay\",\"ignoreEditorialArt\":false,\"displaySubStyle\":\"AppOfDay\"},\"meta\":{\"robots\":{\"restrictSearch\":false}}},{\"id\":\"1449680534\",\"type\":\"editorial-items\",\"href\":\"/v1/editorial/us/editorial-items/1449680534?l=en-US\",\"attributes\":{\"url\":\"https://apps.apple.com/us/story/id1449680534\",\"label\":\"GET ORGANIZED\",\"isCanvasAvailable\":true,\"supportsArcade\":false,\"editorialPlatforms\":[\"iphone\",\"ipad\"],\"editorialArtwork\":{\"dayCard\":{\"width\":3524,\"height\":2160,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features114/v4/70/a5/e4/70a5e4b5-75cc-c299-d5ea-3a7a36d592c6/source/{w}x{h}{c}.{f}\",\"bgColor\":\"eeede8\",\"textColor1\":\"0e0d0c\",\"textColor2\":\"160e02\",\"textColor3\":\"3b3a38\",\"textColor4\":\"413b30\"},\"generalCard\":{\"width\":3524,\"height\":2160,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Features116/v4/b9/54/14/b95414f7-133d-87b9-783c-3641fb726dbd/source/{w}x{h}{c}.{f}\",\"bgColor\":\"fbfaf0\",\"textColor1\":\"0c0e11\",\"textColor2\":\"0c1012\",\"textColor3\":\"2e3132\",\"textColor4\":\"2f3333\"},\"mediaCard\":{\"width\":3524,\"height\":2160,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features123/v4/75/d8/eb/75d8eb99-0bb0-90e6-957b-5aff54ccc2a4/source/{w}x{h}{c}.{f}\",\"bgColor\":\"fffffc\",\"textColor1\":\"050c12\",\"textColor2\":\"002c58\",\"textColor3\":\"373c41\",\"textColor4\":\"335679\"}},\"kind\":\"App\",\"displayStyle\":\"Content\",\"ignoreITunesShortNotes\":true,\"editorialNotes\":{\"name\":\"Your Ultimate Digital Notebook\",\"short\":\"\",\"tagline\":\"\",\"long\":\"\"},\"editorialLocale\":\"en-US\",\"cardDisplayStyle\":\"ShortImage\",\"ignoreEditorialArt\":false},\"meta\":{\"robots\":{\"restrictSearch\":true}}}]},\"customers-also-bought-apps\":{\"href\":\"/v1/catalog/us/apps/1444383602/customers-also-bought-apps?l=en-US\",\"next\":\"/v1/catalog/us/apps/1444383602/customers-also-bought-apps?l=en-US\u0026offset=10\",\"data\":[{\"id\":\"1540956268\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1540956268?l=en-US\"},{\"id\":\"360593530\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/360593530?l=en-US\"},{\"id\":\"1423643723\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1423643723?l=en-US\"},{\"id\":\"1119601770\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1119601770?l=en-US\"},{\"id\":\"1505638148\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1505638148?l=en-US\"},{\"id\":\"922765270\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/922765270?l=en-US\"},{\"id\":\"1021085778\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1021085778?l=en-US\"},{\"id\":\"1473064295\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1473064295?l=en-US\"},{\"id\":\"1232780281\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1232780281?l=en-US\"},{\"id\":\"920402079\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/920402079?l=en-US\"}],\"meta\":{\"metrics\":{\"reco_hash\":\"cab_1\",\"reco_eligible\":\"1\",\"reco_algoId\":\"cab_1\"}}},\"developer\":{\"href\":\"/v1/catalog/us/apps/1444383602/developer?l=en-US\",\"data\":[{\"id\":\"424587624\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/424587624?l=en-US\",\"attributes\":{\"name\":\"Time Base Technology Limited\",\"mediaType\":\"Mobile Software Applications\",\"editorialArtwork\":{},\"genreNames\":[],\"url\":\"https://apps.apple.com/us/developer/time-base-technology-limited/id424587624\"}}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1444383602/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"name\":\"Productivity\",\"parentName\":\"App Store\",\"url\":\"https://itunes.apple.com/us/genre/id6007\",\"parentId\":\"36\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"name\":\"Education\",\"parentName\":\"App Store\",\"url\":\"https://itunes.apple.com/us/genre/id6017\",\"parentId\":\"36\"}}]},\"merchandised-in-apps\":{\"href\":\"/v1/catalog/us/apps/1444383602/merchandised-in-apps?l=en-US\",\"data\":[]}}}]}",".v1.catalog.us.apps.1444383602.l.en-us.platform.mac.fields.5bapps.5d.userrating.omit.5bresource.5d.autos":"{\"x\":1660230713677,\"d\":[{\"id\":\"1444383602\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1444383602?l=en-US\",\"attributes\":{\"userRating\":{\"value\":4.8,\"ratingCount\":60632,\"ratingCountList\":[655,292,899,4682,54104],\"ariaLabelForRatings\":\"4.8 stars\"}}}]}",".v1.catalog.us.apps.l.en-us.ids.1021085778.2c1119601770.2c1232780281.2c1423643723.2c1473064295.2c1505638148.2c1540956268.2c360593530.2c920402079.2c922765270.platform.web.additionalplatforms.appletv.2cipad.2ciphone.2cmac.sparselimit.10":"{\"x\":1660230713721,\"d\":[{\"id\":\"1021085778\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1021085778?l=en-US\",\"attributes\":{\"artistName\":\"Alessandro Sassi\",\"firstVersionSupportingInAppPurchaseApi\":\"4.0\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/quicknotes-x/id1021085778\",\"isIOSBinaryMacOSCompatible\":false,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/a9/cf/ff/a9cfffdf-0090-9746-e90a-525b545719ca/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"180705\",\"textColor2\":\"272524\",\"textColor3\":\"463837\",\"textColor4\":\"52514f\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":true,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"Italian\",\"tag\":\"it\"}],\"supportsGameController\":false,\"releaseDate\":\"2015-07-25\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":848078667,\"editorialArtwork\":{\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 11.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Note-Taking, made easy.\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.asassi.PenPro\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2015-2022 Alessandro Sassi\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"11.0\",\"hasFamilyShareableInAppPurchases\":false,\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Alessandro Sassi\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"Italian\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1021085778\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=848078667\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":40058880}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1021085778\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=848078667\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":40058880}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.3,\"ratingCount\":2387,\"ratingCountList\":[209,90,164,307,1617],\"ariaLabelForRatings\":\"4.3 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"QuickNotes X\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1021085778/developer?l=en-US\",\"data\":[{\"id\":\"1493314059\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/1493314059?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1021085778/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}},{\"id\":\"1119601770\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1119601770?l=en-US\",\"attributes\":{\"artistName\":\"MyScript\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/nebo-notes-pdf-annotations/id1119601770\",\"isIOSBinaryMacOSCompatible\":true,\"reviewsRestricted\":false,\"deviceFamilies\":[\"ipad\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/83/9d/2f/839d2f58-5dce-035d-6fba-6547518a6f62/AppIcon-0-1x_U007emarketing-0-7-0-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000d16\",\"textColor2\":\"021016\",\"textColor3\":\"333e45\",\"textColor4\":\"343f45\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"}],\"supportsGameController\":false,\"releaseDate\":\"2016-08-10\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":850248364,\"editorialArtwork\":{\"storeFlowcase\":{\"width\":4320,\"height\":1080,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Features62/v4/ac/91/24/ac912420-139c-a825-4a9b-a42e2c62e22e/source/{w}x{h}{c}.{f}\",\"bgColor\":\"009fe3\",\"textColor1\":\"001316\",\"textColor2\":\"161616\",\"textColor3\":\"002f3f\",\"textColor4\":\"12323f\"},\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features118/v4/2c/08/be/2c08be02-a15a-1370-8937-9813f6024503/source/{w}x{h}{c}.{f}\",\"bgColor\":\"009fe3\",\"textColor1\":\"111416\",\"textColor2\":\"161616\",\"textColor3\":\"0d303f\",\"textColor4\":\"12323f\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 13.0 or later. Compatible with iPad.\",\"subtitle\":\"Notebook, Handwriting to Text\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.myscript.nebo\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2022 MyScript. All Rights Reserved\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"13.0\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"short\":\"Take beautiful handwritten notes.\",\"tagline\":\"Handwrite with Apple Pencil\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"MyScript\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"German\",\"Italian\",\"Japanese\",\"Korean\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Traditional Chinese\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1119601770\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=850248364\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":162750464}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1119601770\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=850248364\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":162750464}]}]}},\"hasEula\":true,\"userRating\":{\"value\":4.6,\"ratingCount\":14128,\"ratingCountList\":[476,261,618,1987,10786],\"ariaLabelForRatings\":\"4.6 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Nebo: Notes \u0026 PDF Annotations\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1119601770/developer?l=en-US\",\"data\":[{\"id\":\"446368119\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/446368119?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1119601770/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}},{\"id\":\"1232780281\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1232780281?l=en-US\",\"attributes\":{\"artistName\":\"Notion Labs, Incorporated\",\"firstVersionSupportingInAppPurchaseApi\":\"1.7.6\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/notion-notes-docs-tasks/id1232780281\",\"isIOSBinaryMacOSCompatible\":false,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"chartPositions\":{\"appStore\":{\"position\":30,\"genreName\":\"Productivity\",\"genre\":6007,\"chart\":\"top-free\",\"chartLink\":\"https://apps.apple.com/us/charts/iphone/productivity-apps/6007\"}},\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple112/v4/aa/58/e9/aa58e994-d080-7d50-5bbe-b95c83cf9337/AppIconProd-0-1x_U007emarketing-0-10-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000000\",\"textColor2\":\"2c2c2c\",\"textColor3\":\"333333\",\"textColor4\":\"565656\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":true,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"}],\"supportsGameController\":false,\"releaseDate\":\"2017-09-14\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":851216218,\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Features125/v4/f4/55/35/f45535df-b01a-29b4-5d15-3f13cdde54df/source/{w}x{h}{c}.{f}\",\"bgColor\":\"f2f2f2\",\"textColor1\":\"0a0a0a\",\"textColor2\":\"0e0e0f\",\"textColor3\":\"2c2c2c\",\"textColor4\":\"303030\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"The all-in-one workspace\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"notion.id\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"©2022 Notion Labs, Incorporated\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"standard\":\"Is your mind whirring away with all the things you need to do at work, home, and everywhere in between? Give your brain a break by logging your thoughts in Notion. Notion is a digital notepad in which you can jot down all sorts of information. Create private pages for lists, random thoughts and diary entries, or shared ones for documents you’d like to invite friends and colleagues to so you can collaborate.\",\"tagline\":\"A new way to work\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Notion Labs, Incorporated\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"Japanese\",\"Korean\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1232780281\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851216218\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":72037376}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1232780281\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851216218\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":72037376}]}]}},\"hasEula\":true,\"userRating\":{\"value\":4,\"ratingCount\":2513,\"ratingCountList\":[329,136,204,346,1498],\"ariaLabelForRatings\":\"4.0 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Notion - notes, docs, tasks\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1232780281/developer?l=en-US\",\"data\":[{\"id\":\"1232780280\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/1232780280?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1232780281/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6000\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6000?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Business\",\"url\":\"https://itunes.apple.com/us/genre/id6000\"}}]}}},{\"id\":\"1423643723\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1423643723?l=en-US\",\"attributes\":{\"artistName\":\"Kairoos Solutions SL\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/notes-writer-take-good-notes/id1423643723\",\"isIOSBinaryMacOSCompatible\":true,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/dc/5e/51/dc5e515b-7ea3-9f59-ab70-d889048b0cf4/AppIcon-1x_U007emarketing-0-7-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"060606\",\"textColor2\":\"1d1d1d\",\"textColor3\":\"383838\",\"textColor4\":\"4a4a4a\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"}],\"supportsGameController\":false,\"releaseDate\":\"2018-08-15\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":851426353,\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Features122/v4/d8/a9/ae/d8a9ae02-f579-a26d-322f-c511afd427b2/source/{w}x{h}{c}.{f}\",\"bgColor\":\"1d4975\",\"textColor1\":\"b8e1f1\",\"textColor2\":\"a0def7\",\"textColor3\":\"94c0d6\",\"textColor4\":\"82bddb\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features112/v4/81/7b/b3/817bb32a-f54d-911d-4cfa-f07f67ec36eb/source/{w}x{h}sr.{f}\",\"bgColor\":\"1d4a76\",\"textColor1\":\"ecf1f5\",\"textColor2\":\"d2e9fa\",\"textColor3\":\"bbcdda\",\"textColor4\":\"a8c6de\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Note-taking, PDF \u0026 Notebooks\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.kairoos.noteswriterfree\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2022 Kairoos Solutions\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":true,\"editorialNotes\":{\"tagline\":\"For all your note-writing needs\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Kairoos Solutions S.L.\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"German\",\"Italian\",\"Japanese\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1423643723\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851426353\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":275936256}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1423643723\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851426353\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":275936256}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.5,\"ratingCount\":6469,\"ratingCountList\":[365,116,336,746,4906],\"ariaLabelForRatings\":\"4.5 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Notes Writer -Take Good Notes!\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1423643723/developer?l=en-US\",\"data\":[{\"id\":\"978066434\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/978066434?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1423643723/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}},{\"id\":\"1473064295\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1473064295?l=en-US\",\"attributes\":{\"artistName\":\"User Camp\",\"firstVersionSupportingInAppPurchaseApi\":\"1.0\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/penbook/id1473064295\",\"isIOSBinaryMacOSCompatible\":true,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/2d/15/99/2d159943-0d5c-d220-e788-cb4f9f74d370/AppIcon-0-1x_U007emarketing-0-0-0-7-0-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ccd4d7\",\"textColor1\":\"1f1f1f\",\"textColor2\":\"163457\",\"textColor3\":\"414343\",\"textColor4\":\"3a5470\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"}],\"supportsGameController\":false,\"releaseDate\":\"2019-09-24\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":850875980,\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features113/v4/32/30/f3/3230f324-0d7b-6350-52b1-d200a7be4459/source/{w}x{h}{c}.{f}\",\"bgColor\":\"f5f5f5\",\"textColor1\":\"110d0f\",\"textColor2\":\"242627\",\"textColor3\":\"3e3b3d\",\"textColor4\":\"4e4f50\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Note-taking \u0026 handwriting\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"camp.user.penbook\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© User Camp Inc.\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":true,\"editorialNotes\":{\"tagline\":\"Build your perfect notebook\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"User Camp Inc.\",\"isSiriSupported\":false,\"languageList\":[\"English\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1473064295\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=850875980\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":67237888}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1473064295\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=850875980\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":67237888}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.6,\"ratingCount\":11041,\"ratingCountList\":[363,188,484,1143,8863],\"ariaLabelForRatings\":\"4.6 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Penbook\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1473064295/developer?l=en-US\",\"data\":[{\"id\":\"1473063859\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/1473063859?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1473064295/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6002\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6002?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Utilities\",\"url\":\"https://itunes.apple.com/us/genre/id6002\"}}]}}},{\"id\":\"1505638148\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1505638148?l=en-US\",\"attributes\":{\"artistName\":\"Wasdesign, LLC\",\"firstVersionSupportingInAppPurchaseApi\":\"1.0.0\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/pencil-planner-calendar-pro/id1505638148\",\"isIOSBinaryMacOSCompatible\":true,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple116/v4/ad/56/5c/ad565c90-4ffb-936a-4eab-b7b8026362bb/AppIcon-1x_U007emarketing-0-10-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"6780ff\",\"textColor1\":\"040209\",\"textColor2\":\"06030b\",\"textColor3\":\"181b3a\",\"textColor4\":\"191c3c\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Hindi\",\"tag\":\"hi\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Portuguese\",\"tag\":\"pt-BR\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Thai\",\"tag\":\"th\"}],\"supportsGameController\":false,\"releaseDate\":\"2020-05-31\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":847787197,\"editorialArtwork\":{\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Daily digital passion agenda\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.wasdesign.ppp\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2021 Wasdesign, LLC\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":true,\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Wasdesign, LLC\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"German\",\"Hindi\",\"Italian\",\"Japanese\",\"Korean\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Thai\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1505638148\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=847787197\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":42541056}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1505638148\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=847787197\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":42541056}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.7,\"ratingCount\":7079,\"ratingCountList\":[214,58,159,883,5765],\"ariaLabelForRatings\":\"4.7 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Pencil Planner \u0026 Calendar Pro\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1505638148/developer?l=en-US\",\"data\":[{\"id\":\"721094701\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/721094701?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1505638148/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6012\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6012?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Lifestyle\",\"url\":\"https://itunes.apple.com/us/genre/id6012\"}}]}}},{\"id\":\"1540956268\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/1540956268?l=en-US\",\"attributes\":{\"artistName\":\"Quoc Huy Nguyen\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/collanote-note-journal-pdf/id1540956268\",\"isIOSBinaryMacOSCompatible\":true,\"reviewsRestricted\":false,\"deviceFamilies\":[\"iphone\",\"ipad\",\"ipod\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/30/91/bc/3091bc67-6538-85ea-939a-c4234b281f40/AppIcon-0-1x_U007emarketing-0-10-0-sRGB-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"27cdfe\",\"textColor1\":\"07090f\",\"textColor2\":\"040a10\",\"textColor3\":\"0d303f\",\"textColor4\":\"0b313f\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"Bengali\",\"tag\":\"bn\"},{\"name\":\"Catalan\",\"tag\":\"ca\"},{\"name\":\"Croatian\",\"tag\":\"hr\"},{\"name\":\"Czech\",\"tag\":\"cs\"},{\"name\":\"Danish\",\"tag\":\"da\"},{\"name\":\"Dutch\",\"tag\":\"nl\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Greek\",\"tag\":\"el\"},{\"name\":\"Hungarian\",\"tag\":\"hu\"},{\"name\":\"Indonesian\",\"tag\":\"id\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Malay\",\"tag\":\"ms\"},{\"name\":\"Norwegian Bokmål\",\"tag\":\"nb\"},{\"name\":\"Polish\",\"tag\":\"pl\"},{\"name\":\"Portuguese\",\"tag\":\"pt-BR\"},{\"name\":\"Romanian\",\"tag\":\"ro\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Thai\",\"tag\":\"th\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"},{\"name\":\"Ukrainian\",\"tag\":\"uk\"},{\"name\":\"Vietnamese\",\"tag\":\"vi\"}],\"supportsGameController\":false,\"releaseDate\":\"2021-01-07\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":851205086,\"editorialArtwork\":{\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/fb/01/de/fb01dea1-caa0-f7f2-061e-bef1c794ff7c/source/{w}x{h}{c}.{f}\",\"bgColor\":\"b7dedb\",\"textColor1\":\"0a1922\",\"textColor2\":\"2c202c\",\"textColor3\":\"243840\",\"textColor4\":\"443f49\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features116/v4/e4/65/d9/e465d917-d0e3-2d15-24c2-47330b4f3b8a/source/{w}x{h}sr.{f}\",\"bgColor\":\"1c1c1d\",\"textColor1\":\"62abed\",\"textColor2\":\"6890f2\",\"textColor3\":\"538abc\",\"textColor4\":\"5776c0\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Take Good Notes \u0026 Planner\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"net.quochuywdm.collanote496\",\"hasInAppPurchases\":false,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© Quoc Huy Nguyen\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"short\":\"Keep a handwritten journal and collaborate with friends.\",\"tagline\":\"Collaborative note-taking\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Quoc Huy Nguyen\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"Bengali\",\"Catalan\",\"Croatian\",\"Czech\",\"Danish\",\"Dutch\",\"French\",\"German\",\"Greek\",\"Hungarian\",\"Indonesian\",\"Italian\",\"Japanese\",\"Korean\",\"Malay\",\"Norwegian Bokmål\",\"Polish\",\"Portuguese\",\"Romanian\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Thai\",\"Traditional Chinese\",\"Turkish\",\"Ukrainian\",\"Vietnamese\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1540956268\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851205086\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":217896960}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=1540956268\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851205086\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":217896960}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.9,\"ratingCount\":8129,\"ratingCountList\":[51,26,63,359,7630],\"ariaLabelForRatings\":\"4.9 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"CollaNote: Note, Journal \u0026 PDF\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/1540956268/developer?l=en-US\",\"data\":[{\"id\":\"1540956270\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/1540956270?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/1540956268/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}},{\"id\":\"360593530\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/360593530?l=en-US\",\"attributes\":{\"artistName\":\"Ginger Labs\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/notability/id360593530\",\"isIOSBinaryMacOSCompatible\":false,\"reviewsRestricted\":false,\"deviceFamilies\":[\"mac\",\"iphone\",\"ipad\",\"ipod\"],\"chartPositions\":{\"appStore\":{\"position\":147,\"genreName\":\"Productivity\",\"genre\":6007,\"chart\":\"top-free\",\"chartLink\":\"https://apps.apple.com/us/charts/iphone/productivity-apps/6007\"}},\"isPreorder\":false,\"platformAttributes\":{\"osx\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple112/v4/2a/ef/b9/2aefb94f-9fc3-c43f-44a5-6e3081146d71/AppIcon-1x_U007euniversal-5-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"4384ff\",\"textColor2\":\"2e78fb\",\"textColor3\":\"3669cb\",\"textColor4\":\"2560c9\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"Danish\",\"tag\":\"da\"},{\"name\":\"Dutch\",\"tag\":\"nl\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Indonesian\",\"tag\":\"id\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Malay\",\"tag\":\"ms\"},{\"name\":\"Norwegian Bokmål\",\"tag\":\"nb\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Swedish\",\"tag\":\"sv\"},{\"name\":\"Thai\",\"tag\":\"th\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"},{\"name\":\"Ukrainian\",\"tag\":\"uk\"},{\"name\":\"Vietnamese\",\"tag\":\"vi\"}],\"supportsGameController\":false,\"releaseDate\":\"2010-04-01\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":851377375,\"editorialArtwork\":{\"originalFlowcaseBrick\":{\"width\":3200,\"height\":600,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features18/v4/86/e7/5a/86e75a51-e74e-0eee-ae4d-2e0bb5ddc321/source/{w}x{h}{c}.{f}\",\"bgColor\":\"1653b5\",\"textColor1\":\"f8fafa\",\"textColor2\":\"e0e6f1\",\"textColor3\":\"cad8ec\",\"textColor4\":\"b8c8e5\"},\"storeFlowcase\":{\"width\":4320,\"height\":1080,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features128/v4/40/97/64/4097640f-67bf-e263-ab48-635d7234e407/source/{w}x{h}{c}.{f}\",\"bgColor\":\"3e5ab6\",\"textColor1\":\"ecf2fc\",\"textColor2\":\"ebeef7\",\"textColor3\":\"c9d4ee\",\"textColor4\":\"c8d1ea\"},\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features115/v4/70/57/ab/7057ab2c-9741-f103-1762-ae2ea88502a8/source/{w}x{h}{c}.{f}\",\"bgColor\":\"feb929\",\"textColor1\":\"0c0b0a\",\"textColor2\":\"0c0b09\",\"textColor3\":\"2f250f\",\"textColor4\":\"2f250e\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Features126/v4/24/4b/d8/244bd85f-b701-b809-087a-d7f6415940ca/source/{w}x{h}sr.{f}\",\"bgColor\":\"355ef1\",\"textColor1\":\"05070a\",\"textColor2\":\"07090d\",\"textColor3\":\"0e152b\",\"textColor4\":\"0f162f\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"macOS 11.0 or later\",\"subtitle\":\"Easy note-taking \u0026 annotation\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.gingerlabs.Notability\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© Ginger Labs, Inc. 2021\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"11.0\",\"hasFamilyShareableInAppPurchases\":false,\"runsOnIntel\":true,\"editorialNotes\":{\"standard\":\"We rely on this sleek, powerful notetaker to annotate documents, record lectures, sketch illustrations, and more. With built-in iCloud support, it’s perfect for keeping notes, documents, and doodles up to date across your iPhone, iPad, and Mac. \",\"short\":\"For notes that sync with the audio from your lecture or meeting, try Notability. \",\"tagline\":\"Powerful note-taking app\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Ginger Labs, Inc.\",\"preflightPackageUrl\":\"https://osxapps.itunes.apple.com/itunes-assets/Purple112/v4/c6/ab/fb/c6abfb12-ec80-cce9-edbe-fc635e6296df/nsw18081884103774621103.pfpkg\",\"isSiriSupported\":false,\"requiresRosetta\":false,\"languageList\":[\"English\",\"Danish\",\"Dutch\",\"French\",\"German\",\"Indonesian\",\"Italian\",\"Japanese\",\"Korean\",\"Malay\",\"Norwegian Bokmål\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Swedish\",\"Thai\",\"Traditional Chinese\",\"Turkish\",\"Ukrainian\",\"Vietnamese\"],\"runsOnAppleSilicon\":true,\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=360593530\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851377375\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":557097484}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=360593530\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851377375\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":557097484}]}]},\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/63/fa/c9/63fac99e-379b-23d5-25d6-5d227773c90f/AppIcon-1x_U007emarketing-0-10-0-0-sRGB-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"4284ff\",\"textColor1\":\"060c16\",\"textColor2\":\"161613\",\"textColor3\":\"122444\",\"textColor4\":\"1f2c42\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"Danish\",\"tag\":\"da\"},{\"name\":\"Dutch\",\"tag\":\"nl\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Indonesian\",\"tag\":\"id\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Malay\",\"tag\":\"ms\"},{\"name\":\"Norwegian Bokmål\",\"tag\":\"nb\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Swedish\",\"tag\":\"sv\"},{\"name\":\"Thai\",\"tag\":\"th\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"},{\"name\":\"Ukrainian\",\"tag\":\"uk\"},{\"name\":\"Vietnamese\",\"tag\":\"vi\"}],\"supportsGameController\":false,\"releaseDate\":\"2010-04-01\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":851377372,\"editorialArtwork\":{\"originalFlowcaseBrick\":{\"width\":3200,\"height\":600,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features18/v4/86/e7/5a/86e75a51-e74e-0eee-ae4d-2e0bb5ddc321/source/{w}x{h}{c}.{f}\",\"bgColor\":\"1653b5\",\"textColor1\":\"f8fafa\",\"textColor2\":\"e0e6f1\",\"textColor3\":\"cad8ec\",\"textColor4\":\"b8c8e5\"},\"storeFlowcase\":{\"width\":4320,\"height\":1080,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features128/v4/40/97/64/4097640f-67bf-e263-ab48-635d7234e407/source/{w}x{h}{c}.{f}\",\"bgColor\":\"3e5ab6\",\"textColor1\":\"ecf2fc\",\"textColor2\":\"ebeef7\",\"textColor3\":\"c9d4ee\",\"textColor4\":\"c8d1ea\"},\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features115/v4/70/57/ab/7057ab2c-9741-f103-1762-ae2ea88502a8/source/{w}x{h}{c}.{f}\",\"bgColor\":\"feb929\",\"textColor1\":\"0c0b0a\",\"textColor2\":\"0c0b09\",\"textColor3\":\"2f250f\",\"textColor4\":\"2f250e\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is2-ssl.mzstatic.com/image/thumb/Features126/v4/24/4b/d8/244bd85f-b701-b809-087a-d7f6415940ca/source/{w}x{h}sr.{f}\",\"bgColor\":\"355ef1\",\"textColor1\":\"05070a\",\"textColor2\":\"07090d\",\"textColor3\":\"0e152b\",\"textColor4\":\"0f162f\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.0 or later. Compatible with iPhone, iPad, and iPod touch.\",\"subtitle\":\"Easy note-taking \u0026 annotation\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.gingerlabs.Notability\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© Ginger Labs, Inc. 2021\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.0\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"standard\":\"We rely on this sleek, powerful notetaker to annotate documents, record lectures, sketch illustrations, and more. With built-in iCloud support, it’s perfect for keeping notes, documents, and doodles up to date across your iPhone, iPad, and Mac. \",\"short\":\"For notes that sync with the audio from your lecture or meeting, try Notability. \",\"tagline\":\"Powerful note-taking app\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"Ginger Labs, Inc.\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"Danish\",\"Dutch\",\"French\",\"German\",\"Indonesian\",\"Italian\",\"Japanese\",\"Korean\",\"Malay\",\"Norwegian Bokmål\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Swedish\",\"Thai\",\"Traditional Chinese\",\"Turkish\",\"Ukrainian\",\"Vietnamese\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=360593530\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=851377372\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":568236032}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=360593530\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=851377372\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":568236032}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.7,\"ratingCount\":193035,\"ratingCountList\":[7296,1472,2977,12722,168568],\"ariaLabelForRatings\":\"4.7 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Notability\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/360593530/developer?l=en-US\",\"data\":[{\"id\":\"318126112\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/318126112?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/360593530/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}},{\"id\":\"920402079\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/920402079?l=en-US\",\"attributes\":{\"artistName\":\"Apalon Apps\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/notepad-note-taking-app/id920402079\",\"isIOSBinaryMacOSCompatible\":false,\"reviewsRestricted\":false,\"deviceFamilies\":[\"ipad\"],\"isPreorder\":false,\"platformAttributes\":{\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/44/32/8a/44328abb-b965-bd8e-42cf-0d43870baac9/AppIcon-0-1x_U007emarketing-85-220-0-7.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000f16\",\"textColor2\":\"000f16\",\"textColor3\":\"333f45\",\"textColor4\":\"333f45\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"},{\"name\":\"French\",\"tag\":\"fr-FR\"},{\"name\":\"German\",\"tag\":\"de-DE\"},{\"name\":\"Italian\",\"tag\":\"it\"},{\"name\":\"Japanese\",\"tag\":\"ja\"},{\"name\":\"Korean\",\"tag\":\"ko\"},{\"name\":\"Portuguese\",\"tag\":\"pt-PT\"},{\"name\":\"Russian\",\"tag\":\"ru\"},{\"name\":\"Simplified Chinese\",\"tag\":\"zh-Hans-CN\"},{\"name\":\"Spanish\",\"tag\":\"es-ES\"},{\"name\":\"Traditional Chinese\",\"tag\":\"zh-Hant-TW\"},{\"name\":\"Turkish\",\"tag\":\"tr\"}],\"supportsGameController\":false,\"releaseDate\":\"2014-11-26\",\"hasSafariExtension\":false,\"minimumMacOSVersion\":\"11.0\",\"externalVersionId\":828761271,\"editorialArtwork\":{\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 9.0 or later. Compatible with iPad.\",\"subtitle\":\"Notebook \u0026 Handwriting Tool\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"com.apalonapps.notepadfree\",\"hasInAppPurchases\":false,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2014-2020 IAC Search \u0026 Media Technologies Limited\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"9.0\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"tagline\":\"Personalize your notes\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"IAC Search \u0026 Media Europe Ltd.\",\"isSiriSupported\":false,\"languageList\":[\"English\",\"French\",\"German\",\"Italian\",\"Japanese\",\"Korean\",\"Portuguese\",\"Russian\",\"Simplified Chinese\",\"Spanish\",\"Traditional Chinese\",\"Turkish\"],\"requiredCapabilities\":\"armv7 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=920402079\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=828761271\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":81180672}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=920402079\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=828761271\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":81180672}]}]}},\"hasEula\":true,\"userRating\":{\"value\":4.3,\"ratingCount\":8384,\"ratingCountList\":[665,267,712,1314,5426],\"ariaLabelForRatings\":\"4.3 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"4+\",\"value\":100,\"rank\":1}},\"name\":\"Notepad+: Note Taking App\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/920402079/developer?l=en-US\",\"data\":[{\"id\":\"749046894\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/749046894?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/920402079/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6000\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6000?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Business\",\"url\":\"https://itunes.apple.com/us/genre/id6000\"}}]}}},{\"id\":\"922765270\",\"type\":\"apps\",\"href\":\"/v1/catalog/us/apps/922765270?l=en-US\",\"attributes\":{\"artistName\":\"LiquidText, Inc.\",\"familyShareEnabledDate\":\"0001-04-23T00:00:00Z\",\"url\":\"https://apps.apple.com/us/app/liquidtext/id922765270\",\"isIOSBinaryMacOSCompatible\":false,\"reviewsRestricted\":false,\"deviceFamilies\":[\"mac\",\"ipad\"],\"isPreorder\":false,\"platformAttributes\":{\"osx\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is1-ssl.mzstatic.com/image/thumb/Purple112/v4/29/b3/18/29b31860-50c3-6bc6-55b2-312e0003550d/AppIcon-MacOS-0-2x-4-0-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f5f5f5\",\"textColor2\":\"15c8f4\",\"textColor3\":\"c4c4c4\",\"textColor4\":\"10a0c3\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"}],\"supportsGameController\":false,\"releaseDate\":\"2015-09-03\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":850487734,\"editorialArtwork\":{\"originalFlowcaseBrick\":{\"width\":3200,\"height\":600,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features4/v4/5a/f8/7c/5af87c97-23b4-e145-ab81-08c8a69354ad/source/{w}x{h}{c}.{f}\",\"bgColor\":\"fdfdfe\",\"textColor1\":\"ffffff\",\"textColor2\":\"0f0e12\",\"textColor3\":\"353f43\",\"textColor4\":\"3f3e41\"},\"storeFlowcase\":{\"width\":4320,\"height\":1080,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features111/v4/9e/42/81/9e428189-877f-344c-dc2e-608f805b51d9/source/{w}x{h}{c}.{f}\",\"bgColor\":\"28f19c\",\"textColor1\":\"020c0e\",\"textColor2\":\"161616\",\"textColor3\":\"093a2a\",\"textColor4\":\"1a4231\"},\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features128/v4/0e/ed/25/0eed25d3-74c1-cd68-484a-00b66997fa88/source/{w}x{h}{c}.{f}\",\"bgColor\":\"045f68\",\"textColor1\":\"ffffff\",\"textColor2\":\"e0f5fd\",\"textColor3\":\"ccdfe0\",\"textColor4\":\"b4d7df\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features69/v4/03/8b/b8/038bb8bb-36db-cb5f-74d6-397df858ec6f/source/{w}x{h}sr.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"020c0b\",\"textColor2\":\"070d12\",\"textColor3\":\"353c3c\",\"textColor4\":\"393e42\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"macOS 11.0 or later\",\"subtitle\":\"Annotate \u0026 review documents\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"--.LiquidText-PDF\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2022 LiquidText, Inc.\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"11.0\",\"hasFamilyShareableInAppPurchases\":false,\"runsOnIntel\":true,\"editorialNotes\":{\"standard\":\"If your job or studies involve reading through longer works, LiquidText offers the kind of flexibility you’ve always wished for. Using gestures, you can quickly drag excerpts to a workspace on the side, make notes and comments, or compress pages to compare distant sections. With support for popular file formats, sharing options, and cloud services, it’s so intuitive to use that you’ll forget how you ever read without it.\",\"short\":\"Easily make notes and comments on the documents you read.\",\"tagline\":\"Read and annotate your documents\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"LiquidText, Inc.\",\"preflightPackageUrl\":\"https://osxapps.itunes.apple.com/itunes-assets/Purple112/v4/f8/a1/e3/f8a1e341-b99c-2c6d-2497-001c96655013/ywk4890118315287750043.pfpkg\",\"isSiriSupported\":false,\"requiresRosetta\":false,\"languageList\":[\"English\"],\"runsOnAppleSilicon\":true,\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=922765270\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=850487734\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":215212554}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=922765270\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=850487734\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"macSoftware\",\"size\":215212554}]}]},\"ios\":{\"requires32bit\":false,\"artwork\":{\"width\":1024,\"height\":1024,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Purple122/v4/bd/06/a1/bd06a178-54f7-5823-d702-26339548b9e1/AppIcon-0-1x_U007emarketing-2-0-sRGB-85-220.png/{w}x{h}{c}.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"000e13\",\"textColor2\":\"031014\",\"textColor3\":\"333e42\",\"textColor4\":\"364043\"},\"hasMessagesExtension\":false,\"hasPrivacyPolicyText\":false,\"supportedLocales\":[{\"name\":\"English\",\"tag\":\"en-US\"}],\"supportsGameController\":false,\"releaseDate\":\"2015-09-03\",\"editorialBadgeInfo\":{\"editorialBadgeType\":\"editorialPriority\",\"nameForDisplay\":\"Editors’ Choice\"},\"hasSafariExtension\":false,\"externalVersionId\":850486621,\"editorialArtwork\":{\"originalFlowcaseBrick\":{\"width\":3200,\"height\":600,\"url\":\"https://is3-ssl.mzstatic.com/image/thumb/Features4/v4/5a/f8/7c/5af87c97-23b4-e145-ab81-08c8a69354ad/source/{w}x{h}{c}.{f}\",\"bgColor\":\"fdfdfe\",\"textColor1\":\"ffffff\",\"textColor2\":\"0f0e12\",\"textColor3\":\"353f43\",\"textColor4\":\"3f3e41\"},\"storeFlowcase\":{\"width\":4320,\"height\":1080,\"url\":\"https://is4-ssl.mzstatic.com/image/thumb/Features111/v4/9e/42/81/9e428189-877f-344c-dc2e-608f805b51d9/source/{w}x{h}{c}.{f}\",\"bgColor\":\"28f19c\",\"textColor1\":\"020c0e\",\"textColor2\":\"161616\",\"textColor3\":\"093a2a\",\"textColor4\":\"1a4231\"},\"subscriptionHero\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features128/v4/0e/ed/25/0eed25d3-74c1-cd68-484a-00b66997fa88/source/{w}x{h}{c}.{f}\",\"bgColor\":\"045f68\",\"textColor1\":\"ffffff\",\"textColor2\":\"e0f5fd\",\"textColor3\":\"ccdfe0\",\"textColor4\":\"b4d7df\"},\"bannerUber\":{\"width\":4320,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features69/v4/03/8b/b8/038bb8bb-36db-cb5f-74d6-397df858ec6f/source/{w}x{h}sr.{f}\",\"bgColor\":\"ffffff\",\"textColor1\":\"020c0b\",\"textColor2\":\"070d12\",\"textColor3\":\"353c3c\",\"textColor4\":\"393e42\"},\"brandLogo\":{\"width\":1080,\"height\":1080,\"url\":\"https://is5-ssl.mzstatic.com/image/thumb/Features114/v4/0d/d9/6c/0dd96cca-2fb0-2ff7-eeb4-204c1dd54bcc/source/{w}x{h}{c}.{f}\",\"bgColor\":\"000000\",\"textColor1\":\"f2f2f2\",\"textColor2\":\"e5e5e5\",\"textColor3\":\"c1c1c1\",\"textColor4\":\"b7b7b7\"}},\"requirementsString\":\"Requires iOS 14.1 or later. Compatible with iPad.\",\"subtitle\":\"Annotate \u0026 review documents\",\"isDeliveredInIOSAppForWatchOS\":false,\"bundleId\":\"--.LiquidText-PDF\",\"hasInAppPurchases\":true,\"isAppleWatchSupported\":false,\"isStandaloneForWatchOS\":false,\"supportsPassbook\":false,\"copyright\":\"© 2022 LiquidText, Inc.\",\"requiresGameController\":false,\"isHiddenFromSpringboard\":false,\"isGameCenterEnabled\":false,\"minimumOSVersion\":\"14.1\",\"hasFamilyShareableInAppPurchases\":false,\"editorialNotes\":{\"standard\":\"If your job or studies involve reading through longer works, LiquidText offers the kind of flexibility you’ve always wished for. Using gestures, you can quickly drag excerpts to a workspace on the side, make notes and comments, or compress pages to compare distant sections. With support for popular file formats, sharing options, and cloud services, it’s so intuitive to use that you’ll forget how you ever read without it.\",\"short\":\"Easily make notes and comments on the documents you read.\",\"tagline\":\"Read and annotate your documents\"},\"is32bitOnly\":false,\"isStandaloneWithCompanionForWatchOS\":false,\"seller\":\"LiquidText, Inc.\",\"isSiriSupported\":false,\"languageList\":[\"English\"],\"requiredCapabilities\":\"arm64 \",\"offers\":[{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=922765270\u0026pricingParameters=STDQ\u0026pg=default\u0026appExtVrsId=850486621\",\"type\":\"get\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":248626176}]},{\"buyParams\":\"productType=C\u0026price=0\u0026salableAdamId=922765270\u0026pricingParameters=SWUPD\u0026pg=default\u0026appExtVrsId=850486621\",\"type\":\"update\",\"priceFormatted\":\"$0.00\",\"price\":0,\"currencyCode\":\"USD\",\"assets\":[{\"flavor\":\"iosSoftware\",\"size\":248626176}]}]}},\"hasEula\":false,\"userRating\":{\"value\":4.6,\"ratingCount\":4808,\"ratingCountList\":[201,68,154,552,3833],\"ariaLabelForRatings\":\"4.6 stars\"},\"contentRatingsBySystem\":{\"appsApple\":{\"name\":\"17+\",\"value\":600,\"rank\":4,\"advisories\":[\"Unrestricted Web Access\"]}},\"name\":\"LiquidText\",\"sellerLabel\":\"Seller\",\"supportsArcade\":false,\"genreDisplayName\":\"Productivity\",\"isFirstPartyHideableApp\":false,\"usesLocationBackgroundMode\":false},\"relationships\":{\"developer\":{\"href\":\"/v1/catalog/us/apps/922765270/developer?l=en-US\",\"data\":[{\"id\":\"922765269\",\"type\":\"developers\",\"href\":\"/v1/catalog/us/developers/922765269?l=en-US\"}]},\"genres\":{\"href\":\"/v1/catalog/us/apps/922765270/genres?l=en-US\",\"data\":[{\"id\":\"6007\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6007?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Productivity\",\"url\":\"https://itunes.apple.com/us/genre/id6007\"}},{\"id\":\"6017\",\"type\":\"genres\",\"href\":\"/v1/catalog/us/genres/6017?l=en-US\",\"attributes\":{\"parentId\":\"36\",\"parentName\":\"App Store\",\"name\":\"Education\",\"url\":\"https://itunes.apple.com/us/genre/id6017\"}}]}}}]}"}</script><script type="fastboot/shoebox" id="shoebox-global-elements">{"nav":"\u003caside id=\"ac-gn-segmentbar\" class=\"ac-gn-segmentbar\" lang=\"en-US\" dir=\"ltr\" data-strings=\"{ \u0026apos;exit\u0026apos;: \u0026apos;Exit\u0026apos;, \u0026apos;view\u0026apos;: \u0026apos;{%STOREFRONT%} Store Home\u0026apos;, \u0026apos;segments\u0026apos;: { \u0026apos;smb\u0026apos;: \u0026apos;Business Store Home\u0026apos;, \u0026apos;eduInd\u0026apos;: \u0026apos;Education Store Home\u0026apos;, \u0026apos;other\u0026apos;: \u0026apos;Store Home\u0026apos; } }\"\u003e\u003c/aside\u003e\n\u003cinput type=\"checkbox\" id=\"ac-gn-menustate\" class=\"ac-gn-menustate\"\u003e\n\u003cnav id=\"ac-globalnav\" class=\"no-js\" role=\"navigation\" aria-label=\"Global\" data-hires=\"false\" data-analytics-region=\"global nav\" lang=\"en-US\" dir=\"ltr\" data-www-domain=\"www.apple.com\" data-store-locale=\"us\" data-store-root-path=\"/us\" data-store-api=\"https://www.apple.com/[storefront]/shop/bag/status\" data-search-locale=\"en_US\" data-search-suggestions-api=\"https://www.apple.com/search-services/suggestions/\" data-search-defaultlinks-api=\"https://www.apple.com/search-services/suggestions/defaultlinks/\"\u003e\n\t\u003cdiv class=\"ac-gn-content\"\u003e\n\t\t\u003cul class=\"ac-gn-header\"\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-menuicon\"\u003e\n\t\t\t\t\u003ca href=\"#ac-gn-menustate\" role=\"button\" class=\"ac-gn-menuanchor ac-gn-menuanchor-open\" id=\"ac-gn-menuanchor-open\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-menuanchor-label\"\u003eGlobal Nav Open Menu\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\t\u003ca href=\"#\" role=\"button\" class=\"ac-gn-menuanchor ac-gn-menuanchor-close\" id=\"ac-gn-menuanchor-close\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-menuanchor-label\"\u003eGlobal Nav Close Menu\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\t\u003clabel class=\"ac-gn-menuicon-label\" for=\"ac-gn-menustate\" aria-hidden=\"true\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-menuicon-bread ac-gn-menuicon-bread-top\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-top\"\u003e\u003c/span\u003e\n\t\t\t\t\t\u003c/span\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-menuicon-bread ac-gn-menuicon-bread-bottom\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-bottom\"\u003e\u003c/span\u003e\n\t\t\t\t\t\u003c/span\u003e\n\t\t\t\t\u003c/label\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-apple\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-apple\" href=\"https://www.apple.com/\" data-analytics-title=\"apple home\" id=\"ac-gn-firstfocus-small\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eApple\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-bag ac-gn-bag-small\" id=\"ac-gn-bag-small\"\u003e\n\t\t\t\t\u003cdiv class=\"ac-gn-bag-wrapper\"\u003e\n\t\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-bag\" href=\"https://www.apple.com/us/shop/goto/bag\" data-analytics-title=\"bag\" data-analytics-click=\"bag\" aria-label=\"Shopping Bag\" data-string-badge=\"Shopping Bag with item count :\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eShopping Bag\u003c/span\u003e\n\t\t\t\t\t\u003c/a\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge\" aria-hidden=\"true\" data-analytics-title=\"bag\" data-analytics-click=\"bag\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-separator\"\u003e\u003c/span\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-number\"\u003e\u003c/span\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-unit\"\u003e+\u003c/span\u003e\n\t\t\t\t\t\u003c/span\u003e\n\t\t\t\t\u003c/div\u003e\n\t\t\t\t\u003cspan class=\"ac-gn-bagview-caret ac-gn-bagview-caret-large\"\u003e\u003c/span\u003e\n\t\t\t\u003c/li\u003e\n\t\t\u003c/ul\u003e\n\t\t\u003cdiv class=\"ac-gn-search-placeholder-container\" role=\"search\"\u003e\n\t\t\t\u003cdiv class=\"ac-gn-search ac-gn-search-small\"\u003e\n\t\t\t\t\u003ca id=\"ac-gn-link-search-small\" class=\"ac-gn-link\" href=\"https://www.apple.com/us/search\" data-analytics-title=\"search\" data-analytics-intrapage-link aria-label=\"Search apple.com\"\u003e\n\t\t\t\t\t\u003cdiv class=\"ac-gn-search-placeholder-bar\"\u003e\n\t\t\t\t\t\t\u003cdiv class=\"ac-gn-search-placeholder-input\"\u003e\n\t\t\t\t\t\t\t\u003cdiv class=\"ac-gn-search-placeholder-input-text\" aria-hidden=\"true\"\u003e\n\t\t\t\t\t\t\t\t\u003cdiv class=\"ac-gn-link-search ac-gn-search-placeholder-input-icon\"\u003e\u003c/div\u003e\n\t\t\t\t\t\t\t\t\u003cspan class=\"ac-gn-search-placeholder\"\u003eSearch apple.com\u003c/span\u003e\n\t\t\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\t\t\u003cdiv class=\"ac-gn-searchview-close ac-gn-searchview-close-small ac-gn-search-placeholder-searchview-close\"\u003e\n\t\t\t\t\t\t\t\u003cspan class=\"ac-gn-searchview-close-cancel\" aria-hidden=\"true\"\u003eCancel\u003c/span\u003e\n\t\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/div\u003e\n\t\t\u003c/div\u003e\n\t\t\u003cul class=\"ac-gn-list\"\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-apple\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-apple\" href=\"https://www.apple.com/\" data-analytics-title=\"apple home\" id=\"ac-gn-firstfocus\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eApple\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-store\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-store\" href=\"https://www.apple.com/us/shop/goto/store\" data-analytics-title=\"store\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eStore\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-mac\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-mac\" href=\"https://www.apple.com/mac/\" data-analytics-title=\"mac\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eMac\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-ipad\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-ipad\" href=\"https://www.apple.com/ipad/\" data-analytics-title=\"ipad\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eiPad\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-iphone\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-iphone\" href=\"https://www.apple.com/iphone/\" data-analytics-title=\"iphone\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eiPhone\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-watch\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-watch\" href=\"https://www.apple.com/watch/\" data-analytics-title=\"watch\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eWatch\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-airpods\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-airpods\" href=\"https://www.apple.com/airpods/\" data-analytics-title=\"airpods\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eAirPods\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-tvhome\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-tvhome\" href=\"https://www.apple.com/tv-home/\" data-analytics-title=\"tv and home\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eTV \u0026amp; Home\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-onlyonapple\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-onlyonapple\" href=\"https://www.apple.com/services/\" data-analytics-title=\"only on apple\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eOnly on Apple\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-accessories\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-accessories\" href=\"https://www.apple.com/us/shop/goto/buy_accessories\" data-analytics-title=\"accessories\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eAccessories\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-support\"\u003e\n\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-support\" href=\"https://support.apple.com\" data-analytics-title=\"support\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eSupport\u003c/span\u003e\n\t\t\t\t\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-item-menu ac-gn-search\" role=\"search\"\u003e\n\t\t\t\t\u003ca id=\"ac-gn-link-search\" class=\"ac-gn-link ac-gn-link-search\" href=\"https://www.apple.com/us/search\" data-analytics-title=\"search\" data-analytics-intrapage-link aria-label=\"Search apple.com\"\u003e\u003c/a\u003e\n\t\t\t\u003c/li\u003e\n\t\t\t\u003cli class=\"ac-gn-item ac-gn-bag\" id=\"ac-gn-bag\"\u003e\n\t\t\t\t\u003cdiv class=\"ac-gn-bag-wrapper\"\u003e\n\t\t\t\t\t\u003ca class=\"ac-gn-link ac-gn-link-bag\" href=\"https://www.apple.com/us/shop/goto/bag\" data-analytics-title=\"bag\" data-analytics-click=\"bag\" aria-label=\"Shopping Bag\" data-string-badge=\"Shopping Bag with item count : {%BAGITEMCOUNT%}\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-link-text\"\u003eShopping Bag\u003c/span\u003e\n\t\t\t\t\t\u003c/a\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge\" aria-hidden=\"true\" data-analytics-title=\"bag\" data-analytics-click=\"bag\"\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-separator\"\u003e\u003c/span\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-number\"\u003e\u003c/span\u003e\n\t\t\t\t\t\t\u003cspan class=\"ac-gn-bag-badge-unit\"\u003e+\u003c/span\u003e\n\t\t\t\t\t\u003c/span\u003e\n\t\t\t\t\u003c/div\u003e\n\t\t\t\t\u003cspan class=\"ac-gn-bagview-caret ac-gn-bagview-caret-large\"\u003e\u003c/span\u003e\n\t\t\t\u003c/li\u003e\n\t\t\u003c/ul\u003e\n\t\t\u003caside id=\"ac-gn-searchview\" class=\"ac-gn-searchview\" role=\"search\" data-analytics-region=\"search\"\u003e\n\t\t\t\u003cdiv class=\"ac-gn-searchview-content\"\u003e\n\t\t\t\t\u003cdiv class=\"ac-gn-searchview-bar\"\u003e\n\t\t\t\t\t\u003cdiv class=\"ac-gn-searchview-bar-wrapper\"\u003e\n\t\t\t\t\t\t\u003cform id=\"ac-gn-searchform\" class=\"ac-gn-searchform\" action=\"https://www.apple.com/us/search\" method=\"get\"\u003e\n\t\t\t\t\t\t\t\u003cdiv class=\"ac-gn-searchform-wrapper\"\u003e\n\t\t\t\t\t\t\t\t\u003cinput id=\"ac-gn-searchform-input\" class=\"ac-gn-searchform-input\" type=\"text\" aria-label=\"Search apple.com\" placeholder=\"Search apple.com\" autocorrect=\"off\" autocapitalize=\"off\" autocomplete=\"off\" spellcheck=\"false\" role=\"combobox\" aria-autocomplete=\"list\" aria-expanded=\"true\" aria-owns=\"quicklinks suggestions\"\u003e\n\t\t\t\t\t\t\t\t\u003cinput id=\"ac-gn-searchform-src\" type=\"hidden\" name=\"src\" value=\"itunes_serp\"\u003e\n\t\t\t\t\t\t\t\t\u003cbutton id=\"ac-gn-searchform-submit\" class=\"ac-gn-searchform-submit\" type=\"submit\" disabled aria-label=\"Submit Search\"\u003e \u003c/button\u003e\n\t\t\t\t\t\t\t\t\u003cbutton id=\"ac-gn-searchform-reset\" class=\"ac-gn-searchform-reset\" type=\"reset\" disabled aria-label=\"Clear Search\"\u003e\n\t\t\t\t\t\t\t\t\t\u003cspan class=\"ac-gn-searchform-reset-background\"\u003e\u003c/span\u003e\n\t\t\t\t\t\t\t\t\u003c/button\u003e\n\t\t\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\t\t\u003c/form\u003e\n\t\t\t\t\t\t\u003cbutton id=\"ac-gn-searchview-close-small\" class=\"ac-gn-searchview-close ac-gn-searchview-close-small\" aria-label=\"Cancel Search\"\u003e\n\t\t\t\t\t\t\t\u003cspan class=\"ac-gn-searchview-close-cancel\" aria-hidden=\"true\"\u003e\n\t\t\t\t\t\t\t\tCancel\n\t\t\t\t\t\t\t\u003c/span\u003e\n\t\t\t\t\t\t\u003c/button\u003e\n\t\t\t\t\t\u003c/div\u003e\n\t\t\t\t\u003c/div\u003e\n\t\t\t\t\u003caside id=\"ac-gn-searchresults\" class=\"ac-gn-searchresults\" data-string-quicklinks=\"Quick Links\" data-string-suggestions=\"Suggested Searches\" data-string-noresults\u003e\u003c/aside\u003e\n\t\t\t\u003c/div\u003e\n\t\t\t\u003cbutton id=\"ac-gn-searchview-close\" class=\"ac-gn-searchview-close\" aria-label=\"Cancel Search\"\u003e\n\t\t\t\t\u003cspan class=\"ac-gn-searchview-close-wrapper\"\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-searchview-close-left\"\u003e\u003c/span\u003e\n\t\t\t\t\t\u003cspan class=\"ac-gn-searchview-close-right\"\u003e\u003c/span\u003e\n\t\t\t\t\u003c/span\u003e\n\t\t\t\u003c/button\u003e\n\t\t\u003c/aside\u003e\n\t\t\u003caside class=\"ac-gn-bagview\" data-analytics-region=\"bag\"\u003e\n\t\t\t\u003cdiv class=\"ac-gn-bagview-scrim\"\u003e\n\t\t\t\t\u003cspan class=\"ac-gn-bagview-caret ac-gn-bagview-caret-small\"\u003e\u003c/span\u003e\n\t\t\t\u003c/div\u003e\n\t\t\t\u003cdiv class=\"ac-gn-bagview-content\" id=\"ac-gn-bagview-content\"\u003e\n\t\t\t\u003c/div\u003e\n\t\t\u003c/aside\u003e\n\t\u003c/div\u003e\n\u003c/nav\u003e\n\u003cdiv class=\"ac-gn-blur\"\u003e\u003c/div\u003e\n\u003cdiv id=\"ac-gn-curtain\" class=\"ac-gn-curtain\"\u003e\u003c/div\u003e\n\u003cdiv id=\"ac-gn-placeholder\" class=\"ac-nav-placeholder\"\u003e\u003c/div\u003e\n","footer":"\u003cfooter id=\"ac-globalfooter\" class=\"no-js\" role=\"contentinfo\" lang=\"en-US\" dir=\"ltr\"\u003e\u003cdiv class=\"ac-gf-content\"\u003e\u003csection class=\"ac-gf-footer\"\u003e\n\t\u003cdiv class=\"ac-gf-footer-shop\" x-ms-format-detection=\"none\"\u003e\n\t\tMore ways to shop: \u003ca href=\"https://www.apple.com/retail/\" data-analytics-title=\"find an apple store\"\u003eFind an Apple Store\u003c/a\u003e or \u003ca href=\"https://locate.apple.com/\" data-analytics-title=\"other retailers or resellers\" data-analytics-exit-link\u003eother retailer\u003c/a\u003e near you. \u003cspan class=\"nowrap\"\u003eOr call 1-800-MY-APPLE.\u003c/span\u003e\n\t\u003c/div\u003e\n\t\u003cdiv class=\"ac-gf-footer-locale\"\u003e\n\t\t\u003ca class=\"ac-gf-footer-locale-link\" href=\"https://www.apple.com/choose-country-region/\" title=\"Choose your country or region\" aria-label=\"Choose your country or region\" data-analytics-title=\"choose your country\"\u003eChoose your country or region\u003c/a\u003e\n\t\u003c/div\u003e\n\t\u003cdiv class=\"ac-gf-footer-legal\"\u003e\n\t\t\u003cdiv class=\"ac-gf-footer-legal-copyright\"\u003eCopyright \u0026#xA9; 2022 Apple Inc. All rights reserved.\n\t\t\u003c/div\u003e\n\t\t\u003cdiv class=\"ac-gf-footer-legal-links\"\u003e\n\t\t\t\u003ca class=\"ac-gf-footer-legal-link\" href=\"https://www.apple.com/legal/privacy/\" data-analytics-title=\"privacy policy\"\u003ePrivacy Policy\u003c/a\u003e\n\t\t\t\u003ca class=\"ac-gf-footer-legal-link\" href=\"https://www.apple.com/legal/internet-services/terms/site.html\" data-analytics-title=\"terms of use\"\u003eTerms of Use\u003c/a\u003e\n\t\t\t\u003ca class=\"ac-gf-footer-legal-link\" href=\"https://www.apple.com/us/shop/goto/help/sales_refunds\" data-analytics-title=\"sales and refunds\"\u003eSales and Refunds\u003c/a\u003e\n\t\t\t\u003ca class=\"ac-gf-footer-legal-link\" href=\"https://www.apple.com/legal/\" data-analytics-title=\"legal\"\u003eLegal\u003c/a\u003e\n\t\t\t\u003ca class=\"ac-gf-footer-legal-link\" href=\"https://www.apple.com/sitemap/\" data-analytics-title=\"site map\"\u003eSite Map\u003c/a\u003e\n\t\t\u003c/div\u003e\n\t\u003c/div\u003e\n\u003c/section\u003e\n\u003c/div\u003e\u003c/footer\u003e","styles":{"nav":"2210.1.0/en_US/ac-global-nav.6ab06e7ad3aaf025138951b5c958ae3b.css","footer":"2210.1.0/en_US/ac-global-footer.7fb24d229e183ab411ed7662850ce5a0.css"},"scripts":{"nav":"2210.1.0/en_US/ac-global-nav.294b65e7729ab1c596e195cd90349397.js","footer":"2210.1.0/en_US/ac-global-footer.33e5f7b4cd1360fa6599e7adcbb494dd.js"},"isLoaded":true}</script><script type="x/boundary" id="fastboot-body-end"></script>
    <div id="modal-container"></div>
    <script integrity="" src="/assets/vendor-58205b5fd810c33e0b930776b6f89626.js"></script>
<script src="/assets/chunk.309.848d97fb43af54a66969.js"></script>
<script src="/assets/chunk.143.c3acd32b236afd0d2ff9.js"></script>
    <script integrity="" src="/assets/web-experience-app-e37cc888ddf1648f9f2742b4100ea7c1.js" data-app-legacy-src="" nomodule></script>

    
    
  <script type="application/ld+json" data-test-seo-data-org="apps">
    {
      "@context": "https://schema.org",
      "@id": "https://apps.apple.com/#organization",
      "@type": "Organization",
      "name": "App Store",
      "url": "https://apps.apple.com/",
      "logo": "https://apps.apple.com/assets/images/knowledge-graph/apps.png",
      "sameAs": [
        "https://www.wikidata.org/wiki/Q368215",
        "https://twitter.com/AppStore",
        "https://www.facebook.com/AppStore/"
      ],
      "parentOrganization": {
        "@type": "Organization",
        "name": "Apple",
        "@id": "https://www.apple.com/#organization",
        "url": "https://www.apple.com/"
      }
    }
  </script>

  

<script type="module" src="/assets/web-experience-app-e76abf5e8c2c2d0e964e24279a3baf42.modern.js" integrity=""></script></body></html>
// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: magic;
// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: cyan; icon-glyph: magic;
// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: null; icon-glyph: null;
// This script was downloaded using ScriptDude.
// Do not remove these lines, if you want to benefit from automatic updates.
// source: https://gist.githubusercontent.com/andreasRedeker/da8fdee258326af14400187c521723df/raw/webcam-widget.js; docs: https://gist.github.com/andreasRedeker/da8fdee258326af14400187c521723df; hash: -1223220317;

// Script by Andreas Redeker <hello@andreasredeker.de>

let param = args.widgetParameter
let url

if (param != null && param.length > 0) {
  url = param
} else {
  url = "https://www.foto-webcam.eu/webcam/zugspitze-sued/"
}

const imgUrl = url + "current/400.jpg"
const imgReq = await new Request(imgUrl)
const img = await imgReq.loadImage()

if (config.runsInWidget) {
  let widget = createWidget(img)
  Script.setWidget(widget)
  Script.complete()
} else { 	
  let widget = createWidget(img)
  await widget.presentMedium()
}

function createWidget(img) {

  const widget = new ListWidget()
  widget.backgroundColor = Color.black()
  widget.url = url
  widget.backgroundImage = img 

  widget.addSpacer()

  const titleText = widget.addText(getWcNameFromUrl())
  titleText.font = Font.boldSystemFont(12)
  titleText.textColor = Color.white()
  titleText.shadowRadius = 3
  titleText.shadowColor = Color.black()

  widget.addSpacer(2)

  const subtitleText = widget.addText('foto-webcam.eu')
  subtitleText.font = Font.systemFont(8)
  subtitleText.textColor = Color.white()
  subtitleText.textOpacity = 0.8
  subtitleText.shadowRadius = 3
  subtitleText.shadowColor = Color.black()

  return widget
}

function getWcNameFromUrl() {
  let wcName = url.split('/')
  wcName = wcName[wcName.length - 2].replace('-', ' ').replace(/\b\w/g, function(c) {
    return c.toUpperCase();
  })
  return wcName
}
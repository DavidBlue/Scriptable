// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: cyan; icon-glyph: magic;
const QUOTES_API = 'https://quotes-api.netlify.app/.netlify/functions/get-quote';
const BG_COLOR = "#000000";
const BG_IMAGE = true;

let randomQuote = await loadQuote()

let img = null;

// no-background.js is needed from here if BG_IMAGE is set to true
// https://github.com/supermamon/scriptable-no-background
if (BG_IMAGE) {
  const nobg = importModule("no-background.js");

  // Bug: for some reason nobg isn't available in the createWidget function
  // need to define another variable like this in order for it to work:
  img = await nobg.getSliceForWidget("random-quotes");
}


if (config.runsInWidget) {
  let widget = createWidget(randomQuote)

  Script.setWidget(widget)
  Script.complete()
} else {
  const options = ['Small', 'Medium', 'Large', 'Cancel']
  let resp = await presentAlert('Preview Widget', options)
  if (resp == options.length - 1) return
  let size = options[resp]
  let widget = createWidget(randomQuote)
  await widget[`present${size}`]()
}

function createWidget(dailyText) {

  let quote = dailyText.text || "";
  let author = `- ${dailyText.author || "Unknown"}`;

  let titleFont = Font.semiboldSystemFont(13)
  let txtFont = Font.systemFont(12)

  let w = new ListWidget();
  if (BG_IMAGE) {
    w.backgroundImage = img;
  } else {
    w.backgroundColor = new Color(BG_COLOR);
  }

  let quoteTxt = w.addText(quote)
  quoteTxt.font = titleFont
  quoteTxt.textSize = 13
  quoteTxt.textColor = Color.white()


  let authorTxt = w.addText(author)
  authorTxt.font = txtFont
  authorTxt.textColor = Color.white()
  authorTxt.textOpacity = 0.8
  authorTxt.textSize = 12

  return w
}

async function loadQuote() {

  let url = QUOTES_API;

  let req = new Request(url)
  let json = await req.loadJSON()
  return json;
}


async function presentAlert(prompt, items, asSheet) {
  let alert = new Alert()
  alert.message = prompt

  for (const item of items) {
    alert.addAction(item)
  }
  let resp = asSheet ?
    await alert.presentSheet() :
    await alert.presentAlert()
  return resp
}
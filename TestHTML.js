// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: light-brown; icon-glyph: magic;
const htmlWidget = importModule("html-widget")

let widget = await htmlWidget(`
<widget>
  <text>Goodmorning!</text>
</widget>
`)
Script.setWidget(widget)
widget.presentSmall()
Script.complete()